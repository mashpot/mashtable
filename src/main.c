/*
 * main.c
 *
 * Created 31 Jul 2023 by Mason Hall (mason@mashpot.net)
 *
 * Copyright (C) 2023 Mason Hall
 */

#include <stdio.h>
#include <stdlib.h>
#include <unistd.h>

//#include <mashlib.h>
//#include <mreg.h>
//#include <balloc.h>

#include <test.h>

static const char *
__attribute__ ((pure))
argcmp (const char *arg, const char *name)
{
  int c = -1;

  if (*arg == *name++)
    {
      while ((c = *++arg) == *name++ && c != 0);
    }
  return c != -1 ? arg : NULL;
}

#define SIZES_MAX 16
int
main(int argc, char *argv[])
{
  const size_t sizec = SIZES_MAX;
  size_t sizev[SIZES_MAX] = {
      32,
      512,
      1024,
      4096,
      32768,
      //65536,
      //131072,
      //262144
  };
  size_t size, count, msecs, i, n;

  count = 4096;
  msecs = 10000;//52500;

  argv++;
  argc--;
  n = (size_t) argc;
  size_t si = 0;
  int dowait = 0;

  for (i = 0; i < n; ++i)
    {
      const char *arg = argv[i];
      const char *p;
      switch (*arg)
	{
	case '-':
	  while (*++arg == '-');
	  if ((p = argcmp (arg, "time")))
	    {
	      if (*p == 0 && i + 1 < n)
		{
		  p = argv[++i];
		  msecs = strtoul (p, NULL, 10);
		  continue;
		}
	      else if (*p == '=')
		{
		  msecs = strtoul (++p, NULL, 10);
		  continue;
		}
	    }
	  else if ((p = argcmp (arg, "count")))
	    {
	      if (*p == 0 && i + 1 < n)
		{
		  p = argv[++i];
		  count = strtoul (p, NULL, 10);
		  continue;
		}
	      else if (*p == '=')
		{
		  count = strtoul (++p, NULL, 10);
		  continue;
		}
	    }
	  else if ((p = argcmp (arg, "pid")) || (p = argcmp (arg, "wait")))
	    {
	      dowait = 1;
	      continue;
	    }
	  else if ((p = argcmp (arg, "list")))
	    {
	      set_verbose (get_verbose () + 1);
	      continue;
	    }

	  fprintf (stderr, "invalid argument '%s'\n", argv[i]);
	  return 1;

	default:

	  if (si < sizec)
	    {
	      size = strtoul (argv[i], NULL, 10);
	      if (size == (size_t) -1)
		{
		  fprintf (stderr, "mashtable: invalid size '%s'\n", argv[i]);
		  return 1;
		}
	      sizev[si++] = size;
	      if (si < sizec)
		sizev[si] = 0;
	    }
	  else
	    {
	      fprintf (stderr, "warning: too many sizes '%s'\n", argv[i]);
	    }
	}
    }

  if (dowait)
    {
      fprintf (stderr, "PID: %u\n", getpid ());
      getchar ();
    }

  int rv = 0;
  for (i = 0; i < sizec; ++i)
    {
      size = sizev[i];

      if (size == 0)
	break;
      fprintf (stderr, ""
	      "size:   %lu\n"
	      "count:  %lu\n"
	      "msecs:  %lu\n",
	      size, count, msecs);

      if ((rv = test_balloc (size, count, msecs)) != 0)
	break;
    }

  return rv;
}
