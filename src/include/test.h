/*
 * test.h
 *
 * Created 19 Sep 2023 by Mason Hall (mason@mashpot.net)
 *
 * Copyright (C) 2023 Mason Hall
 */

#ifndef _TEST_H_
#define _TEST_H_

int get_verbose (void);
int set_verbose (int verbose);

int test_balloc (size_t maxblock, size_t nblocks, int msecs);

#endif /* _TEST_H_ */

