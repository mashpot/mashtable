/*
 * balloc.h
 *
 * Created 12 Aug 2023 by Mason Hall (mason@mashpot.net)
 *
 * Copyright (C) 2023 Mason Hall
 */

#ifndef BALLOC_H_
#define BALLOC_H_

#include <block-size.h>

extern void *bregion (bui_t size);

#define BA_NOEXPAND 1

extern bui_t balloc (void **pregion, size_t size, int flags);

extern void bfree (void *region, bui_t bi);

extern void bregion_print (void *region);

#endif /* BALLOC_H_ */

