/*
 * arch.h
 *
 * Created 13 Dec 2022 by Mason Hall (mason@mashpot.net)
 *
 * Copyright (C)  Mason Hall
 */

#ifndef ARCH_H_
#define ARCH_H_

#define ARCH_x86        1
#define ARCH_x86_64     2
#define ARCH_AARCH32    3
#define ARCH_AARCH64    4

/*
 * x86
 */
#if defined __i386__

#define ARCH ARCH_x86
#define ARCHSIZE 32

/*
 * x86_64
 */
#elif defined __x86_64__ || defined __amd64__

#define ARCH ARCH_x86_64

#if !defined __ILP32__
#define ARCHSIZE 64
#else
#define ARCHSIZE 32
#endif

/*
 * ARM
 */
#elif defined __arm__

#define ARCH ARCH_AARCH32
#define ARCHSIZE 32

/*
 * ARM64
 */
#elif defined __aarch64__

#define ARCH ARCH_AARCH64
#define ARCHSIZE 64

#if !defined __ILP32__
#define ARCHSIZE 64
#else
#define ARCHSIZE 32
#endif

#endif

#endif /* ARCH_H_ */

