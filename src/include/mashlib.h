/*
 * mashlib.h
 *
 * Created 29 Aug 2023 by Mason Hall (mason@mashpot.net)
 *
 * Copyright (C) 2023 Mason Hall
 */

#ifndef MASHLIB_H_
#define MASHLIB_H_

unsigned int getseed (void);

unsigned int getsseed (void);

unsigned long base10_ndigits (unsigned long n);

char *ulongstr (char *s, size_t len, unsigned long n);

double exp_decay (double x);

#endif /* MASHLIB_H_ */

