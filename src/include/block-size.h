/*
 * block-size.h
 *
 * Created 11 Oct 2023 by Mason Hall <mason@mashpot.net>
 *
 * Copyright (C) 2023 Mason Hall
 */

#ifndef _BLOCK_SIZE_H
#define _BLOCK_SIZE_H

typedef __UINT64_TYPE__ bui_t;
typedef __UINT64_TYPE__ boff_t;
typedef __UINT64_TYPE__ bsiz_t;

#define BUI_SZ 8

#define BLOCK_ALIGNMENT (2 * BUI_SZ)

#define BLOCK_ALIGN_MASK (BLOCK_ALIGNMENT - 1)

#define block_align(x) (((x) + BLOCK_ALIGN_MASK) & ~(BLOCK_ALIGN_MASK))

#endif /* _BLOCK_SIZE_H */

