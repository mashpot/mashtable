/*
 * mreg.h
 *
 * Created 2 Aug 2023 by Mason Hall (mason@mashpot.net)
 *
 * Copyright (C) 2023 Mason Hall
 */

#ifndef MREG_H_
#define MREG_H_

void *mralloc (size_t bytes);

void mrfree (void *ptr);

void *mrrealloc (void *ptr, size_t newsize);

int mregion_willremap (void *ptr, size_t size);

int mreg_test (void);

#endif /* MREG_H_ */

