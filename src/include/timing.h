/*
 * timing.h
 *
 * Created 29 Aug 2023 by Mason Hall (mason@mashpot.net)
 *
 * Copyright (C) 2023 Mason Hall
 */

#ifndef TIMING_H_
#define TIMING_H_

#define TIMING_CLKID CLOCK_MONOTONIC_RAW

#define NSECS_PER_SEC 1000000000
#define USECS_PER_SEC 1000000
#define MSECS_PER_SEC 1000

static inline __attribute__ ((__gnu_inline__))
struct timespec
timev_diff (struct timespec st, struct timespec et)
{
  struct timespec tvdiff;

  if (((long int) et.tv_nsec - (long int) st.tv_nsec) >= 0) {
      tvdiff.tv_sec = et.tv_sec - st.tv_sec;
      tvdiff.tv_nsec = et.tv_nsec - st.tv_nsec;
  } else {
      tvdiff.tv_sec = et.tv_sec - st.tv_sec - 1;
      tvdiff.tv_nsec = et.tv_nsec - st.tv_nsec + NSECS_PER_SEC;
  }
  return tvdiff;
}

static inline __attribute__ ((__gnu_inline__))
double
timev_secs (struct timespec tv)
{
  return ((double) tv.tv_sec) + ((double) tv.tv_nsec / 1000000000.0);
}

static inline __attribute__ ((__gnu_inline__))
double
timev_msecs (struct timespec tv)
{
  return ((double) tv.tv_sec * 1000.0) + ((double) tv.tv_nsec / 1000000.0);
}

static inline __attribute__ ((__gnu_inline__))
double
timev_usecs (struct timespec tv)
{
  return ((double) tv.tv_sec * 1000000.0) + ((double) tv.tv_nsec / 1000.0);
}

static inline __attribute__ ((__gnu_inline__))
double
timev_nsecs (struct timespec tv)
{
  return ((double) tv.tv_sec * 1000000000.0) + ((double) tv.tv_nsec);
}

#if 0
int
_test (void);
#endif

#endif /* TIMING_H_ */

