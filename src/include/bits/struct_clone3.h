/*
 * struct_clone3.h
 *
 * Created 2 Sep 2023 by Mason Hall (mason@mashpot.net)
 *
 * Copyright (C) 2023 Mason Hall
 */

#ifndef BITS_STRUCT_CLONE3_H_
#define BITS_STRUCT_CLONE3_H_

#if !defined SYS_CLONE3_H_ && !defined SYS_CLONE3_C_
# error "Never include <bits/struct_clone3.h> directly; use <sys/clone3.h> instead."
#endif

#include <linux/types.h>

#define sys_t __aligned_u64

struct clone3
{
  sys_t cl_flags;
  sys_t cl_pidfd;
  sys_t cl_child_tid;
  sys_t cl_parent_tid;
  sys_t cl_exit_signal;
  sys_t cl_stack;
  sys_t cl_stack_size;
  sys_t cl_tls;
  sys_t cl_set_tid;
  sys_t cl_set_tid_size;
  sys_t cl_cgroup;
};

#endif /* BITS_STRUCT_CLONE3_H_ */

