/*
 * linux_types.h
 *
 * Created 2 Sep 2023 by Mason Hall (mason@mashpot.net)
 *
 * Copyright (C) 2023 Mason Hall
 */

#ifndef BITS_LINUX_TYPES_H_
#define BITS_LINUX_TYPES_H_

#ifndef __ASSEMBLY__
# error ("Cannot include <linux_types.h> with __ASSEMBLY__ defined.")
#endif

//#include <linux/types.h>
#include <asm/types.h>

#define STR(s) _VAL(_STR(s))
#define _STR(s) #s
#define _VAL(s) s

#pragma message ("__aligned_u64" STR(__aligned_u64))

typedef __u64 sys_t
__attribute__((aligned(8)));

#endif /* BITS_LINUX_TYPES_H_ */

