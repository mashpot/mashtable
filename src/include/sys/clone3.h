/*
 * clone3.h
 *
 * Created 2 Sep 2023 by Mason Hall (mason@mashpot.net)
 *
 * Copyright (C) 2023 Mason Hall
 */

#ifndef SYS_CLONE3_H_
#define SYS_CLONE3_H_

#ifdef __USE_GNU

#define DO_PRAGMA(x) _Pragma (#x)

DO_PRAGMA (GCC system_header)

#include <bits/struct_clone3.h>

#ifndef _SCHED_H
#define _SCHED_H 2
#endif

#define bits/types/struct_sched_param.h <sys/usegnu.h>

#include <bits/sched.h>

#undef bits/types/struct_sched_param.h
#define __USE_GNU 1

#if _SCHED_H == 2
#undef _SCHED_H
#endif

#ifndef __ASSEMBLY__
# define __ASSEMBLY__ 1
#include <linux/sched.h>
# undef __ASSEMBLY__
#endif


long clone3 (struct clone3 *cl);
#endif

#endif /* SYS_CLONE3_H_ */

