/*
 * pidfd.h
 *
 * Created 3 Sep 2023 by Mason Hall (mason@mashpot.net)
 *
 * Copyright (C) 2023 Mason Hall
 */

#ifndef SYS_PIDFD_H_
#define SYS_PIDFD_H_

int pidfd_open (pid_t pid, unsigned int flags);

int pidfd_getfd (int pidfd, int targetfd, unsigned int flags);

int pidfd_send_signal (int pidfd, int sig, siginfo_t *info, unsigned int flags);

#endif /* SYS_PIDFD_H_ */

