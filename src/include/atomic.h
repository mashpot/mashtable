/*
 * atomic.h
 *
 * Created 4 Mar 2023 by Mason Hall (mason@mashpot.net)
 *
 * Copyright (C) 2023 Mason Hall
 */

#ifndef ATOMIC_H_
#define ATOMIC_H_

/*
 * The macros where inspired by "glibc/include/atomic.h".
 * From time to time I like to look at how things are done by GNU, I'll
 * usually learn of a new thing or way to do something. I must thank GNU for
 * not just what they stand for, but in providing me with the best educational
 * tools, content, and environment, I almost definitely would not be in as good
 * of a place without them. Thanks :)
 */

#define atomic_cmpxchg_acquire(mem, expected, desired) \
  ({ __atomic_compare_exchange_n((mem), (expected), (desired), 0, \
        __ATOMIC_ACQUIRE, __ATOMIC_RELAXED); })
#define atomic_cmpxchg_release(mem, expected, desired) \
  ({ __atomic_compare_exchange_n((mem), (expected), (desired), 0, \
        __ATOMIC_RELEASE, __ATOMIC_RELAXED); })
#define atomic_cmpxchg_relaxed(mem, expected, desired) \
  ({ __atomic_compare_exchange_n((mem), (expected), (desired), 0, \
        __ATOMIC_RELAXED, __ATOMIC_RELAXED); })

#define atomic_cmpxchg_weak_acquire(mem, expected, desired) \
  ({ __atomic_compare_exchange_n((mem), (expected), (desired), 1, \
        __ATOMIC_ACQUIRE, __ATOMIC_RELAXED); })
#define atomic_cmpxchg_weak_release(mem, expected, desired) \
  ({ __atomic_compare_exchange_n((mem), (expected), (desired), 1, \
        __ATOMIC_RELEASE, __ATOMIC_RELAXED); })
#define atomic_cmpxchg_weak_relaxed(mem, expected, desired) \
  ({ __atomic_compare_exchange_n((mem), (expected), (desired), 1, \
        __ATOMIC_RELAXED, __ATOMIC_RELAXED); })

#define atomic_load_relaxed(mem) \
  ({ __atomic_load_n((mem), __ATOMIC_RELAXED); })
#define atomic_load_acquire(mem) \
  ({ __atomic_load_n((mem), __ATOMIC_ACQUIRE); })
#define atomic_load_release(mem) \
  ({ __atomic_load_n((mem), __ATOMIC_RELEASE); })
#define atomic_load_consume(mem) \
  ({ __atomic_load_n((mem), __ATOMIC_CONSUME); })

#define atomic_store_release(mem, val) \
  ({ __atomic_store_n((mem), (val), __ATOMIC_RELEASE); })
#define atomic_store_relaxed(mem, val) \
  ({ __atomic_store_n((mem), (val), __ATOMIC_RELAXED); })

#define atomic_xchg_acquire(mem, val) \
  ({ __atomic_exchange_n((mem), (val), __ATOMIC_ACQUIRE); })
#define atomic_xchg_release(mem, val) \
  ({ __atomic_exchange_n((mem), (val), __ATOMIC_RELEASE); })
#define atomic_xchg_relaxed(mem, val) \
  ({ __atomic_exchange_n((mem), (val), __ATOMIC_RELAXED); })
#define atomic_xchg_acq_rel(mem, val) \
  ({ __atomic_exchange_n((mem), (val), __ATOMIC_ACQ_REL); })


/*
 * Atomic arithmetic fetch operations
 */

#define atomic_add_fetch_acquire(mem, val) \
  ({ __atomic_add_fetch((mem), (val), __ATOMIC_ACQUIRE); })
#define atomic_add_fetch_release(mem, val) \
  ({ __atomic_add_fetch((mem), (val), __ATOMIC_RELEASE); })
#define atomic_add_fetch_acq_rel(mem, val) \
  ({ __atomic_add_fetch((mem), (val), __ATOMIC_ACQ_REL); })
#define atomic_add_fetch_relaxed(mem, val) \
  ({ __atomic_add_fetch((mem), (val), __ATOMIC_RELAXED); })

#define atomic_sub_fetch_acquire(mem, val) \
  ({ __atomic_sub_fetch((mem), (val), __ATOMIC_ACQUIRE); })
#define atomic_sub_fetch_release(mem, val) \
  ({ __atomic_sub_fetch((mem), (val), __ATOMIC_RELEASE); })
#define atomic_sub_fetch_acq_rel(mem, val) \
  ({ __atomic_sub_fetch((mem), (val), __ATOMIC_ACQ_REL); })
#define atomic_sub_fetch_relaxed(mem, val) \
  ({ __atomic_sub_fetch((mem), (val), __ATOMIC_RELAXED); })

#define atomic_and_fetch_relaxed(mem, val) \
  ({ __atomic_and_fetch((mem), (val), __ATOMIC_RELAXED); })
#define atomic_or_fetch_relaxed(mem, val) \
  ({ __atomic_or_fetch((mem), (val), __ATOMIC_RELAXED); })
#define atomic_xor_fetch_relaxed(mem, val) \
  ({ __atomic_xor_fetch((mem), (val), __ATOMIC_RELAXED); })


/*
 * Atomic fetch arithmetic operations
 */

#define atomic_fetch_add_acquire(mem, val) \
  ({ __atomic_fetch_add((mem), (val), __ATOMIC_ACQUIRE); })
#define atomic_fetch_add_release(mem, val) \
  ({ __atomic_fetch_add((mem), (val), __ATOMIC_RELEASE); })
#define atomic_fetch_add_acq_rel(mem, val) \
  ({ __atomic_fetch_add((mem), (val), __ATOMIC_ACQ_REL); })
#define atomic_fetch_add_relaxed(mem, val) \
  ({ __atomic_fetch_add((mem), (val), __ATOMIC_RELAXED); })

#define atomic_fetch_sub_acquire(mem, val) \
  ({ __atomic_fetch_sub((mem), (val), __ATOMIC_ACQUIRE); })
#define atomic_fetch_sub_release(mem, val) \
  ({ __atomic_fetch_sub((mem), (val), __ATOMIC_RELEASE); })
#define atomic_fetch_sub_acq_rel(mem, val) \
  ({ __atomic_fetch_sub((mem), (val), __ATOMIC_ACQ_REL); })
#define atomic_fetch_sub_relaxed(mem, val) \
  ({ __atomic_fetch_sub((mem), (val), __ATOMIC_RELAXED); })

#define atomic_fetch_and_relaxed(mem, val) \
  ({ __atomic_fetch_and((mem), (val), __ATOMIC_RELAXED); })
#define atomic_fetch_xor_relaxed(mem, val) \
  ({ __atomic_fetch_xor((mem), (val), __ATOMIC_RELAXED); })
#define atomic_fetch_or_relaxed(mem, val) \
  ({ __atomic_fetch_or((mem), (val), __ATOMIC_RELAXED); })

#endif /* ATOMIC_H_ */
