/*
 * globdefs.h
 *
 * Created 31 Jul 2023 by Mason Hall (mason@mashpot.net)
 *
 * Copyright (C) 2023 Mason Hall
 */

#ifndef GLOBDEFS_H_
#define GLOBDEFS_H_

/* Defined in Makefile
#define DEBUG 1
*/

#if ARCH == ARCH_x86_64
# define __BREAKPOINT __asm__ ("int $3");
#elif 1
# define __BREAKPOINT __builtin_trap ();
#else
# include <signal.h>
# define __BREAKPOINT raise (SIGTRAP);
#endif

#define __FATAL __BREAKPOINT

#if DEBUG
# define __BREAK __BREAKPOINT
#else
# define __BREAK
#endif

extern void error_message (const char *restrict func, int line,
                           const char *restrict format, ...)
     __attribute__ ((format (printf, 3, 4)));

#define fatal_error(format, ...) do { \
    error_message (__func__, __LINE__, format"\n", ## __VA_ARGS__); \
    __FATAL \
    __builtin_unreachable (); \
} while (0)

#define fatal_perror(str) do { \
    int _errsv = errno; \
    error_message (__func__, __LINE__, "%s: %s %s\n", (str), \
                   strerrorname_np (_errsv), \
                   strerrordesc_np (_errsv)); \
    __FATAL \
    __builtin_unreachable (); \
} while (0)

#define printerrno(str) do { \
    int _errsv = errno; \
    error_message (__func__, __LINE__, "%s: %s %s\n", (str), \
                   strerrorname_np (_errsv), \
                   strerrordesc_np (_errsv)); \
} while (0)

#define warn_error(format, ...) do { \
    error_message (__func__, __LINE__, format"\n", ## __VA_ARGS__); \
    __BREAK \
} while (0)

#define printwarning(format, ...) do { \
    error_message (__func__, __LINE__, format"\n", ## __VA_ARGS__); \
} while (0)

#define warn_perror(str) do { \
    int _errsv = errno; \
    error_message (__func__, __LINE__, "%s: %s %s\n", (str), \
                   strerrorname_np (_errsv), \
                   strerrordesc_np (_errsv)); \
    __BREAK \
} while (0)

#define warn_message(msg) do { \
    void *raddr = __builtin_extract_return_addr (__builtin_return_address (0)); \
    fprintf (stderr, "%s:%i: %s @ ret: %p\n", __func__, __LINE__, msg, raddr); \
    __BREAK \
} while (0)


//#define __always_inline inline __attribute__ ((__always_inline__))


#define EXPORT __attribute__ ((visibility ("default")))

# define strong_alias(name, aliasname) _strong_alias(name, aliasname)
# define _strong_alias(name, aliasname) \
  extern __typeof (name) aliasname __attribute__ ((alias (#name))) \
    __attribute_copy__ (name);

/* WTF does glibc do in elf/rtld_static_init.c when defining the SHARED macro
 * which enabled the following macro and struct declarations.
 */

//#define __attribute_relro__ __attribute__ ((section (".data.rel.ro")))
#define __attribute_hidden__ __attribute__ ((visibility ("hidden")))
#define __attribute_pagesz__ __attribute__ ((aligned (4096)))

#define __attribute_grtopts__ \
  __attribute__ ((section (".data.rel.ro.grtopts")))

#define __attribute_rtopts__ \
  __attribute__ ((section (".data.rel.rtopts")))

struct grtopts
{
  size_t _op_pagesize;

  int _op_nprocs_conf;
  int _op_nprocs;

  size_t _op_open_max;
};

extern const struct grtopts _grtopts
    __attribute_grtopts__;

#define ROK	(0)
#define RERROR	(-1)
#define RINTR	(1)
#define RALRM	(2)

struct rtopts
{
  long _op_runcode;

  int _op_verbose;
};

extern struct rtopts _rtopts
    __attribute_rtopts__;

#define GROP(name) _grtopts._op_##name
#define RTOP(name) _rtopts._op_##name

#define PAGESIZE GROP (pagesize)
#define PAGE_SIZE GROP (pagesize)

#define align_pagesize(sz)			      \
      (((sz) + PAGESIZE - 1 < PAGESIZE		      \
	? PAGESIZE				      \
	: ((sz) + PAGESIZE - 1) & ~(PAGESIZE - 1)))

#define runcode RTOP (runcode)

#define verbose_level RTOP (verbose)

#endif /* GLOBDEFS_H_ */

