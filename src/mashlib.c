/*
 * mashlib.c
 *
 * Created 29 Aug 2023 by Mason Hall (mason@mashpot.net)
 *
 * Copyright (C) 2023 Mason Hall
 */

#include <stdio.h>
#include <stdlib.h>

#include <sys/random.h>
#include <time.h>
#include <math.h>

#include <globdefs.h>

unsigned int
getseed (void)
{
  unsigned int buf = 0;
  ssize_t n;

  n = getrandom (&buf, sizeof (buf), GRND_NONBLOCK);

  return n == -1 ? 0 : buf;
}

unsigned int
getsseed (void)
{
  struct timespec tv;
  clock_gettime (CLOCK_MONOTONIC_RAW, &tv);
  return (unsigned int) tv.tv_sec * tv.tv_nsec;
}

unsigned long
__attribute__ ((const))
base10_ndigits (unsigned long n)
{
  /*
   * The performace of the following statements was compared against log10.
   * Floating point log10 took 39438587 ns / 1000000 iterations, the if
   * statements only took 20067008 ns / 1000000 iterations; In total thats
   * about 19.4 ms difference. I believe can still be improved on by counting
   * the number of leading 0-bits in n, the value of which may be used in a
   * surounding switch statement that will jump to the lowest value if
   * statement for n's number of 0-bits.
   */
#if 0
  double l10 = log10 ((double) n) + 1.0;
  return (unsigned long) l10;
#else
  if (n < 10UL)
    return 1;
  else if (n < 100UL)
    return 2;
  else if (n < 1000UL)
    return 3;
  else if (n < 10000UL)
    return 4;
  else if (n < 100000UL)
    return 5;
  else if (n < 1000000UL)
    return 6;
  else if (n < 10000000UL)
    return 7;
  else if (n < 100000000UL)
    return 8;
  else if (n < 1000000000UL)
    return 9;
#if (__SIZEOF_LONG__ < 8)
  /* 2**32-1  =4294967295 */
  else return 10;
#else
  else if (n < 10000000000UL)
    return 10;
  else if (n < 100000000000UL)
    return 11;
  else if (n < 1000000000000UL)
    return 12;
  else if (n < 10000000000000UL)
    return 13;
  else if (n < 100000000000000UL)
    return 14;
  else if (n < 1000000000000000UL)
    return 15;
  else if (n < 10000000000000000UL)
    return 16;
  else if (n < 100000000000000000UL)
    return 17;
  else if (n < 1000000000000000000UL)
    return 18;
  else if (n < 10000000000000000000UL)
    return 19;
#if (__SIZEOF_LONG__ < 16)
  /* 2**64-1  =18446744073709551615 */
  else return 20;
#else
  else if (n < 100000000000000000000UL)
    return 20;
  else if (n < 1000000000000000000000UL)
    return 21;
  else if (n < 10000000000000000000000UL)
    return 22;
  else if (n < 100000000000000000000000UL)
    return 23;
  else if (n < 1000000000000000000000000UL)
    return 24;
  else if (n < 10000000000000000000000000UL)
    return 25;
  else if (n < 100000000000000000000000000UL)
    return 26;
  else if (n < 1000000000000000000000000000UL)
    return 27;
  else if (n < 10000000000000000000000000000UL)
    return 28;
  else if (n < 100000000000000000000000000000UL)
    return 29;
  else if (n < 1000000000000000000000000000000UL)
    return 30;
  else if (n < 10000000000000000000000000000000UL)
    return 31;
  else if (n < 100000000000000000000000000000000UL)
    return 32;
  else if (n < 1000000000000000000000000000000000UL)
    return 33;
  else if (n < 10000000000000000000000000000000000UL)
    return 34;
  else if (n < 100000000000000000000000000000000000UL)
    return 35;
  else if (n < 1000000000000000000000000000000000000UL)
    return 36;
  else if (n < 10000000000000000000000000000000000000UL)
    return 37;
  else if (n < 100000000000000000000000000000000000000UL)
    return 38;
  /* 2**128-1 =340282366920938463463374607431768211455 */
  else return 39;

#if (__SIZEOF_LONG__ > 16)
# error "Size of long is too long! See end of numdigits()."
#endif

#endif /* < 16 */

#endif /* < 8 */

#endif
}

char *
ulongstr (char *s, size_t len, unsigned long n)
{
  unsigned long c = base10_ndigits (n);
  char *p;

  if (len > c)
    {
      s += c;
      p = s;
      *p = 0;
    do
      {
        *--p = '0' + (n % 10);
      }
    while ((n /= 10) != 0);
    }
  return s;
}

/*
	:---:---:---:---:---:-- Exponential Decay --:---:---:---:---:---:
	|###								|
	|###								|
	|###								|
	|###								|
	|### ###							|
	|### ###							|
	|### ###							|
	|### ### ###							|
	|### ### ### ###						|
	|### ### ### ### ### ###					|
	|### ### ### ### ### ### ### ### ### ###			|
	:---:---:---:---:---:---:---:---:---:---:---:---:---:---:---:---:
	This graph represents the output of the following function.
 */

#define M_EA (0.06598803584531254262657284925808198750019073486328125)

double
exp_decay (double x)
{
  return (exp (-M_E * x) - M_EA) / (1.0 - M_EA);
}

EXPORT unsigned int getseed (void);

EXPORT unsigned int getsseed (void);

EXPORT unsigned long base10_ndigits (unsigned long n);

EXPORT char *ulongstr (char *s, size_t len, unsigned long n);

EXPORT double exp_decay (double x);

