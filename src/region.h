/*
 * region.h
 *
 * Created 5 Sep 2023 by Mason Hall (mason@mashpot.net)
 *
 * Copyright (C) 2023 Mason Hall
 */

#ifndef _REGION_H_
#define _REGION_H_

#include <block-size.h>

/*
 *
 */

int
region_init (void *mem, size_t size);

bui_t
region_malloc (void *region, size_t bytes);

void
region_free (void *region, bui_t bi);

bui_t
region_realloc (void *region, bui_t bi, size_t bytes);

void
region_print_bins (void *region);

void
region_print (void *region);

#endif /* _REGION_H_ */

