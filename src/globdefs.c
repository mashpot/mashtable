/*
 * globdefs.c
 *
 * Created 1 Aug 2023 by Mason Hall (mason@mashpot.net)
 *
 * Copyright (C) 2023 Mason Hall
 */

#define _GNU_SOURCE

#include <stdio.h>
#include <stdlib.h>
#include <string.h>
#include <errno.h>
#include <unistd.h>
#include <sys/mman.h>
#include <sys/sysinfo.h>
//#include <link.h>

#include <globdefs.h>

extern int
__mprotect (void *addr, size_t len, int prot);

const char interp_section[]
    __attribute__ ((section (".interp")));

const char interp_section[] = "/lib64/ld-linux-x86-64.so.2";


void soentry (void)
  __attribute__ ((used, cold));


void (* __rtopts_init_array []) (void)
    __attribute__ ((used, section (".init_array"), aligned (sizeof (void *))));

void _init_grtopts (void)
    __attribute__ ((used, section (".text.grtopts"), visibility ("default")));

void _init_rtopts (void)
    __attribute__ ((used, section (".text.rtopts"), visibility ("default")));


const struct grtopts _grtopts
  __attribute__ ((used));

struct rtopts _rtopts
  __attribute__ ((used));


void (* __rtopts_init_array []) (void) = {
    &_init_grtopts,
    &_init_rtopts,
};

void
_init_grtopts (void)
{
  struct grtopts opts;
  struct grtopts *grop;
  size_t pagealign;
  uptr addr;
  uptr size;
  void *mem;

  opts._op_nprocs_conf	= (size_t) get_nprocs_conf ();
  opts._op_nprocs	= (size_t) get_nprocs ();
  opts._op_pagesize	= (size_t) sysconf (_SC_PAGESIZE);
  opts._op_open_max	= (size_t) sysconf (_SC_OPEN_MAX);

  if (__builtin_expect ((long) opts._op_open_max <= 0, 0)
      || __builtin_expect ((long) opts._op_pagesize <= 0, 0))
    {
      warn_perror ("sysconf");
      fatal_error ("failed to initialize global runtime options");
      return;
    }

  pagealign = opts._op_pagesize - 1;

  grop = (struct grtopts*) (&_grtopts);

  addr = (uptr) (grop);
  size = addr + sizeof (struct grtopts);

  addr = (addr) & ~(pagealign);
  size = (size + pagealign) & ~(pagealign);
  size -= addr;

  mem = (void *) addr;

  if (__mprotect (mem, size, PROT_READ | PROT_WRITE) != 0)
    {
      warn_perror ("__mprotect");
      fatal_error ("failed to initialize global runtime options");
      return;
    }

#pragma GCC diagnostic push
#pragma GCC diagnostic ignored "-Wanalyzer-write-to-const"
  *grop = opts;
#pragma GCC diagnostic pop

  if (__mprotect (mem, size, PROT_READ) != 0)
    {
      warn_perror ("__mprotect");
      fatal_error ("failed to initialize global runtime options");
      return;
    }

  return;
}

void
_init_rtopts (void)
{
  _rtopts._op_verbose = 0;
  _rtopts._op_runcode = ROK;

  return;
}

void
__attribute__ ((noreturn))
soentry (void)
{
  static const char errmsg[] = {
      "libmashtable.so: cannot be run as an executable\n"
  };
  static const size_t len = sizeof (errmsg) - 1;
  const int fd = STDERR_FILENO;

  TEMP_FAILURE_RETRY (write (fd, &errmsg[0], len));

  exit (1);
}

