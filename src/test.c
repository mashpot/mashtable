/*
 * test.c
 *
 * Created 29 Aug 2023 by Mason Hall (mason@mashpot.net)
 *
 * Copyright (C) 2023 Mason Hall
 */

#define _GNU_SOURCE

#include <stdio.h>
#include <stdlib.h>
#include <string.h>
#include <unistd.h>
#include <signal.h>
#include <errno.h>
#include <time.h>
#include <math.h>
#include <malloc.h>
#include <sys/resource.h>

#include <globdefs.h>
#include <mashlib.h>
#include <mreg.h>
#include <timing.h>
#include "region.h"

/*
 * Control where debug messages are written. We use STDERR to seperate the
 * output from region_print and region_print_bins, which is on STDOUT to ease
 * piping to a file for further examination.
 */
//static FILE *msgout = stderr;

/* Macro so we to standardize how we print */
#define print(fmt, ...) \
  fprintf (stderr, fmt, ## __VA_ARGS__);

/*
 * Routines for accessing verbosity level from outside the DSO.
 */
int
__attribute__ ((pure))
get_verbose (void)
{
  return verbose_level;
}

int
set_verbose (int verbose)
{
  return (verbose_level = verbose);
}

EXPORT int get_verbose (void);
EXPORT int set_verbose (int verbose);

/*
 * Setup and handle timer signal...
 */
#define TIMER_INTVAL_MSEC 666

#define TIMER_ITV_SEC (TIMER_INTVAL_MSEC / 1000)
#define TIMER_ITV_NSEC ((TIMER_INTVAL_MSEC % 1000) * 1000000)

static const struct itimerspec its = {
    .it_value.tv_sec	  = TIMER_ITV_SEC,
    .it_value.tv_nsec	  = TIMER_ITV_NSEC,
    .it_interval.tv_sec	  = TIMER_ITV_SEC,
    .it_interval.tv_nsec  = TIMER_ITV_NSEC,
};

static timer_t timerid = NULL;
static long int timer_msecs = 0;

void
signal_handler (int sig, siginfo_t *info, void *ucontext)
{
  (void) info;
  (void) ucontext;

  if (runcode == ROK)
   switch (sig)
    {
    case SIGALRM:
      timer_msecs -= TIMER_INTVAL_MSEC;
      //timer_msecs -= timer_nsecs / 1000000;
      //timer_nsecs %= 1000000;

      //printf ("%li\n", timer_msecs);

      if (timer_msecs > 0 && timer_settime (timerid, 0, &its, NULL) == -1)
	  fatal_perror ("timer_settime()");
      else if (timer_msecs <= 0)
	  runcode = RALRM;
      break;

    default:
      runcode = RINTR;
    }

  return;
}

void
setup_signals (int msecs)
{
  struct sigaction sa;
  struct sigevent sev;
  sigset_t mask;

  runcode = ROK;

  sa = (struct sigaction) {
      .sa_flags = SA_SIGINFO,
      .sa_sigaction = &signal_handler,
  };
  sigemptyset (&sa.sa_mask);
  sigaddset (&sa.sa_mask, SIGALRM);

  if (sigaction (SIGINT, &sa, NULL) == -1)
    fatal_perror ("sigaction()");

  if (msecs < 0)
    return;

  sigemptyset (&mask);
  sigaddset (&mask, SIGALRM);

if (sigprocmask (SIG_BLOCK, &mask, NULL) == -1)
    fatal_perror ("sigprocmask()");

  sa = (struct sigaction) {
      .sa_flags = SA_SIGINFO,
      .sa_sigaction = &signal_handler,
  };
  sigemptyset (&sa.sa_mask);
  sigaddset (&sa.sa_mask, SIGINT);

  if (sigaction (SIGALRM, &sa, NULL) == -1)
    fatal_perror ("sigaction()");

  if (timerid == NULL)
    {
      sev = (struct sigevent) {
	  .sigev_notify = SIGEV_SIGNAL,
	  .sigev_signo = SIGALRM,
	  .sigev_value.sival_ptr = &timerid,
      };
      if (timer_create (CLOCK_MONOTONIC, &sev, &timerid) == -1)
	fatal_perror ("timer_create()");
    }

  timer_msecs = msecs;

  if (timer_settime (timerid, 0, &its, NULL) == -1)
    fatal_perror ("timer_settime()");

  if (sigprocmask (SIG_UNBLOCK, &mask, NULL) == -1)
    fatal_perror ("sigprocmask()");

  return;
}

/*
 * A whole lot of mess is below this point. Testing can change a lot and
 * quickly, especially when wanting to quickly test changes make to the
 * allocator.
 */
#define TIMING 1
#define TIME_ALLOC 1
#define TIME_FREE 1

#define ALC_RALLOC	1
#define ALC_MALLOC	2

#define ALLOCATOR ALC_RALLOC
//#define ALLOCATOR ALC_MALLOC

#if TIMING

#include <x86intrin.h>

#define flushcache(p, size)					\
 ( __extension__ ({						\
    void *__p = (p);						\
    const void *const __end = __p + (size);			\
								\
    do								\
      _mm_clflush (__p);					\
    while (((__p) += 64) < __end);				\
  }))

#define TIMING_BEGIN \
  long int __results; \
  { \
    unsigned long int __count = 0; \
    long int __secs = 0; \
    long int __nsecs = 0;


#if 1
#define timing(expression) \
(__extension__ \
 ({ struct timespec __st, __et; \
    \
    if (__builtin_expect (clock_gettime (TIMING_CLKID, &__st) == -1, 0)) \
      { \
	fatal_perror ("clock_gettime"); \
      } \
    \
    __auto_type __retval = (expression); \
    \
    if (__builtin_expect (clock_gettime (TIMING_CLKID, &__et) == -1, 0)) \
      { \
	fatal_perror ("clock_gettime"); \
      } \
    \
      { \
	__st = timev_diff (__st, __et); \
	unsigned long int __n = __nsecs + __st.tv_nsec; \
	__secs += __st.tv_sec + (__n / NSECS_PER_SEC); \
	__nsecs = __n % NSECS_PER_SEC; \
	__count++; \
      } \
    \
    __retval; \
 }))

#define TIMING_END \
    __results = ((__secs * NSECS_PER_SEC) / __count) + (__nsecs / __count); \
  }
#else

#define timing(expression) \
(__extension__ \
 ({ /*struct timespec __st, __et;*/ \
    \
    unsigned long __st, __et; \
    /*_mm_mfence ();*/ \
    _mm_lfence (); \
    \
    /*if (__builtin_expect (clock_gettime (TIMING_CLKID, &__st) == -1, 0))*/ \
      /*fatal_perror ("clock_gettime");*/ \
      \
    __st = __rdtsc (); \
    \
    _mm_lfence (); \
    \
    __auto_type __retval = (expression); \
    \
    /*_mm_mfence ();*/ \
    _mm_lfence (); \
    __et = __rdtsc (); \
    _mm_lfence (); \
    \
    /*if (__builtin_expect (clock_gettime (TIMING_CLKID, &__et) == -1, 0))*/ \
      /*fatal_perror ("clock_gettime");*/ \
    \
      { \
	/*__st = timev_diff (__st, __et);*/ \
	/*long int __n = __nsecs + __st.tv_nsec;*/ \
	unsigned long int __n = __nsecs + (__et - __st); \
	__secs += __n / NSECS_PER_SEC; \
	__nsecs = __n % NSECS_PER_SEC; \
	/*__secs += __st.tv_sec + (__n / NSECS_PER_SEC);*/ \
	/*__nsecs = __st.tv_sec + (__n % NSECS_PER_SEC);*/ \
	__count++; \
      } \
    \
    __retval; \
 }))

#define TIMING_END \
    __results = ((__secs * NSECS_PER_SEC) / __count) + (__nsecs / __count); \
  }

#endif

#else

#define TIMING_BEGIN \
  {

#define timing(expression) \
 ( __extension__ ({ __auto_type __retval = (expression); \
    __retval; }))

#define TIMING_END \
  }

#endif


#include "rbtree.c"

static simblock *
init_blocks (const size_t count)
{
  simblock *const blocks = malloc (sizeof (simblock) * count);

  if (blocks != 0)
    {
      const simblock *const e = blocks + count;
      simblock *p = blocks;
      simblock *bwd = 0;

      while (p < e)
	{
	  p->dtick = 0;
	  p->fwd = p + 1 < e ? p + 1 : 0;
	  p->bwd = bwd;
	  p->rb_left = 0;
	  p->rb_right = 0;
	  p->__rb_parent_color = 0;
	  p->bytes = 0;
	  p->bi = 0;
	  bwd = p++;
	}
    }

  return blocks;
}

static unsigned long
genblocksize (unsigned long maxsz)
{
  return (unsigned long) floor (exp_decay (drand48 ()) * (double) maxsz);
}

static simblock *
new_alloc_block (const size_t tick,
		 const size_t maxsize,
		 const size_t nblocks,
		 simblock **const dlist,
		 simblock **const alist,
		 simblock **const atree)
{
  simblock *p = *dlist;
  simblock *fwd;
  size_t dtick;

  if (p == 0)
    return 0;

  *dlist = fwd = p->fwd;

  if (fwd)
    fwd->bwd = 0;

  p->bytes = genblocksize (maxsize - 16) + 16;
  p->dtick = dtick = tick + ((maxsize/16*nblocks) - genblocksize (maxsize/16*nblocks));
  p->fwd = 0;
  p->bwd = 0;
  //printf ("sz: %lu\n", p->bytes);
  p->bi = 0;

  atree_insert (p, atree, alist);

  return p;
}

void
printblks (unsigned long nblk, unsigned long maxblk, char idchr)
{
  unsigned long i, n;
  static char buf[] = {
      "\033[1K\r[ ] 100%\t99999999\t[________________________________]"
  };
  char *p;

  if (idchr != 0)
    buf[6] = idchr;

  n = (unsigned long) roundf(((float) nblk / (float) maxblk) * 100.0f);
  p = &buf[11];
  *p-- = '0' + (n % 10);
  *p-- = n < 10 ? ' ' : (n /= 10, ('0' + (n % 10)));
  *p-- = n < 10 ? ' ' : (n /= 10, ('0' + (n % 10)));
  p = &buf[21];
  n = nblk;
  *p-- = '0' + (n % 10);
  *p-- = n < 10 ? ' ' : (n /= 10, ('0' + (n % 10)));
  *p-- = n < 10 ? ' ' : (n /= 10, ('0' + (n % 10)));
  *p-- = n < 10 ? ' ' : (n /= 10, ('0' + (n % 10)));
  *p-- = n < 10 ? ' ' : (n /= 10, ('0' + (n % 10)));
  *p-- = n < 10 ? ' ' : (n /= 10, ('0' + (n % 10)));
  *p-- = n < 10 ? ' ' : (n /= 10, ('0' + (n % 10)));
  *p-- = n < 10 ? ' ' : '+';
  p = &buf[24];

  n = (unsigned long) roundf(((float) nblk / (float) maxblk) * 32.0f);
  for (i = 0; i < 32; ++i)
    *p++ = i < n ? '#' : '_';

  int rv = write (STDERR_FILENO, &buf[0], sizeof (buf)/sizeof (buf[0]) - 1);
  (void) rv;
}

static unsigned int seed =
#if 0
			   0;
#else
			   253768458;
#endif

__attribute__ ((constructor))
static void
setup_seed (void)
{
  if (seed == 0)
#if 0
    seed = getseed ();
#else
    seed = getsseed ();
#endif
  fprintf (stderr, "seed: %u\n", seed);
}

extern void
region_print (void *mreg);

int
test_balloc (size_t maxblock, size_t nblocks, int msecs)
{
  simblock *blocks;
  simblock *dlist;
  simblock *alist;
  simblock *atree;
  simblock *p;
  bui_t bi;
  size_t size;
  sigset_t mask;

  //maxblock = align_up (maxblock, 4);
  maxblock = maxblock < 24 ? 24 : maxblock;
  nblocks = align_up (nblocks, 4);
  alist = atree = 0;
  dlist = blocks = init_blocks (nblocks);
  if (!blocks)
    {
      warn_perror ("malloc");
      return -1;
    }

  srand (seed);
  srand48 (seed);

#if ALLOCATOR == ALC_RALLOC
  void *region;
  size_t rsize = nblocks * ((maxblock > 32 ? maxblock : 32) + 16) + 4096;

  rsize = align_pagesize (rsize);
  if (RTOP (verbose))
    fprintf (stderr, "region_size: %lu\n", rsize);
  region = mralloc (rsize);

  if (region == NULL)
    {
      free (blocks);
      return -1;
    }

  if (region_init (region, rsize) != 0)
    {
      free (blocks);
      mrfree (region);
      return -1;
    }
#elif ALLOCATOR == ALC_MALLOC
  if (mallopt (M_ARENA_MAX, 1) != 1)
    {
      fprintf (stderr, "mallopt() failed\n");
      return -1;
    }
  if (mallopt (M_MMAP_MAX, 0) != 1)
    {
      fprintf (stderr, "mallopt() failed\n");
      return -1;
    }
  if (mallopt (M_TRIM_THRESHOLD, -1) != 1)
    {
      fprintf (stderr, "mallopt() failed\n");
      return -1;
    }
#endif

  setup_signals (msecs);

  sigemptyset (&mask);
  sigaddset (&mask, SIGALRM);

  if (sigprocmask (SIG_UNBLOCK, &mask, NULL) == -1)
    fatal_perror ("sigprocmask");

#if 0
  const long int itv_msecs = TIMER_INTVAL_MSEC + (TIMER_INTVAL_MSEC >> 2);
  long int next_msecs = timer_msecs - itv_msecs;
#endif

  unsigned long tick = 0;
#define COUNT 0
#if COUNT
  unsigned long count = 0;
  unsigned long ncount = 0;
  unsigned long mcount = nblocks >> 7;
  if (mcount < 65536)
    mcount = 65536;
#endif

  TIMING_BEGIN
  do
    {
      while (alist && tick >= alist->dtick)
	{
	  p = alist;
	  atree_remove (p, &atree, &alist);
	  bi = p->bi;
	  tick += p->bytes >> 4;

	  if (sigprocmask (SIG_BLOCK, &mask, NULL) == -1)
	    fatal_perror ("sigprocmask");

#if ALLOCATOR == ALC_RALLOC
# if TIME_FREE
          (void) timing (({ region_free (region, bi); (int) 1; }));
# else
          region_free (region, bi);
# endif
#elif ALLOCATOR == ALC_MALLOC
# if TIME_FREE
          (void) timing (({ free ((void*)bi); (int) 1; }));
# else
          free ((void *) bi);
# endif
#endif
#if 0
	  if (runcode == ROK && timer_msecs < next_msecs
	      && (next_msecs = timer_msecs - itv_msecs) > 0
	      && verbose_level > 1)
	    region_print (region);
#endif
#if COUNT
	  if (ncount > mcount)
	    {
	      ncount = 0;
	      printblks (count, nblocks, '-');
#if 0
	      fprintf (stderr, "\033[1K\r[-]%3.0f%%\t%8lu\t",
		       ((float) count / (float) nblocks) * 100.0,
		       count);
	      int i,nc = (int) floor(((float) count / (float) nblocks)*32.0);
	      for (i = 0; i < 32; ++i)
		putc (i < nc ? '#' : '_', stderr);
#endif
	    }
#endif
	  if (sigprocmask (SIG_UNBLOCK, &mask, NULL) == -1)
	    fatal_perror ("sigprocmask");

	  if (runcode != ROK)
	    break;

	  if (dlist)
	      dlist->bwd = p;
	  p->fwd = dlist;
	  p->bwd = 0;
	  dlist = p;
#if COUNT
	  --count;
	  ++ncount;
#endif
        }

      if (runcode != ROK)
	break;

      if (rand () % 3 == 1 && alist
	  && (p = new_alloc_block (tick, maxblock, nblocks,
				   &dlist, &alist, &atree)))
	{
	  simblock *pa = alist;
	  atree_remove (pa, &atree, &alist);
	  bi = pa->bi;
	  tick += pa->bytes >> 4;

	  if (dlist)
	      dlist->bwd = pa;
	  pa->fwd = dlist;
	  pa->bwd = 0;
	  dlist = pa;

	  size = p->bytes;
	  tick += size;

	  if (sigprocmask (SIG_BLOCK, &mask, NULL) == -1)
	    fatal_perror ("sigprocmask");

	  bi = timing (region_realloc (region, bi, size));

	  if (sigprocmask (SIG_UNBLOCK, &mask, NULL) == -1)
	    fatal_perror ("sigprocmask");

	  if (runcode != ROK)
	    break;

	  p->bi = bi;

	  if (bi == 0)
	    {
	      warn_error ("region_realloc(): size %lu\n", size);
	      runcode = RERROR;
	      break;
	    }
	}

      int nrnd = (rand () % (nblocks >> 1)) + (nblocks >> 1);
      int ni = 0;

      if (dlist == 0)
	{
	  if (!alist)
	    {
	      warn_error ("no more blocks!\n");
	      runcode = RALRM;
	    }
	  else if (atree)
	    {
	      p = atree_maximum (atree);
	      tick = p->dtick;
	    }
	  else
	    tick += maxblock;
	}
      else while (ni++ < nrnd
		  && (p = new_alloc_block (tick, maxblock, nblocks,
					   &dlist, &alist, &atree)))
	{
	  size = p->bytes;
	  tick += size;

	  if (sigprocmask (SIG_BLOCK, &mask, NULL) == -1)
	    fatal_perror ("sigprocmask");
#if ALLOCATOR == ALC_RALLOC
# if TIME_ALLOC
	  bi = timing (region_malloc (region, size));
# else
	  bi = region_malloc (region, size);
# endif
#elif ALLOCATOR == ALC_MALLOC
# if TIME_ALLOC
	  bi = timing ((bui_t) malloc (size));
# else
	  bi = (bui_t) malloc (size);
# endif
#endif
#if 0
	  if (runcode == ROK && timer_msecs < next_msecs
	      && (next_msecs = timer_msecs - itv_msecs) > 0
	      && verbose_level > 1)
	    region_print (region);
#endif
#if COUNT
	  if (ncount > mcount)
	    {
	      ncount = 0;
	      printblks (count, nblocks, '+');
#if 0
	      fprintf (stderr, "\033[1K\r[+]%3.0f%%\t%8lu\t",
		       ((float) count / (float) nblocks) * 100.0,
		       count);
	      int i,nc = (int) floor(((float) count / (float) nblocks)*32.0);
	      for (i = 0; i < 32; ++i)
		putc (i < nc ? '#' : '_', stderr);
#endif
	    }
#endif
	  if (sigprocmask (SIG_UNBLOCK, &mask, NULL) == -1)
	    fatal_perror ("sigprocmask");

	  if (runcode != ROK)
	    break;

	  if (bi == 0)
	    {
#if ALLOCATOR == ALC_RALLOC
# if 0
	      if (region)
		region_print (region);
# endif
	      warn_error ("region_malloc(): size %lu\n", size);
#elif ALLOCATOR == ALC_MALLOC
	      warn_perror ("malloc");
#endif
	      runcode = RERROR;
	      break;
	    }

	  p->bi = bi;
#if COUNT
	  ++count;
	  ++ncount;
#endif
	}
    }
  while (runcode == ROK);
  TIMING_END

#if COUNT
  printblks (count, nblocks, 0);
  putc ('\n', stderr);
#endif

#if ALLOCATOR == ALC_RALLOC
  if (verbose_level)
    {
      if (verbose_level > 1)
	region_print (region);
      region_print_bins (region);
    }
#endif

  if (verbose_level)
    {
      struct rusage usage;
      if (getrusage (RUSAGE_SELF, &usage) == -1)
	perror ("getrusage");
      else
	{
	  fprintf (stderr,
		   "utime:\t%f\n"
		   "stime:\t%f\n"
		   "mrss:\t%li\n",
		   (double) usage.ru_utime.tv_sec + ((double) usage.ru_utime.tv_usec / 1000000.0),
		   (double) usage.ru_stime.tv_sec + ((double) usage.ru_stime.tv_usec / 1000000.0),
		   usage.ru_maxrss - ((sizeof (simblock)*nblocks))/1000);
	}
    }

#if TIMING
  while (fprintf (stderr, "timing: %ld nsecs\n\n", __results) < 0);
#endif

#if ALLOCATOR == ALC_RALLOC
  mrfree (region);
#elif ALLOCATOR == ALC_MALLOC
  while (alist)
    {
      p = alist;
      atree_remove (p, &atree, &alist);
      bi = p->bi;
      free ((void*)bi);
    }
#endif

  free (blocks);

  if (runcode == RALRM)
      runcode = 0;
  return runcode;
}

EXPORT int test_balloc (size_t maxblock, size_t nblocks, int msecs);

#if 0
typedef struct simblock
{
  size_t dtick;

  struct simblock *fwd;
  struct simblock *bwd;

  struct simblock *bst_left;
  struct simblock *bst_right;
  struct simblock *bst_parent;

  size_t bytes;
  union {
      bui_t bi;
      void *ptr;
  };
} simblock;

static simblock *
init_blocks (const size_t count)
{
  simblock *const blocks = malloc (sizeof (simblock) * count);

  if (blocks != 0)
    {
      const simblock *const e = blocks + count;
      simblock *p = blocks;
      simblock *bwd = 0;

      while (p < e)
	{
	  p->dtick = 0;
	  p->fwd = p + 1 < e ? p + 1 : 0;
	  p->bwd = bwd;
	  p->bst_left = 0;
	  p->bst_right = 0;
	  p->bst_parent = 0;
	  p->bytes = 0;
	  p->bi = 0;
	  bwd = p++;
	}
    }

  return blocks;
}

static __always_inline void
atree_set_parent (simblock *const node, simblock *const parent)
{
  node->bst_parent = parent;
}

static void
atree_insert (simblock *const p, simblock **const atree, simblock **const alist)
{
  const size_t dtick = p->dtick;
  simblock **link = atree;
  simblock *tmp, *node;
  simblock *parent = 0;
  long int cmp;

  p->bst_left = 0;
  p->bst_right = 0;
  p->bst_parent = 0;

  while ((node = *link) && (cmp = (long int) dtick - (long int) node->dtick) != 0)
    {
      if (cmp < 0)
        link = &node->bst_left;
      else
        link = &node->bst_right;
      parent = node;
    }

  if (node == 0)
    {
      if (link == atree && *alist != 0)
	warn_error ("alist != 0");
      *link = p;
      atree_set_parent (p, parent);

      if (!parent)
	{
	  p->fwd = 0;
	  p->bwd = 0;
	  if (*alist != 0)
	    warn_error ("node != 0");
	  *alist = p;
	}
      else
	{
	  if (cmp < 0)
	    {
	      p->fwd = parent;
	      p->bwd = tmp = parent->bwd;
	      parent->bwd = p;

	      if (tmp)
		tmp->fwd = p;
	      else
		{
		  if (parent != *alist)
		    warn_error ("parent != alist");
		  *alist = p;
		}
	    }
	  else
	    {
	      tmp = parent->fwd;
	      while (tmp && tmp->dtick == parent->dtick)
		{
		  parent = tmp;
		  tmp = parent->fwd;
		}
	      p->bwd = parent;
	      p->fwd = tmp;
	      parent->fwd = p;

	      if (tmp)
		tmp->bwd = p;
	    }
	}
    }
  else
    {
      p->bwd = node;
      p->fwd = tmp = node->fwd;
      node->fwd = p;

      if (tmp)
	tmp->bwd = p;
    }

  if (p->fwd && p->fwd->dtick < p->dtick)
    warn_error ("mismatch");
  if (p->bwd && p->bwd->dtick > p->dtick)
    warn_error ("mismatch");
}

static __always_inline void
__atree_change_child (simblock *const old, simblock *const new,
		      simblock *const parent, simblock **const root)
{
  if (parent)
    {
      if (parent->bst_left == old)
	parent->bst_left = new;
      else
	parent->bst_right = new;
    }
  else
    {
      if (*root != 0 && *root != old)
	warn_perror ("root != old");
      *root = new;
    }
}

static void
atree_remove (simblock *const node, simblock **const atree, simblock **const alist)
{
  simblock *parent, *tmp, *child, *fwd, *bwd;

  fwd = node->fwd;
  bwd = node->bwd;
  parent = node->bst_parent;
  child = node->bst_right;
  tmp = node->bst_left;

  if (fwd)
    {
      fwd->bwd = bwd;

      if (fwd->dtick == node->dtick)
	{
	  if (bwd)
	    bwd->fwd = fwd;
	  else
	    {
	      if (node != *alist)
		warn_error ("node != alist");
	      *alist = fwd;
	    }

	  atree_set_parent (fwd, parent);
	  __atree_change_child (node, fwd, parent, atree);
	  fwd->bst_right = child;
	  fwd->bst_left = tmp;
	  if (child)
	    atree_set_parent (child, fwd);
	  if (tmp)
	    atree_set_parent (tmp, fwd);
	  return;
	}
    }

  if (bwd)
    bwd->fwd = fwd;
  else
    {
      if (node != *alist)
	warn_error ("node != alist");
      *alist = fwd;
    }

  if (!tmp)
    {
      if (child)
        atree_set_parent (child, parent);
      __atree_change_child (node, child, parent, atree);
    }
  else if (!child)
    {
      if (tmp)
	atree_set_parent (tmp, parent);
      __atree_change_child (node, tmp, parent, atree);
    }
  else
    {
      simblock *successor = child, *child2;
      tmp = child->bst_left;

      if (!tmp)
	{
	  /*
	   * Case 2: node's successor is its right child
	   *
	   *    (n)          (s)
	   *    / \          / \
	   *  (x) (s)  ->  (x) (c)
	   *        \
	   *        (c)
	   */
	  parent = successor;
	  child2 = successor->bst_right;
	}
      else
	{
	  /*
	   * Case 3: node's successor is leftmost under
	   * node's right child subtree
	   *
	   *    (n)          (s)
	   *    / \          / \
	   *  (x) (y)  ->  (x) (y)
	   *      /            /
	   *    (p)          (p)
	   *    /            /
	   *  (s)          (c)
	   *    \
	   *    (c)
	   */
	  do {
	      parent = successor;
	      successor = tmp;
	      tmp = tmp->bst_left;
	  } while (tmp);
	  child2 = successor->bst_right;
	  parent->bst_left = child2;
	  successor->bst_right = child;
	  atree_set_parent (child, successor);
	}

      tmp = node->bst_left;
      successor->bst_left = tmp;
      atree_set_parent (tmp, successor);

      if (child2)
	atree_set_parent (child2, parent);

      tmp = node->bst_parent;
      __atree_change_child (node, successor, tmp, atree);
      atree_set_parent (successor, tmp);
    }

  node->bst_left = 0;
  node->bst_right = 0;
  node->bst_parent = 0;
}

static __always_inline __attribute__ ((pure))
simblock *
atree_maximum (simblock *node)
{
  simblock *child = node->bst_right;

  while (child)
    {
      node = child;
      child = node->bst_right;
    }

  return node;
}
#endif

