/*
 * balloc.c
 *
 * Created 11 Aug 2023 by Mason Hall (mason@mashpot.net)
 *
 * Copyright (C) 2023 Mason Hall
 */

#include <stdio.h>
#include <stdlib.h>
#include <string.h>

#include <globdefs.h>
#include <mreg.h>
#include <balloc.h>


#define _CAT(A, B) A #B
#define CAT(A, B) _CAT (A, B)

#define SSB 1

typedef struct _mchunk
{
  bui_t _chunk_prevsize;

  bui_t _chunk_size;

  bui_t fwd;

  bui_t bwd;

} mchunk;

#define CHK_MIN_SZ (sizeof (struct _mchunk))

#define CHK_MIN_MASK (CHK_MIN_SZ - 1)

#define CHK_MEM_OFF (__builtin_offsetof (mchunk, fwd))

#define CHK_ALIGN (sizeof (bui_t) * 2)

#define CHK_ALIGN_MASK (CHK_ALIGN - 1)

#define req2size(sz) \
  (((sz) + CHK_MEM_OFF <= CHK_MIN_SZ) ? \
   CHK_MIN_SZ                         : \
   ((sz) + CHK_MEM_OFF + CHK_ALIGN_MASK) & ~(CHK_ALIGN_MASK))

#define SSB_SIZE (CHK_MIN_SZ)

#if 1
# define UNITE_THRESHOLD (CHK_MIN_SZ * 2)
#else   /* testing value */
# define UNITE_THRESHOLD ((CHK_MIN_SZ * CHK_MIN_SZ) >> 1)
//# define UNITE_THRESHOLD (1024)
#endif

/* leave the low n bits for flags */
#define CHK_NFLAGS (2)

/* bit set if previous chunk in use */
#define PREV_INUSE (0x1)

#define prev_inuse(p) ((p)->_chunk_size & PREV_INUSE)

/* other bit flag is reserved and not currently being used */
#define CHK_FLAG_RESV (0x2)

/* bit flag mask */
#define CHK_FLAG_MASK (PREV_INUSE | CHK_FLAG_RESV)

/* extract size without flag bits */
#define chunksize(p) ((p)->_chunk_size & ~(CHK_FLAG_MASK))

/* return previous chunk size */
#define prev_size(p) ((p)->_chunk_prevsize)

#define set_prev_size(p, sz) ((p)->_chunk_prevsize = (sz))

/* moving around */
#define next_chunk(p) ((mchunk*)(((char*)(p)) + chunksize (p)))

#define prev_chunk(p) ((mchunk*)(((char*)(p)) - prev_size (p)))

/* set chunk size at head */
#define set_size_head(p, sz) ((p)->_chunk_size = (((p)->_chunk_size & CHK_FLAG_MASK) | (sz)))

/* set chunk size at foot */
#define set_size_foot(p, sz) ((next_chunk (p))->_chunk_prevsize = (sz))

/* set/clear inuse flag */
#define set_inuse(p) ((next_chunk (p))->_chunk_size |= PREV_INUSE)

#define clr_inuse(p) ((next_chunk (p))->_chunk_size &= ~(PREV_INUSE))

#define is_inuse(p) ((next_chunk (p))->_chunk_size & PREV_INUSE)

/* set head size/flags */
#define set_head(p, s) ((p)->_chunk_size = (s))

#define get_head(p) ((p)->_chunk_size)

/* set foot size */
#define set_foot(p, s) ((next_chunk (p))->_chunk_prevsize = (s))

#pragma GCC poison _chunk_size
#pragma GCC poison _chunk_prevsize

/* get chunk address from index/offset */
#define chunk(p, i) ((mchunk*)(((char*)(p)) + (i)))

#define chunkindex(reg, p) (((char*)(p)) - ((char*)(reg)))

/*
 * ------------------------------- memory region ------------------------------
 */

typedef struct _mregion
{
  bui_t size;

  bui_t bi_top;

  bui_t bi_binfwd;
  bui_t bi_binbwd;

  bui_t bi_ssbfwd;
  bui_t bi_ssbbwd;
  bui_t bi_ssbmax;

  /* Total number of in use and free chunks */
  bui_t nchunks;

  /* Number of bytes in use */
  bui_t bused;

  /* Number of used chunks */
  bui_t nused;

} mregion;

#define _CAT(A, B) A #B
#define CAT(A, B) _CAT (A, B)

#define REGION_HDR_SZ (128)

_Static_assert (sizeof (mregion) <= REGION_HDR_SZ,
                CAT ("mregion struct size is greater than ", REGION_HDR_SZ));

#define REGION_MIN_SZ (REGION_HDR_SZ + CHK_MIN_SZ + CHK_MIN_SZ)

#define PAGE_SIZE_MASK (PAGE_SIZE - 1)

#define REGION_ALIGNMENT (4096)

#define REGION_ALIGN_MASK (REGION_ALIGNMENT - 1)

#define region_align(sz) \
  ((sz) + REGION_ALIGN_MASK < REGION_MIN_SZ \
   ? REGION_MIN_SZ \
   : ((sz) + REGION_ALIGN_MASK) & ~(REGION_ALIGN_MASK))

static int
region_init (void *mem, size_t size)
{
  mregion *mreg = mem;
  mchunk *mchk;
  uptr align;
  bui_t bi;

  if (__builtin_expect (size < REGION_MIN_SZ, 0))
    {
      warn_error ("size is insufficient");
      return -1;
    }

  align = size & PAGE_SIZE_MASK;

  if (__builtin_expect (align > 0, 0))
    {
      warn_error ("size is not page aligned");
      return -1;
    }

  align = (uptr)(mreg) & PAGE_SIZE_MASK;

  if (__builtin_expect (align > 0, 0))
    {
      warn_error ("mem is not page aligned");
      return -1;
    }

  bi = REGION_HDR_SZ;

  mreg->size = size;
  mreg->nchunks = 1;
  mreg->nused = 0;
  mreg->bused = 0;
  mreg->bi_top = bi;
  mreg->bi_binfwd = bi;
  mreg->bi_binbwd = bi;
  mreg->bi_ssbfwd = 0;
  mreg->bi_ssbbwd = 0;
  mreg->bi_ssbmax = 0;

  /* set inital chunk */
  mchk = chunk (mreg, bi);
  set_prev_size (mchk, 0);

  size = size - bi - CHK_MIN_SZ;

  set_head (mchk, size | PREV_INUSE);
  mchk->fwd = 0;
  mchk->bwd = 0;

  /* set end chunk */
  mchk = next_chunk (mchk);
  set_prev_size (mchk, size);

  set_head (mchk, 0);  /* end has size of zero */
  mchk->fwd = 0;
  mchk->bwd = 0;

  return 0;
}

void *
bregion (bui_t size)
{
  size = region_align (size + REGION_HDR_SZ);

  mregion *mreg = mralloc (size);

  if (mreg && region_init (mreg, size) != 0)
    {
      mrfree (mreg);
      mreg = NULL;
    }
  if (mreg == NULL)
    {
      fatal_error ("internal error");
    }

  return mreg;
}

/*
 * ----------------------------- region managment -----------------------------
 */

static mregion *
region_resize (mregion *mreg, size_t newsize)
{
  bui_t bi, oldsize, size, top, end, fwd, bwd, chkmax;
  mchunk *mchk;

  newsize = region_align (newsize);
  oldsize = mreg->size;

  if (__builtin_expect (oldsize < REGION_MIN_SZ, 0))
    fatal_error ("corrupt region size");

  top = mreg->bi_top;
  end = oldsize - CHK_MIN_SZ;

  if (__builtin_expect (top < REGION_HDR_SZ, 0)
      || __builtin_expect (top >= end, 0))
    fatal_error ("corrupt top index");

  chkmax = end - top;

  if (newsize < oldsize)
    {
      mchk = chunk (mreg, end);

      if (prev_inuse (mchk))
        {
          warn_message ("last in use cannot shrink");
          return mreg;
        }

      size = prev_size (mchk);

      if (__builtin_expect (size > chkmax, 0)
          || __builtin_expect (size < CHK_MIN_SZ, 0))
        fatal_error ("invalid chunk size");

      bi = end - size;
      end = newsize - CHK_MIN_SZ;

      if (bi > end)
        {
          warn_message ("inefficient size available");
          return mreg;
        }

      size = end - bi;
      mchk = chunk (mreg, bi);

      if (size >= CHK_MIN_SZ)
        {
          /* Set new last chunk size */
          set_size_head (mchk, size);

          /* Setup end chunk with new last chunk size */
          mchk = chunk (mreg, end);

          set_prev_size (mchk, size);
          set_head (mchk, 0);

          mchk->fwd = 0;
          mchk->bwd = 0;
        }
      else
        {
          /* We cannot consume the last chunk if the new size would be less
           * than CHK_MIN_SZ, sizes greater or equal to CHK_MIN_SZ are handled
           * above. Though, a new size of exactly zero allows for removal of
           * the last chunk, which is handled below.
           */
          if (size > 0)
            {
              warn_message ("cannot consume last chunk");
              return mreg;
            }

          if (__builtin_expect (bi != end, 0))
            fatal_error ("mistake was made in this code, (bi != end)");

          /* Unlink chunk for removal, then replacement by the end chunk.
           * When chunk is head or tail of a bin, either bi_bin* or bi_ssb*,
           * determined when either fwd or bwd are zero within the chunk;
           * If so determined, the chunks index is compared with the
           * corresponding fwd or bwd index of the aforementioned bins, if
           * matching the value will be replaced with the chunks own fwd/bwd,
           * effectively unlinking it in that direction. Otherwise, if no
           * matching index is found, a fatal error will occur.
           *
           * It may be worth it to simply raise an error, like the above "cannot
           * consume last chunk", to avoid this complexity for this presumably
           * rare code path. Though, the main issue is not knowing which bin a
           * free chunk is in. A simple solution, one things are more worked
           * out, would be to tag the lower two bits in free chunks fwd and bwd
           * indices, to represent the bin it belongs.
           */
          set_size_head (mchk, 0);

          fwd = mchk->fwd;
          bwd = mchk->bwd;

          if (fwd)
            {
              if (__builtin_expect (fwd < top, 0)
                  || __builtin_expect (fwd >= end, 0))
                fatal_error ("currupt fwd chunk");

              chunk (mreg, fwd)->bwd = bwd;
            }
          else
            {
              if (mreg->bi_binbwd == bi)
                mreg->bi_binbwd = bwd;
              else if (mreg->bi_ssbbwd == bi)
                mreg->bi_ssbbwd = bwd;
              else
                fatal_error ("unlinked unused chunk");
            }

          if (bwd)
            {
              if (__builtin_expect (bwd < top, 0)
                  || __builtin_expect (bwd >= end, 0))
                fatal_error ("currupt bwd chunk");

              chunk (mreg, bwd)->fwd = fwd;
            }
          else
            {
              if (mreg->bi_binfwd == bi)
                mreg->bi_binfwd = fwd;
              else if (mreg->bi_ssbfwd == bi)
                mreg->bi_ssbfwd = fwd;
              else
                fatal_error ("unlinked unused chunk");
            }
          //mreg->nchunks--;
        }
      mchk->fwd = 0;
      mchk->bwd = 0;

      mreg = mrrealloc (mreg, newsize);

      if (mreg == NULL)
        return NULL;

      mreg->size = newsize;
    }
  else if (newsize > oldsize)
    {
      mreg = mrrealloc (mreg, newsize);

      if (mreg == NULL)
        return NULL;

      mreg->size = newsize;

      /* Use the end chunk as the last free chunk that will consume the newly
       * allocated area. Note that the current end is already offset backwards
       * by CHK_MIN_SZ, therefore the new end is the current end offset by the
       * newly allocated size.
       */
      size = 0;
      bi = end;
      mchk = chunk (mreg, bi);

      if (!prev_inuse (mchk))
        {
          size = prev_size (mchk);
          bi -= size;
          mchk = chunk (mreg, bi);

          fwd = mchk->fwd;
          bwd = mchk->bwd;

          if (fwd)
            {
              if (__builtin_expect (fwd < top, 0)
                  || __builtin_expect (fwd >= end, 0))
                fatal_error ("currupt fwd");

              chunk (mreg, fwd)->bwd = bwd;
            }
          else
            {
              if (mreg->bi_binbwd == bi)
                mreg->bi_binbwd = bwd;
              else if (mreg->bi_ssbbwd == bi)
                mreg->bi_ssbbwd = bwd;
              else
                fatal_error ("unlinked unused chunk");
            }

          if (bwd)
            {
              if (__builtin_expect (bwd < top, 0)
                  || __builtin_expect (bwd >= end, 0))
                fatal_error ("currupt bwd");

              chunk (mreg, bwd)->fwd = fwd;
            }
          else
            {
              if (mreg->bi_binfwd == bi)
                mreg->bi_binfwd = fwd;
              else if (mreg->bi_ssbfwd == bi)
                mreg->bi_ssbfwd = fwd;
              else
                fatal_error ("unlinked unused chunk");
            }

          mreg->nchunks--;
        }
      size += newsize - oldsize;

      set_size_head (mchk, size);

      /* We will link this new free chunk at the end of bi_bin, this will
       * ensure any existing smaller chunks will be used first. We want
       * allocations to trend towards the regions head.
       */
      bwd = mreg->bi_binbwd;
      mreg->bi_binbwd = bi;

      if (bwd)
        {
          if (__builtin_expect (bwd < top, 0)
              || __builtin_expect (bwd >= end, 0))
            fatal_error ("currupt bi_binbwd");

          chunk (mreg, bwd)->fwd = bi;
        }
      else
        {
          if (__builtin_expect (mreg->bi_binfwd != 0, 0))
            fatal_error ("bi_binbwd without bi_binfwd");

          mreg->bi_binfwd = bi;
          mreg->bi_binbwd = bi;
        }

      /* We are linking at the tail of the bin, so fwd should always be zero.
       */
      mchk->fwd = 0;
      mchk->bwd = bwd;

      mreg->nchunks++;

      /* Setup new end with new bin size */
      end = bi + size;
      mchk = chunk (mreg, end);

      set_prev_size (mchk, size);
      set_head (mchk, 0);
      mchk->fwd = 0;
      mchk->bwd = 0;
    }

  return mreg;
}

/*
 * ----------------------------- small sorted bin -----------------------------
 */

static void
small_sorted_add (mregion *mreg, bui_t bi)
{
  bui_t size, bwd, fwd, fwdsize;
  mchunk *mchk, *pchk;

  mchk = chunk (mreg, bi);
  size = chunksize (mchk);

  fwd = mreg->bi_ssbfwd;
  bwd = 0;

  while (fwd)
    {
      pchk = chunk (mreg, fwd);
      fwdsize = chunksize (pchk);

      if (fwdsize >= size)
        break;

      bwd = fwd;
      fwd = pchk->fwd;
    }

  if (fwd)
    {
      if (__builtin_expect (pchk == 0, 0))
        fatal_error ("(fwd && !pchk) which should be impossible");

      /* bwd should be correctly obtained from the above loop, we keep track of
       * the previous chunk as bwd. Though, we should still test to ensure we
       * are not about to link to a currupt chunk.
       */
      if (__builtin_expect (pchk->bwd != bwd, 0))
        fatal_error ("corrupt ssb bwd chunk");

      pchk->bwd = bi;
    }
  else
    {
      /* We are the furthest fwd which is the last, or first bwd chunk. The
       * largest size within ssb is the last chunk, so we must update ssbmax
       * with our size.
       */
      mreg->bi_ssbbwd = bi;
      mreg->bi_ssbmax = size;
    }

  if (bwd)
    {
      if (bwd == bi)
        fatal_error ("bwd == fwd");
      chunk (mreg, bwd)->fwd = bi;
    }
  else
    {
      /* Finally, the only case we should be here is when ssb is empty. Both
       * fwd and bwd should be zero, and we must has set ourself to ssbfwd in
       * the above else condition. Because no chunks are in ssb we are both
       * ssbfwd and ssbbwd.
       */
      //if (__builtin_expect (fwd != 0, 0))
      //  fatal_error ("ssb not empty ??? %lu", bwd);

      mreg->bi_ssbfwd = bi;
    }

  /* Last of all be sure to link ourself with fwd and bwd.
   *
   * NOTE:
   * Maybe we could move this to just after the inital loop, but then we
   * would miss out on being able to debug the value of mchk.
   */
  if (fwd == bi)
    fatal_error ("bwd == fwd");
  mchk->fwd = fwd;
  mchk->bwd = bwd;

  return;
}

static bui_t
small_sorted_pop (mregion *mreg, bui_t size)
{
  bui_t top, end, bi, bwd, fwd, nsize;
  mchunk *mchk;//, *pchk;

  if (mreg->bi_ssbmax == 0)
    return 0;

  end = mreg->size - CHK_MIN_SZ;
  top = mreg->bi_top;

  if (__builtin_expect (mreg->bi_ssbbwd == 0, mreg->bi_ssbfwd == 0)
      != __builtin_expect (mreg->bi_ssbfwd == 0, mreg->bi_ssbbwd == 0))
    {
      bregion_print (mreg);
      fatal_error ("missing fwd/bwd chunks");
    }

#if 0
  if (size < (mreg->bi_ssbmax >> 1))
    {
#endif
      fwd = mreg->bi_ssbfwd;
      bwd = 0;

      while (fwd)
        {
          if (__builtin_expect (fwd < top, 0)
              || __builtin_expect (fwd >= end, 0))
            fatal_error ("corrupt bin index");

          mchk = chunk (mreg, fwd);

          if (__builtin_expect (mchk->bwd != bwd, 0))
            fatal_error ("currupt bin links");

          nsize = chunksize (mchk);

          if (nsize >= size)
            break;

          bwd = fwd;
          fwd = mchk->fwd;
        }

      if (fwd == 0)
        return 0;

      bi = fwd;
      fwd = mchk->fwd;

      if (__builtin_expect (nsize != prev_size (next_chunk (mchk)), 0))
        fatal_error ("corrupt head/foot size");

      /* unlink chunk from bin */

      if (fwd)
        {
          /* bwd should have been already checked in the loop.
          */
          if (__builtin_expect (fwd < top, 0)
              || __builtin_expect (fwd >= end, 0))
            fatal_error ("corrupt bin list");

          chunk (mreg, fwd)->bwd = bwd;
        }
      else
        {
          mreg->bi_ssbbwd = bwd;
          mreg->bi_ssbmax = bwd ? chunksize (chunk (mreg, bwd)) : 0;
        }

      if (bwd)
        {
          if (bwd == fwd)
            fatal_error ("bwd == fwd");
          chunk (mreg, bwd)->fwd = fwd;
        }
      else
        {
          mreg->bi_ssbfwd = fwd;
        }
#if 0
    }
  else
    {
      pchk = NULL;
      fwd = 0;
      bwd = mreg->bi_ssbbwd;

      while (bwd)
        {
          if (__builtin_expect (bwd < top || bwd >= end, 0))
            fatal_error ("corrupt bin index");

          mchk = chunk (mreg, bwd);

          if (__builtin_expect (mchk->fwd != fwd, 0))
            fatal_error ("currupt bin links");

          nsize = chunksize (mchk);

          if (nsize < size)
            break;

          pchk = mchk;
          fwd = bwd;
          bwd = mchk->bwd;
        }

      if (bwd == 0 || pchk == NULL)
        return 0;

      mchk = pchk;
      nsize = chunksize (mchk);

      bi = fwd;
      fwd = mchk->fwd;

      if (__builtin_expect (nsize != prev_size (next_chunk (mchk)), 0))
        fatal_error ("corrupt head/foot size");

      /* unlink chunk from bin */

      if (fwd)
        {
          chunk (mreg, fwd)->bwd = bwd;
        }
      else
        {

          mreg->bi_ssbbwd = bwd;
          if (!bwd)
            mreg->bi_ssbmax = 0;
          else
            {
              /* fwd should have been already checked in the loop.
              */
              if (__builtin_expect (bwd < top, 0)
                  || __builtin_expect (bwd >= end, 0))
                fatal_error ("corrupt bin list");

              mreg->bi_ssbmax = chunksize (chunk (mreg, bwd));
            }
        }

      if (bwd)
        {
          /* fwd should have been already checked in the loop.
          */
          if (__builtin_expect (bwd < top, 0)
              || __builtin_expect (bwd >= end, 0))
            fatal_error ("corrupt bin list");

          if (bwd == fwd)
            fatal_error ("bwd == fwd");
          chunk (mreg, bwd)->fwd = fwd;
        }
      else
        {
          mreg->bi_ssbfwd = fwd;
        }

    }
#endif

  //mchk->fwd = 0;
  //mchk->bwd = 0;

  set_inuse (mchk);
  mreg->bused += nsize;
  mreg->nused++;

  return bi;
}

/*
 * Smashes the small-sorted-bin forwards into the regular bin, effectively
 * emptying and resetting ssb.
 */
static void
ssb_smash (mregion *mreg)
{
  bui_t fwd, bwd, top, end;
  mchunk *mchk;

  end = mreg->size - CHK_MIN_SZ;
  top = mreg->bi_top;

  /* Attach the end of ssb to the head of the regular bin
   */
  fwd = mreg->bi_binfwd;
  bwd = mreg->bi_ssbbwd;

  if (fwd)
    {
      if (__builtin_expect (fwd < top, 0)
          || __builtin_expect (fwd >= end, 0))
        fatal_error ("currupt binfwd");

      mchk = chunk (mreg, fwd);

      if (__builtin_expect (mchk->bwd != 0, 0))
        fatal_error ("binfwd is not head");

      mchk->bwd = bwd;
    }
  else
    {
      if (__builtin_expect (mreg->bi_binbwd != 0, 0))
        fatal_error ("binbwd without binfwd");

      mreg->bi_binbwd = bwd;
    }

  if (bwd)
    {
      if (__builtin_expect (bwd < top, 0)
          || __builtin_expect (bwd >= end, 0))
        fatal_error ("currupt ssbbwd");

      mchk = chunk (mreg, bwd);

      if (__builtin_expect (mchk->fwd != 0, 0))
        fatal_error ("ssbbwd is not tail");

      mchk->fwd = fwd;
    }
  else
    {
      /* A bin with one or more chunks must have both fwd and bwd, the head
       * and tail indices, respectively. Otherwise, when a bin is empty, both
       * fwd and bwd must be zero. Furthermore, for bins with only one chunk,
       * both fwd and bwd may point to the same chunk.
       *
       * Additionally, chunks have their own fwd and bwd. One must also ensure
       * fwd and bwd are zero, in chunks exclusive to a bin; When lead or tail
       * to abin chunks must zero the inverse direction, for example consider
       * a bin with four chunks.
       *
       *                    {bwd, fwd}
       *              [bin] { 16,  8 } -> ( 16, 64, 32,  8 )
       *
       * The fwd and bwd indices are 16 and 8, respectively. To traverse
       * forward throughout this bin we begin with the chunk at index 16.
       *
       *               [16] {  0, 64 } -> ( @>, 64, 32,  8 )
       *
       * We can see in this chunk that bwd is zero, therefore it must be
       * the first chunk in the bin, which I can gladly confirm it is.
       *
       *               [32] { 64,  8 } -> ( 16, 64, @>, 16 )
       *
       *               [08] { 32, ?? } -> ( 16, 64, 32, @> )
       *
       * Skipping forward a little, we are the chunk index 32 and use
       * it's fwd index of 8, which is where we are now... We are the CPU
       * traversing this structure, just imagine, it would be the most
       * intensely profound experience; at tremendous speeds, with the next
       * instructions already loaded and ready to fire, or maybe already being
       * pipelined. For our bin here, a double-linked list it is entirely like
       * teleporting around the memory space, and what do we do now that we
       * are at the last chunk??? That's right, read fwd and keep traversing
       * forward, that is unless, who ever linked this chunk correctly
       * terminated it by setting fwd to zero.
       *
       * Anyway, to continue, at this point (in real code looking below), both
       * ssbfwd and ssbbwd should be zero, but we still confirm it with this
       * built-in that shoooould apparently talk to the CPU, which now I've been
       * told predicts a lot of things, I think it's a gambling issue, but when
       * they make a misprediction  they, the CPU that is, spend a lot of time
       * getting back on their feet. So let's a friend and suggest some better
       * paths to take!
       */
      if (__builtin_expect (mreg->bi_ssbfwd != 0, 0))
        fatal_error ("ssbfwd without ssbbwd");

      return;
    }

  /* Now that we've handled linking ssbbwd <-> binfwd, lets finish by smashing
   * ssbfwd it into binfwd. Effectively sandwiching both sets of chunks into
   * the bin.
   *              ssbfwd < > ssbbwd <-> binfwd < > binbwd
   *                   ssnfwd <...>>>...binfwd   > binbwd
   *                         sinfwd <...>>>...   > binbwd
   *                                    binfwd < > binbwd
   */
  fwd = mreg->bi_ssbfwd;

  if (__builtin_expect (fwd < top, 0)
      || __builtin_expect (fwd >= end, 0))
    fatal_error ("currupt ssbfwd");

  mchk = chunk (mreg, fwd);

  if (__builtin_expect (mchk->bwd != 0, 0))
    fatal_error ("ssbfwd is not head");

  mreg->bi_binfwd = fwd;

  if (mreg->bi_binbwd == 0)
    mreg->bi_binbwd = bwd;

  /* Last of all clear ssb */
  mreg->bi_ssbfwd = 0;
  mreg->bi_ssbbwd = 0;
  mreg->bi_ssbmax = 0;

  return;
}

/*
 *
 */

static bui_t
chunk_popsize (mregion *mreg, size_t bytes)
{
  bui_t bi, size, nb, end, top, fwd, bwd;
  mchunk *mchk;

  end = mreg->size - CHK_MIN_SZ;
  top = mreg->bi_top;

  if (__builtin_expect (mreg->bi_binbwd == 0, mreg->bi_binfwd == 0)
      != __builtin_expect (mreg->bi_binfwd == 0, mreg->bi_binbwd == 0))
    {
      bregion_print (mreg);
      fatal_error ("missing fwd/bwd chunks");
    }

  bwd = 0;
  bi = mreg->bi_binfwd;

  while (bi != 0)
    {
      if (__builtin_expect (bi < top || bi >= end, 0))
        fatal_error ("corrupt bin index");

      mchk = chunk (mreg, bi);

      if (__builtin_expect (mchk->bwd != bwd, 0))
        fatal_error ("currupt bin links");

      size = chunksize (mchk);

      if (size >= bytes)
        break;

      bwd = bi;
      bi = mchk->fwd;
    }

  if (bi == 0)
    return 0;

  if (__builtin_expect (size != prev_size (next_chunk (mchk)), 0))
      fatal_error ("corrupt head/foot size");

  fwd = mchk->fwd;

  /* unlink chunk from bin */

  if (fwd)
    {
      /* bwd should have been already checked in the loop.
       */
      if (__builtin_expect (fwd < top, 0)
          || __builtin_expect (fwd >= end, 0))
        fatal_error ("corrupt bin list");

      chunk (mreg, fwd)->bwd = bwd;
    }
  else
    {
      mreg->bi_binbwd = bwd;
    }

  if (bwd)
    {
      chunk (mreg, bwd)->fwd = fwd;
    }
  else
    {
      mreg->bi_binfwd = fwd;
    }

  /* Clears chunks fwd and bwd ??? */
#if 0
  mchk->fwd = 0;
  mchk->bwd = 0;
#endif

  nb = size - bytes;

  if (nb > CHK_MIN_SZ)
    {
      /* We have some extra space we'd like to bin, so that means we need to
       * split ourselfs into a new chunk.
       */

      set_size_head (mchk, bytes);  /* set bytes requested */

      bui_t bii = bi + bytes;         /* offset to new chunk */
      mchk = chunk (mreg, bii);

      /* Set bytes in new chunks prev_size, and that we are in currently use.
       * Also set new chunks size.
       */
      set_prev_size (mchk, bytes);
      set_head (mchk, nb | PREV_INUSE);
      set_size_foot (mchk, nb);

      /* Link the new chunk. TODO: IF CHUNK IS SSBSIZE LINK TO SSB !!! or
       * actually maybe only at free, we don't really want alloc taking up to
       * much time.
       *
       * We don't need to do much checking, mostly done above so should be safe.
       */
#if SSB
      if (nb > SSB_SIZE)
        {
#endif
          fwd = mreg->bi_binfwd;
          mchk->fwd = fwd;
          mchk->bwd = 0;

          if (fwd)
            {
              chunk (mreg, fwd)->bwd = bii;
            }
          else
            {
              mreg->bi_binbwd = bii;
            }
          mreg->bi_binfwd = bii;
#if SSB
        }
      else
        {
          small_sorted_add (mreg, bii);
        }
#endif

      mreg->bused += bytes;
      mreg->nused++;
      mreg->nchunks++;    /* split chunk */
    }
  else
    {
      /* We're easy, request bytes fits, snug as a bug in a rug. Maybe we could
       * do some other work here to even things out
       */
      set_inuse (mchk);
      mreg->bused += size;
      mreg->nused++;
    }

  return bi;
}

static bui_t bin_unite (mregion *mreg, bui_t bi);

static void
consolidate (mregion *mreg)
{
  bui_t bi, end, top, binfwd, binbwd, fwd;
  mchunk *mchk;

  ssb_smash (mreg);

  end = mreg->size - CHK_MIN_SZ;
  top = mreg->bi_top;

  binfwd = mreg->bi_binfwd;
  binbwd = mreg->bi_binbwd;

  if (binfwd == 0)
    {
      if (__builtin_expect (binbwd != 0, 0))
        fatal_error ("binfwd without binbwd");
      return;
    }

  bi = binfwd;
  binfwd = 0;
  binbwd = 0;
  do
    {

      if (__builtin_expect (bi < top || bi >= end, 0))
        fatal_error ("corrupt binfwd index");

      mchk = chunk (mreg, bi);
      fwd = mchk->fwd;

      if (__builtin_expect (mchk->bwd != 0, 0))
        fatal_error ("binfwd non-zero bwd");

      mchk->fwd = 0;
      mchk->bwd = 0;
      mreg->bi_binfwd = fwd;

      if (fwd)
        {
          if (__builtin_expect (fwd < top, 0)
              || __builtin_expect (fwd >= end, 0))
            fatal_error ("corrupt fwd index");

          chunk (mreg, fwd)->bwd = 0;

          bi = bin_unite (mreg, bi);
          mchk = chunk (mreg, bi);
        }
      else
        {
          if (__builtin_expect (bi != mreg->bi_binbwd, 0))
            fatal_error ("binfwd != binbwd");

          mreg->bi_binbwd = 0;
        }

#if 1
      mchk->fwd = binfwd;
      mchk->bwd = 0;

      if (binfwd)
        {
          chunk (mreg, binfwd)->bwd = bi;
        }
      else
        {
          binbwd = bi;
        }
      binfwd = bi;
#else
      bin_sort_link (mreg, &binfwd, &binbwd, bi);
#endif

      bi = mreg->bi_binfwd;
    }
  while (bi);

  mreg->bi_binfwd = binfwd;
  mreg->bi_binbwd = binbwd;

  return;
}

bui_t
balloc (void **pregion, size_t size, int flags)
{
  mregion *mreg;
  bui_t bi;

  mreg = *pregion;
  size = req2size (size);

#if 1
  if ((mreg->nchunks >> 3) > mreg->nused)
    consolidate (mreg);
#endif

#if SSB
  do
    {
      if (size <= SSB_SIZE // mreg->bi_ssbmax
          && (bi = small_sorted_pop (mreg, size)) != 0)
        break;
#endif
      bi = chunk_popsize (mreg, size);
#if SSB
    }
  while (0);
#endif

  if (bi == 0)
    do
    {
      if ((flags & BA_NOEXPAND))
	{
	  consolidate (mreg);
	  bi = chunk_popsize (mreg, size);
	  if (bi != 0)
	break;
	  return 0;
	}

      size_t oldsize, newsize, avlsize;

      oldsize = mreg->size;
      avlsize = oldsize - mreg->bused;
      newsize = oldsize + (avlsize < size ? size - avlsize : size);
      newsize = region_align (newsize + (newsize >> 1));

      if (avlsize > (size << 1) && mregion_willremap (mreg, newsize))
        {
          consolidate (mreg);
          bi = chunk_popsize (mreg, size);
          if (bi != 0)
        break;
        }

      mreg = region_resize (mreg, newsize);
      *pregion = mreg;

      if (mreg == NULL)
        return 0;

      if ((bi = chunk_popsize (mreg, size)) == 0)
        return 0;
    }
    while (0);


  return bi + CHK_MEM_OFF;
}

static bui_t
bin_unite (mregion *mreg, bui_t bi)
{
  bui_t top, end, size, fwd, bwd, idx;
  mchunk *mchk;

  top = mreg->bi_top;
  end = mreg->size - CHK_MIN_SZ;

  mchk = chunk (mreg, bi);
  size = chunksize (mchk);
  idx = bi + size;
  do
    {
      mchk = chunk (mreg, idx);
      size = chunksize (mchk);

      if (size == 0)
        break;

      if (__builtin_expect (size < CHK_MIN_SZ || idx + size > end, 0))
        fatal_error ("corrupt size");

      if (is_inuse (mchk))
        break;

      fwd = mchk->fwd;
      bwd = mchk->bwd;

      if (fwd)
        {
          if (__builtin_expect (fwd < top, 0)
              || __builtin_expect (fwd >= end, 0))
            fatal_error ("corrupt fwd index");

          chunk (mreg, fwd)->bwd = bwd;
        }
      else
        {
          if (mreg->bi_binbwd == idx)
            mreg->bi_binbwd = bwd;
          else if (mreg->bi_ssbbwd == idx)
            mreg->bi_ssbbwd = bwd;
          else
            fatal_error ("unlinked unused chunk");
          //mreg->bi_binbwd = bwd;
        }

      if (bwd)
        {
           if (__builtin_expect (bwd < top, 0)
              || __builtin_expect (bwd >= end, 0))
            fatal_error ("corrupt bwd index");

          chunk (mreg, bwd)->fwd = fwd;
        }
      else
        {
          if (mreg->bi_binfwd == idx)
            mreg->bi_binfwd = fwd;
          else if (mreg->bi_ssbfwd == idx)
            mreg->bi_ssbfwd = fwd;
          else
            fatal_error ("unlinked unused chunk");
          //mreg->bi_binfwd = fwd;
        }

      idx += size;
      mreg->nchunks--;
    }
  while (1);

  size = idx - bi;
  mchk = chunk (mreg, bi);

  while (!prev_inuse (mchk))
    {
      idx = prev_size (mchk);
      bi -= idx;

      if (__builtin_expect (idx < CHK_MIN_SZ || bi < top, 0))
        fatal_error ("corrupt prev_size");

      mchk = chunk (mreg, bi);
      size += idx;

      fwd = mchk->fwd;
      bwd = mchk->bwd;

      if (fwd)
        {
          if (__builtin_expect (fwd < top, 0)
              || __builtin_expect (fwd >= end, 0))
            fatal_error ("corrupt fwd index");

          chunk (mreg, fwd)->bwd = bwd;
        }
      else
        {
          if (mreg->bi_binbwd == bi)
            mreg->bi_binbwd = bwd;
          else if (mreg->bi_ssbbwd == bi)
            mreg->bi_ssbbwd = bwd;
          else
            fatal_error ("unlinked unused chunk");

          //mreg->bi_binbwd = bwd;
        }

      if (bwd)
        {
           if (__builtin_expect (bwd < top, 0)
              || __builtin_expect (bwd >= end, 0))
            fatal_error ("corrupt bwd index");

          chunk (mreg, bwd)->fwd = fwd;
        }
      else
        {
          if (mreg->bi_binfwd == bi)
            mreg->bi_binfwd = fwd;
          else if (mreg->bi_ssbfwd == bi)
            mreg->bi_ssbfwd = fwd;
          else
            fatal_error ("unlinked unused chunk");

          //mreg->bi_binfwd = fwd;
        }

      mreg->nchunks--;
    }

  set_size_head (mchk, size);
  set_size_foot (mchk, size);

  bi = chunkindex (mreg, mchk);

  return bi;
}

void
bfree (void *region, bui_t bi)
{
  mregion *mreg = region;
  mchunk *mchk;
  bui_t top, end, size, ssbsz, fwd;

  top = mreg->bi_top;
  end = mreg->size - CHK_MIN_SZ;

  bi -= CHK_MEM_OFF;

  if (bi < top || bi >= end)
    fatal_error ("invalid block index");

  mchk = chunk (mreg, bi);
  size = chunksize (mchk);

  mreg->bused -= size;
  mreg->nused--;
  ssbsz = SSB_SIZE;

#if 0
  if (size > ssbsz)
  //if ((mreg->nchunks >> 3) + (mreg->nchunks >> 1) > mreg->nused)
    {
      bi = bin_unite (mreg, bi);
      mchk = chunk (mreg, bi);
      size = chunksize (mchk);
    }
  //printf ("%lu / %lu\n", size, ssbsz);
#endif

//#undef SSB
//#define SSB 0
#if SSB
  if (size > ssbsz)
    {
#endif
#if 1
      fwd = mreg->bi_binfwd;
      mchk->fwd = fwd;
      mchk->bwd = 0;

      if (fwd)
        {
          if (__builtin_expect (fwd < top, 0)
              || __builtin_expect (fwd >= end, 0))
            fatal_error ("corrupt binfwd");

          chunk (mreg, fwd)->bwd = bi;
        }
      else
        {
          if (__builtin_expect (mreg->bi_binbwd != 0, 0))
            fatal_error ("binbwd without binfwd");

          mreg->bi_binbwd = bi;
        }
      mreg->bi_binfwd = bi;
#else
      bin_sort_link (mreg, &mreg->bi_binfwd, &mreg->bi_binbwd, bi);
#endif
#if SSB
    }
  else
    {
      small_sorted_add (mreg, bi);
    }
#endif

  clr_inuse (mchk);

  return;
}

void
bregion_print (void *region)
{
  mregion *mreg = region;
  mchunk *mchk;
  bui_t bi, top, end, size, prevsize, used, nchunk;
  char inuse;

  size = mreg->size;
  top = mreg->bi_top;
  used = 0;

  if (__builtin_expect (size < REGION_HDR_SZ, 0))
    fatal_error ("invalid or corrupt region");

  printf (
        "\n[bregion]:\n"
        "  size       = %lu\n"
        "  bi_top     = 0x%lx\n"
        "  bi_binfwd  = 0x%lx\n"
        "  bi_binbwd  = 0x%lx\n"
        "  bi_ssbfwd  = 0x%lx\n"
        "  bi_ssbbwd  = 0x%lx\n"
        "  nchunks    = %lu\n"
        "  bused      = %lu\n"
        "  nused      = %lu\n"
        "\n",
      size,
      top,
      mreg->bi_binfwd,
      mreg->bi_binbwd,
      mreg->bi_ssbfwd,
      mreg->bi_ssbbwd,
      mreg->nchunks,
      mreg->bused,
      mreg->nused
  );

  nchunk = 0;
  end = size - CHK_MIN_SZ;
  size = 0;
  bi = top;


  //if (GROP (verbose) > 1)
    do
    {
      prevsize = size;
      bi += size;

      if (__builtin_expect (bi < top || bi > end, 0))
        fatal_error ("corrupt bin index");

      mchk = chunk (mreg, bi);
      size = chunksize (mchk);

      if (size > 0)
        ++nchunk;

      inuse = size == 0
              ? '_'
              : is_inuse (mchk) ? '#' : '-';
      if (inuse == '#')
        used += size;

      if (__builtin_expect (prev_size (mchk) != prevsize, 0))
        fatal_error ("corrupt head/foot size");

      printf ("  %lx [%c] size: %4lu B\n",
          bi,
          inuse,
          size
      );
    }
    while (size > 0);

  printf ("used: %lu\n", used);

  if (nchunk != mreg->nchunks)
    {
      fatal_error ("corrupt or miscount nchunks ((mreg) %lu != (real) %lu)",
                    mreg->nchunks, nchunk);
    }

}

EXPORT void *bregion (bui_t size);

EXPORT bui_t balloc (void **pregion, size_t size, int flags);

EXPORT void bfree (void *region, bui_t bi);

EXPORT void bregion_print (void *region);

