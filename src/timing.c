/*
 * timing.c
 *
 * Created 29 Aug 2023 by Mason Hall (mason@mashpot.net)
 *
 * Copyright (C) 2023 Mason Hall
 */

#include <stdio.h>
#include <stdlib.h>

#include <globdefs.h>
#include <timing.h>

#define TIME_FUNC_RV8     (1)
#define TIME_FUNC_RV16    (2)
#define TIME_FUNC_RV32    (4)
#define TIME_FUNC_RV64    (8)

#define TIME_FUNC_EV8     (16)
#define TIME_FUNC_EV16    (32)
#define TIME_FUNC_EV32    (64)
#define TIME_FUNC_EV64    (128)

extern __attribute__ ((weak))
int
time_func (size_t count, int flags, u64 ev, u64 (*func)(), ...);

extern inline __attribute__ ((__gnu_inline__))
int
time_func (size_t count, int flags, u64 ev, u64 (*func)(), ...)
{
  int rvtype = flags & 15;
  u64 rv = 0;

  switch (rvtype)
    {
    case TIME_FUNC_RV8:
      rv = ((u8 (*)())(func)) (__builtin_va_arg_pack ());
      break;
    case TIME_FUNC_RV16:
      rv = ((u16 (*)())(func)) (__builtin_va_arg_pack ());
      break;
    case TIME_FUNC_RV32:
      rv = ((u32 (*)())(func)) (__builtin_va_arg_pack ());
      break;
    case TIME_FUNC_RV64:
      rv = ((u64 (*)())(func)) (__builtin_va_arg_pack ());
      break;
    }

  return rv;
}

int __test (int val, int opt)
{
  return val - opt;
}

int _test (void)
{
  int rv = time_func (1, TIME_FUNC_RV32, -1, (u64 (*)()) &__test, 1, 1);
  fatal_error ("time_func: %i", rv);
  return 0;
}

EXPORT int _test (void);


#if 0
#define DBG_TIME_CLKID CLOCK_MONOTONIC_RAW
#define NSECS_PER_SEC 1E9

struct timeresult
{
  struct timespec tv;
  void *rv;
};

#define _STR(v) #v

#define timefunc(func, count, reterr, ...) do { \
    size_t i, n = (size_t)(count); \
    /*struct timeresult resv[n]; */\
    struct timespec st, et; \
    void *rv = 0; \
    double avg = 0.0; \
    \
    for (i = 0; i < n; ++i) \
      { \
        if (clock_gettime (DBG_TIME_CLKID, &st) == -1) \
          break; \
        \
        rv = (void*) (func)(__VA_ARGS__); \
        \
        if (clock_gettime (DBG_TIME_CLKID, &et) == -1) \
          break; \
        \
        st = timevdiff (st, et); \
        avg += (st.tv_sec * NSECS_PER_SEC) \
                + ((double) st.tv_nsec); \
        /* resv[i].tv = timevdiff (st, et); */\
        /* resv[i].rv = rv; */\
        \
        if (rv == (void*)(reterr)) \
          break; \
      } \
    \
    if (i < n) \
      { \
        const char *s1, *s2 = ""; \
        \
        if (rv == (void*)(reterr)) { \
            s1 = "failed at iteration"; \
        } else { \
            s1 = "clock_gettime:"; \
            s2 = strerror (errno); \
        } \
        fprintf (stderr, "%s:timefunc: [%lu/%lu] "_STR(func)"() %s%s\n", \
                 __func__, i, n, s1, s2); \
      } \
    \
    /*double avg = timeres_avg (&resv[0], i); */\
    /* avg /= (double) n; */\
    fprintf (stderr, _STR(func)"() %f ns/%lu\n", avg, i); \
    \
} while (0)
#endif

