/*
 * mreg.c
 *
 * Created 1 Aug 2023 by Mason Hall (mason@mashpot.net)
 *
 * Copyright (C) 2023 Mason Hall
 */

#define _GNU_SOURCE

#include <stdio.h>
#include <stdlib.h>
#include <unistd.h>
#include <errno.h>
#include <string.h>
#include <sys/mman.h>

#include <globdefs.h>

#define ALIGN_UP_PTR(ptr, size) \
  ((__typeof__ (ptr)) align_up ((uptr) (ptr), (size)))

#define ALIGN_DOWN_PTR(ptr, size) \
  ((__typeof__ (ptr)) align_down ((uptr) (ptr), (size)))

#define M_RDWR  (PROT_READ | PROT_WRITE)
#define M_FLAGS (MAP_PRIVATE | MAP_ANONYMOUS)  // MAP_SHARED

#define MP_ALIGN_MASK (PAGESIZE - 1)
#define MP_DIFF_MAX   (PAGESIZE << 2)

#define req2size(req) \
  (((req) + MP_ALIGN_MASK <= PAGESIZE)  ? \
   PAGESIZE                             : \
   ((req) + MP_ALIGN_MASK) & ~MP_ALIGN_MASK)

/* using GNU malloc min pages to keep aligned */
#define MP_PAGE_MIN (32 * 1024)

#define MP_PAGE_MAX (PAGESIZE * PAGESIZE)

#define MP_LOCK 0

typedef struct mreginfo
{
  void *regptr;


  /* actual size of regs */
  size_t size;

  /* requested size - if zero reg not in use (not actually, add flags) */
  size_t usedsize;

  /* last request - if not in use and non-zero, free regs. */
  size_t lastsize;

} mreginfo;

struct mrstate
{
  void *regptr;

  size_t size;

  size_t nreg;

  size_t lastsize;

  mreginfo regv[];
};

static struct mrstate *mstate = NULL;

#define mreginfo_offset(p) ((uptr) (((char*)(p)) - ((char*)(mstate))))

#define mreginfo_at(off) ((mreginfo*) (((char*)(mstate)) + (off)))

static int
mstate_init (void)
{
  size_t pagsz = _grtopts._op_pagesize;

  if (__builtin_expect ((long int) pagsz <= 0, 0))
    fatal_error ("invalid pagesize");

  void *mem = mmap (NULL, pagsz, M_RDWR, M_FLAGS, -1, 0);

  if (mem == MAP_FAILED)
    {
      warn_perror ("mmap");
      return -1;
    }

  mstate = mem;
  mstate->regptr = NULL;
  mstate->size = PAGESIZE;
  mstate->nreg = 0;
  mstate->lastsize = 0;

  memset (&mstate->regv[0], 0, sizeof (mreginfo));

  //if (mlockall (MCL_CURRENT | MCL_FUTURE) == -1)
  //  fatal_perror ("mlockall");

  return 0;
}

static int
mstate_grow (void)
{
  struct mrstate *ms;
  size_t size, newsize;
  void *mem;

  size = mstate->size;
  newsize = size + PAGESIZE;

  mem = mmap (NULL, newsize, M_RDWR, M_FLAGS, -1, 0);

  if (mem == MAP_FAILED)
    {
      perror ("mstate_grow:mmap");
      warn_message ("mapping failed, can't grow mstate.");
      return -1;
    }

  ms = mem;
  memcpy (ms, mstate, size);
  ms->size = newsize;
  ms->lastsize = size;

  mem = mstate;
  mstate = ms;

  if (__builtin_expect (munmap (mem, size), 0))
    {
      perror ("munmap");
      warn_message ("mstate_grow: unmap failed, will retry next grow");
      mstate->regptr = mem;
    }

  return 0;
}

static mreginfo *
region_hold (mreginfo *p)
{
  size_t size;
  uptr off;

  size = mstate->size;
  off = mreginfo_offset (p);

  if (__builtin_expect (off > size, 0))
    fatal_error ("invalid pointer");

  if (__builtin_expect (p->usedsize != 0, 0))
    {
      warn_message ("cannot hold used region");
      return NULL;
    }

  if (off == size)
    {
      if (mstate_grow () != 0)
        return NULL;

      mstate->nreg++;
      p = mreginfo_at (off);  /* address may have changed */
    }

  /* do hold ? */

  return p;
}

static void
region_release (mreginfo *p)
{
  size_t size;
  uptr off;

  size = mstate->size;
  off = mreginfo_offset (p);

  if (__builtin_expect (off > size, 0))
    fatal_error ("region_release: invalid pointer");

  if (off == size)
      --mstate->nreg;

  memset (p, 0, sizeof (mreginfo));
}

static mreginfo *
region_get (size_t size)
{
  mreginfo *p;

  /* should aquire read-lock when threading */

  for (p = &mstate->regv[0]; p->regptr; ++p)
    {
      if (p->usedsize == 0)
        {
          ssize_t diff = (ssize_t) p->size - (ssize_t) size;

          if ((p->lastsize) || (diff >= 0 && diff < (ssize_t) MP_DIFF_MAX))
            break;

          p->lastsize = (size_t) diff;
        }
    }

  /* reg is now held by caller */
  return region_hold (p);
}

static mreginfo *
__attribute__ ((pure))
region_is_ptr (void *ptr)
{
  mreginfo *p;

  if ((((uptr) ptr) & (PAGESIZE-1)))
    return NULL;

  for (p = &mstate->regv[0]; p->regptr; ++p)
    {
      if (p->regptr == ptr)
        return p;
    }
  return NULL;
}


static mreginfo *
__attribute__ ((pure))
region_for_ptr (void *ptr)
{
  ptr = ALIGN_DOWN_PTR (ptr, MP_PAGE_MAX);
  mreginfo *p;

  for (p = &mstate->regv[0]; p->regptr; ++p)
    {
      if (ptr == ALIGN_DOWN_PTR (p->regptr, MP_PAGE_MAX))
        return p;
    }
  return NULL;
}

static mreginfo *
region_map (mreginfo *p, size_t size, int flags)
{
  size_t oldsize, newsize;
  void *mem;

  size = req2size (size);

  /* align regions to max pages to keep seperated */
  if (size < MP_PAGE_MAX)
    newsize = MP_PAGE_MAX;
  else
    newsize = align_up (size, MP_PAGE_MAX);
  newsize = align_up (newsize, PAGESIZE);

  oldsize = p->size;
  mem = p->regptr;

  if (mem == NULL)
    {
      mem = mmap (NULL, newsize, PROT_NONE, M_FLAGS, -1, 0);
    }
  else if (oldsize - newsize > MP_DIFF_MAX)  /* intended overflow */
    {
      /*
       * mremap fails when address range is not completely writeable, this
       * may only occur if MREMAP_MAYMOVE is specified during expansion; or
       * for a number of other reasons. It's best to be safe and set the
       * M_RDWR with mprotect.
       *
       */
      if (mprotect (mem, oldsize, M_RDWR) != 0)
        goto mproterr;

      mem = mremap (mem, oldsize, newsize, flags);
    }

  if (mem == MAP_FAILED)
    {
      perror ("m(re)map");
      warn_message ("mralloc(): failed to map region");

      newsize = 0;
    }
  else if (mprotect (mem, newsize, PROT_NONE) != 0
           || mprotect (mem, size, M_RDWR) != 0)
    {
mproterr:
      perror ("mprotect");
      warn_message ("mralloc(): failed to change memory protections");

      if (munmap (mem, newsize) != 0)
        {
          perror ("munmap");
          warn_message ("mralloc(): unmap failed after protection fail");
        }

      region_release (p);
      return NULL;
    }

#if MP_LOCK
  if (mlock2 (mem, size, MLOCK_ONFAULT) == -1)
    warn_perror ("mlock2");
#endif

  p->regptr = mem;
  p->size = newsize;
  p->usedsize = size;
  p->lastsize = oldsize;

  return p;
}

static void
_mrfree (void *ptr)
{
  mreginfo *p;
  uptr off;

  p = region_for_ptr (ptr);

  if (__builtin_expect (p == NULL, 0)
      || __builtin_expect (p->regptr == NULL, 0))
    fatal_error ("mrfree(): invalid pointer");

  off = (uptr) ptr - (uptr) p->regptr;

  if (__builtin_expect (off > p->usedsize, 0))
    fatal_error ("mrfree(): invalid size");

  ptr = ALIGN_UP_PTR (ptr, PAGESIZE);
  off = p->usedsize - ((uptr) ptr - (uptr) p->regptr);

  /* nothing we can do */
  if (off == 0)
    return;

  if (__builtin_expect (off > p->usedsize, 0))
    fatal_error ("mrfree(): unaligned region");

  if (mprotect (ptr, off, PROT_NONE) != 0)
    {
      perror ("mprotect");
      warn_message ("mrfree(): failed to change memory protections");
      return;
    }

  p->usedsize -= off;  /* should be zero indicating free of all was freed */
}

void *
mrrealloc (void *ptr, size_t newsize)
{
  mreginfo *p;

  p = region_is_ptr (ptr);

  if (__builtin_expect (p == NULL, 0))
    fatal_error ("invalid pointer");

  ptr = p->regptr;

  if (mprotect (ptr, p->usedsize, PROT_NONE) != 0)
    {
      perror ("mprotect");
      warn_message ("failed to change protections");
      region_release (p);
      return NULL;
    }

  p = region_map (p, newsize, MREMAP_MAYMOVE);

  if (p == NULL)
    return NULL;

  ptr = p->regptr;

  if (ptr == MAP_FAILED)
    {
      region_release (p);
      ptr = NULL;
    }
  return ptr;
}

int
mregion_willremap (void *ptr, size_t size)
{
  size_t oldsize, newsize;
  mreginfo *p;

  p = region_is_ptr (ptr);

  if (__builtin_expect (p == NULL, 0))
    fatal_error ("invalid pointer");

  size = req2size (size);

  /* align regions to max pages to keep seperated */
  if (size < MP_PAGE_MAX)
    newsize = MP_PAGE_MAX;
  else
    newsize = align_up (size, MP_PAGE_MAX);
  newsize = align_up (newsize, PAGESIZE);

  oldsize = p->size;

  if (oldsize - newsize > MP_DIFF_MAX)
    return 1;
  return 0;
}

void *
mralloc (size_t bytes)
{
  mreginfo *p;
  void *mem;

  if (mstate == NULL && mstate_init () != 0)
    return NULL;

  if ((p = region_get (bytes)) == NULL)
    return NULL;

  p = region_map (p, bytes, MREMAP_MAYMOVE);

  if (p == NULL)
    return NULL;

  mem = p->regptr;

  if (mem == MAP_FAILED)
    {
      region_release (p);
      mem = NULL;
    }
  return mem;
}

void
mrfree (void *ptr)
{
  _mrfree (ptr);
}

int
mreg_test (void)
{
  printf ("PAGESIZE: %lu\n", PAGESIZE);
  return 0;
}


EXPORT void *mralloc (size_t bytes);

EXPORT void mrfree (void *ptr);

EXPORT void *mrrealloc (void *ptr, size_t newsize);

EXPORT int mregion_willremap (void *ptr, size_t size);

EXPORT int mreg_test (void);


