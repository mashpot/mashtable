/*
 * rbtree.c
 *
 * Created 11 Oct 2023 by Mason Hall <mason@mashpot.net>
 *
 * Copyright (C) 2023 Mason Hall
 */

#include <block-size.h>

typedef struct simblock
{
  size_t dtick;

  struct simblock *fwd;

  struct simblock *bwd;

  struct simblock *rb_left;

  struct simblock *rb_right;

  bui_t __rb_parent_color;

  size_t bytes;

  union {
      bui_t bi;
      void *ptr;
  };

} simblock;


#define RB_RED	  0
#define RB_BLACK  1

#define RB_COLOR_MASK (RB_RED | RB_BLACK)


#define __rb_parent(pc)	((simblock *)((pc) & ~(BLOCK_ALIGN_MASK)))

#define __rb_color(pc) ((pc) & RB_COLOR_MASK)

#define __rb_is_black(pc) __rb_color (pc)

#define __rb_is_red(pc)	(!__rb_color (pc))


#define rb_parent(p) __rb_parent ((p)->__rb_parent_color)

#define rb_color(p) __rb_color ((p)->__rb_parent_color)

#define rb_is_black(p) __rb_is_black ((p)->__rb_parent_color)

#define rb_is_red(p) __rb_is_red ((p)->__rb_parent_color)


static __always_inline void
rb_set_parent (simblock *const node, simblock *const parent)
{
  node->__rb_parent_color = rb_color (node) + (bui_t) parent;
}

static __always_inline void
rb_set_parent_color (simblock *const node, simblock *const parent, int color)
{
  node->__rb_parent_color = (bui_t) parent + color;
}

static __always_inline void
__rb_change_child (simblock *const old, simblock *const new,
		   simblock *const parent, simblock **const root)
{
  if (parent)
    {
      if (parent->rb_left == old)
	parent->rb_left = new;
      else
	parent->rb_right = new;
    }
  else
    {
      if (*root != 0 && *root != old)
	warn_perror ("root != old");
      *root = new;
    }
}

static __always_inline void
__rb_rotate_set_parents (simblock *old, simblock *new,
			 simblock **root, int color)
{
  simblock *parent = rb_parent (old);
  new->__rb_parent_color = old->__rb_parent_color;
  rb_set_parent_color (old, new, color);
  __rb_change_child (old, new, parent, root);
}

static __always_inline void
rb_link_node (simblock *node, simblock *parent, simblock **link)
{
  node->__rb_parent_color = (bui_t) parent;
  node->rb_left = node->rb_right = NULL;

  *link = node;
}

static __always_inline simblock *
__rb_remove (simblock *node, simblock **const root)
{
  simblock *child = node->rb_right;
  simblock *tmp = node->rb_left;
  simblock *parent, *rebalance;
  bui_t pc;

  if (!tmp)
    {
      /*
       * Case 1: node to erase has no more than 1 child (easy!)
       *
       * Note that if there is one child it must be red due to 5)
       * and node must be black due to 4). We adjust colors locally
       * so as to bypass __rb_erase_color() later on.
       */
      pc = node->__rb_parent_color;
      parent = __rb_parent (pc);
      __rb_change_child (node, child, parent, root);

      if (child)
	{
	  child->__rb_parent_color = pc;
	  rebalance = NULL;
	}
      else
	rebalance = __rb_is_black (pc) ? parent : NULL;
    }
  else if (!child)
    {
      /* Still case 1, but this time the child is node->rb_left */
      tmp->__rb_parent_color = pc = node->__rb_parent_color;
      parent = __rb_parent (pc);
      __rb_change_child (node, tmp, parent, root);
      rebalance = NULL;
    }
  else
    {
      simblock *successor = child, *child2;
      tmp = child->rb_left;

      if (!tmp)
	{
	  /*
	   * Case 2: node's successor is its right child
	   *
	   *    (n)          (s)
	   *    / \          / \
	   *  (x) (s)  ->  (x) (c)
	   *        \
	   *        (c)
	   */
	  parent = successor;
	  child2 = successor->rb_right;
	}
      else
	{
	  /*
	   * Case 3: node's successor is leftmost under
	   * node's right child subtree
	   *
	   *    (n)          (s)
	   *    / \          / \
	   *  (x) (y)  ->  (x) (y)
	   *      /            /
	   *    (p)          (p)
	   *    /            /
	   *  (s)          (c)
	   *    \
	   *    (c)
	   */
	  do {
	      parent = successor;
	      successor = tmp;
	      tmp = tmp->rb_left;
	  } while (tmp);

	  child2 = successor->rb_right;
	  parent->rb_left = child2;
	  successor->rb_right = child;
	  rb_set_parent (child, successor);
	}

      tmp = node->rb_left;
      successor->rb_left = tmp;
      rb_set_parent (tmp, successor);

      pc = node->__rb_parent_color;
      tmp = __rb_parent (pc);
      __rb_change_child (node, successor, tmp, root);

      if (child2)
	{
	  rb_set_parent_color (child2, parent, RB_BLACK);
	  rebalance = NULL;
	}
      else
	rebalance = rb_is_black (successor) ? parent : NULL;

      successor->__rb_parent_color = pc;
    }

  return rebalance;
}

static __always_inline void
rb_set_black (simblock *node)
{
  node->__rb_parent_color += RB_BLACK;
}

static __always_inline simblock *
rb_red_parent (simblock *red)
{
  return (simblock *) red->__rb_parent_color;
}

static __always_inline void
__rb_remove_color (simblock *parent, simblock **root)
{
  simblock *sibling, *tmp1, *tmp2;
  simblock *node = NULL;

  for (;;)
    {
      sibling = parent->rb_right;

      if (node != sibling)
	{
	  if (rb_is_red (sibling))
	    {
	      /*
	       * Case 1 - left rotate at parent
	       *
	       *     P               S
	       *    / \             / \
	       *   N   s    -->    p   Sr
	       *      / \         / \
	       *     Sl  Sr      N   Sl
	       */
	      tmp1 = sibling->rb_left;
	      parent->rb_right = tmp1;
	      sibling->rb_left = parent;
	      rb_set_parent_color (tmp1, parent, RB_BLACK);
	      __rb_rotate_set_parents (parent, sibling, root, RB_RED);
	      sibling = tmp1;
	    }

	  tmp1 = sibling->rb_right;

	  if (!tmp1 || rb_is_black (tmp1))
	    {
	      tmp2 = sibling->rb_left;

	      if (!tmp2 || rb_is_black (tmp2))
		{
		  /*
		   * Case 2 - sibling color flip
		   * (p could be either color here)
		   *
		   *    (p)           (p)
		   *    / \           / \
		   *   N   S    -->  N   s
		   *      / \           / \
		   *     Sl  Sr        Sl  Sr
		   *
		   * This leaves us violating 5) which
		   * can be fixed by flipping p to black
		   * if it was red, or by recursing at p.
		   * p is red when coming from Case 1.
		   */
		  rb_set_parent_color (sibling, parent, RB_RED);

		  if (rb_is_red (parent))
		    rb_set_black (parent);
		  else
		    {
		      node = parent;
		      parent = rb_parent (node);
		      if (parent)
			continue;
		    }
		  break;
		}
	      /*
	       * Case 3 - right rotate at sibling
	       * (p could be either color here)
	       *
	       *   (p)           (p)
	       *   / \           / \
	       *  N   S    -->  N   sl
	       *     / \             \
	       *    sl  Sr            S
	       *                       \
	       *                        Sr
	       *
	       * Note: p might be red, and then both
	       * p and sl are red after rotation(which
	       * breaks property 4). This is fixed in
	       * Case 4 (in __rb_rotate_set_parents()
	       *         which set sl the color of p
	       *         and set p RB_BLACK)
	       *
	       *   (p)            (sl)
	       *   / \            /  \
	       *  N   sl   -->   P    S
	       *       \        /      \
	       *        S      N        Sr
	       *         \
	       *          Sr
	       */
	      tmp1 = tmp2->rb_right;
	      sibling->rb_left = tmp1;
	      tmp2->rb_right = sibling;
	      parent->rb_right = tmp2;

	      if (tmp1)
		rb_set_parent_color (tmp1, sibling, RB_BLACK);

	      tmp1 = sibling;
	      sibling = tmp2;
	    }
	  /*
	   * Case 4 - left rotate at parent + color flips
	   * (p and sl could be either color here.
	   *  After rotation, p becomes black, s acquires
	   *  p's color, and sl keeps its color)
	   *
	   *      (p)             (s)
	   *      / \             / \
	   *     N   S     -->   P   Sr
	   *        / \         / \
	   *      (sl) sr      N  (sl)
	   */
	  tmp2 = sibling->rb_left;
	  parent->rb_right = tmp2;
	  sibling->rb_left = parent;
	  rb_set_parent_color (tmp1, sibling, RB_BLACK);

	  if (tmp2)
	    rb_set_parent (tmp2, parent);

	  __rb_rotate_set_parents (parent, sibling, root, RB_BLACK);
	  break;
	}
      else
	{
	  sibling = parent->rb_left;

	  if (rb_is_red (sibling))
	    {
	      /* Case 1 - right rotate at parent */
	      tmp1 = sibling->rb_right;
	      parent->rb_left = tmp1;
	      sibling->rb_right = parent;
	      rb_set_parent_color (tmp1, parent, RB_BLACK);
	      __rb_rotate_set_parents (parent, sibling, root, RB_RED);
	      sibling = tmp1;
	    }

	  tmp1 = sibling->rb_left;

	  if (!tmp1 || rb_is_black (tmp1))
	    {
	      tmp2 = sibling->rb_right;

	      if (!tmp2 || rb_is_black (tmp2))
		{
		  /* Case 2 - sibling color flip */
		  rb_set_parent_color (sibling, parent, RB_RED);

		  if (rb_is_red (parent))
		    rb_set_black (parent);
		  else
		    {
		      node = parent;
		      parent = rb_parent (node);
		      if (parent)
			continue;
		    }
		  break;
		}

	      /* Case 3 - left rotate at sibling */
	      tmp1 = tmp2->rb_left;
	      sibling->rb_right = tmp1;
	      tmp2->rb_left = sibling;
	      parent->rb_left = tmp2;

	      if (tmp1)
		rb_set_parent_color (tmp1, sibling, RB_BLACK);

	      tmp1 = sibling;
	      sibling = tmp2;
	    }

	  /* Case 4 - right rotate at parent + color flips */
	  tmp2 = sibling->rb_right;
	  parent->rb_left = tmp2;
	  sibling->rb_right = parent;
	  rb_set_parent_color (tmp1, sibling, RB_BLACK);

	  if (tmp2)
	    rb_set_parent (tmp2, parent);

	  __rb_rotate_set_parents (parent, sibling, root, RB_BLACK);
	  break;
	}
    }
}

static __always_inline void
__rb_insert (simblock *node, simblock **root)
{
  simblock *gparent, *tmp;
  simblock *parent = rb_red_parent (node);

  for (;;)
    {
      /*
       * Loop invariant: node is red.
       */
      if (__builtin_expect (!parent, 0))
	{
	  rb_set_parent_color (node, NULL, RB_BLACK);
	  break;
	}

      /*
       * If there is a black parent, we are done.
       * Otherwise, take some corrective action as,
       * per 4), we don't want a red root or two
       * consecutive red nodes.
       */
      if (rb_is_black (parent))
	break;

      gparent = rb_red_parent (parent);
      tmp = gparent->rb_right;

      if (parent != tmp)
	{
	  if (tmp && rb_is_red (tmp))
	    {
	      /*
	       * Case 1 - node's uncle is red (color flips).
	       *
	       *       G            g
	       *      / \          / \
	       *     p   u  -->   P   U
	       *    /            /
	       *   n            n
	       *
	       * However, since g's parent might be red, and
	       * 4) does not allow this, we need to recurse
	       * at g.
	       */
	      rb_set_parent_color (tmp, gparent, RB_BLACK);
	      rb_set_parent_color (parent, gparent, RB_BLACK);
	      node = gparent;
	      parent = rb_parent (node);
	      rb_set_parent_color (node, parent, RB_RED);
	      continue;
	    }

	  tmp = parent->rb_right;

	  if (node == tmp)
	    {
	      /*
	       * Case 2 - node's uncle is black and node is
	       * the parent's right child (left rotate at parent).
	       *
	       *      G             G
	       *     / \           / \
	       *    p   U  -->    n   U
	       *     \           /
	       *      n         p
	       *
	       * This still leaves us in violation of 4), the
	       * continuation into Case 3 will fix that.
	       */
	      tmp = node->rb_left;
	      parent->rb_right = tmp;
	      node->rb_left = parent;

	      if (tmp)
		rb_set_parent_color (tmp, parent, RB_BLACK);

	      rb_set_parent_color (parent, node, RB_RED);
	      parent = node;
	      tmp = node->rb_right;
	    }

	  /*
	   * Case 3 - node's uncle is black and node is
	   * the parent's left child (right rotate at gparent).
	   *
	   *        G           P
	   *       / \         / \
	   *      p   U  -->  n   g
	   *     /                 \
	   *    n                   U
	   */
	  gparent->rb_left = tmp;
	  parent->rb_right = gparent;

	  if (tmp)
	    rb_set_parent_color (tmp, gparent, RB_BLACK);

	  __rb_rotate_set_parents (gparent, parent, root, RB_RED);
	  break;
	}
      else
	{
	  tmp = gparent->rb_left;

	  if (tmp && rb_is_red (tmp))
	    {
	      /* Case 1 - color flips */
	      rb_set_parent_color (tmp, gparent, RB_BLACK);
	      rb_set_parent_color (parent, gparent, RB_BLACK);
	      node = gparent;
	      parent = rb_parent (node);
	      rb_set_parent_color (node, parent, RB_RED);
	      continue;
	    }

	  tmp = parent->rb_left;

	  if (node == tmp)
	    {
	      /* Case 2 - right rotate at parent */
	      tmp = node->rb_right;
	      parent->rb_left = tmp;
	      node->rb_right = parent;

	      if (tmp)
		rb_set_parent_color (tmp, parent, RB_BLACK);

	      rb_set_parent_color (parent, node, RB_RED);
	      parent = node;
	      tmp = node->rb_left;
	    }

	  /* Case 3 - left rotate at gparent */
	  gparent->rb_right = tmp;
	  parent->rb_left = gparent;

	  if (tmp)
	    rb_set_parent_color (tmp, gparent, RB_BLACK);

	  __rb_rotate_set_parents (gparent, parent, root, RB_RED);
	  break;
	}
    }
}

static void
atree_remove (simblock *const node, simblock **const atree, simblock **const alist)
{
  simblock *parent, *tmp, *child, *fwd, *bwd;

  fwd = node->fwd;
  bwd = node->bwd;
  parent = rb_parent (node);
  child = node->rb_right;
  tmp = node->rb_left;

  if (fwd)
    {
      fwd->bwd = bwd;

      if (fwd->dtick == node->dtick)
	{
	  if (bwd)
	    bwd->fwd = fwd;
	  else
	    {
	      if (node != *alist)
		warn_error ("node != alist");
	      *alist = fwd;
	    }

	  bui_t pc;
	  fwd->__rb_parent_color = pc = node->__rb_parent_color;
	  parent = __rb_parent (pc);
	  __rb_change_child (node, fwd, parent, atree);
	  fwd->rb_right = child;
	  fwd->rb_left = tmp;

	  if (child)
	    rb_set_parent (child, fwd);
	  if (tmp)
	    rb_set_parent (tmp, fwd);

	  return;
	}
    }

  if (bwd)
    bwd->fwd = fwd;
  else
    {
      if (node != *alist)
	warn_error ("node != alist");
      *alist = fwd;
    }

  tmp = __rb_remove (node, atree);

  if (tmp)
    __rb_remove_color (tmp, atree);
}

static void
atree_insert (simblock *const p, simblock **const atree, simblock **const alist)
{
  const size_t dtick = p->dtick;
  simblock **link = atree;
  simblock *tmp, *node;
  simblock *parent = 0;
  long int cmp;

  p->rb_left = 0;
  p->rb_right = 0;
  p->__rb_parent_color = 0;

  while ((node = *link) && (cmp = (long int) dtick - (long int) node->dtick) != 0)
    {
      if (cmp < 0)
        link = &node->rb_left;
      else
        link = &node->rb_right;
      parent = node;
    }

  if (node == 0)
    {
      if (link == atree && *alist != 0)
	warn_error ("alist != 0");

      rb_link_node (p, parent, link);
      __rb_insert (p, atree);

      if (!parent)
	{
	  p->fwd = 0;
	  p->bwd = 0;
	  if (*alist != 0)
	    warn_error ("node != 0");
	  *alist = p;
	}
      else
	{
	  if (cmp < 0)
	    {
	      p->fwd = parent;
	      p->bwd = tmp = parent->bwd;
	      parent->bwd = p;

	      if (tmp)
		tmp->fwd = p;
	      else
		{
		  if (parent != *alist)
		    warn_error ("parent != alist");
		  *alist = p;
		}
	    }
	  else
	    {
	      tmp = parent->fwd;
	      while (tmp && tmp->dtick == parent->dtick)
		{
		  parent = tmp;
		  tmp = parent->fwd;
		}
	      p->bwd = parent;
	      p->fwd = tmp;
	      parent->fwd = p;

	      if (tmp)
		tmp->bwd = p;
	    }
	}

    }
  else
    {
      p->bwd = node;
      p->fwd = tmp = node->fwd;
      node->fwd = p;

      if (tmp)
	tmp->bwd = p;
    }

  if (p->fwd && p->fwd->dtick < p->dtick)
    warn_error ("mismatch");
  if (p->bwd && p->bwd->dtick > p->dtick)
    warn_error ("mismatch");
}

static __always_inline __attribute__ ((pure))
simblock *
atree_maximum (simblock *node)
{
  simblock *child = node->rb_right;

  while (child)
    {
      node = child;
      child = node->rb_right;
    }

  return node;
}

