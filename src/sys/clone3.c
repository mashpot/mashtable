/*
 * clone3.c
 *
 * Created 2 Sep 2023 by Mason Hall (mason@mashpot.net)
 *
 * Copyright (C) 2023 Mason Hall
 */

#define _GNU_SOURCE

#include <linux/sched.h>
#include <sched.h>
#include <sys/syscall.h>
#include <unistd.h>

#define SYS_CLONE3_C_
#include <bits/struct_clone3.h>

#include <globdefs.h>

long clone3 (struct clone3 *cl)
{
  return syscall (SYS_clone3, cl, sizeof (struct clone3));
}

EXPORT long clone3 (struct clone3 *cl);

