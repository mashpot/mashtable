/*
 * debug.c
 *
 * Created 21 Aug 2023 by Mason Hall (mason@mashpot.net)
 *
 * Copyright (C) 2023 Mason Hall
 */

#define _GNU_SOURCE

#include <stdio.h>
#include <stdlib.h>
#include <string.h>
#include <unistd.h>
#include <fcntl.h>
#include <errno.h>
#include <signal.h>
#include <sys/prctl.h>
#include <sys/ptrace.h>
#include <stdarg.h>
#include <sys/mman.h>
#include <time.h>

#include <mashlib.h>
#include <globdefs.h>
#include <sys/clone3.h>
#include <sys/pidfd.h>

static int
write_message (int fd, const char *buf, size_t len)
{
  size_t nb = 0;
  long int n = 0;

  while (nb < len)
    {
      n = TEMP_FAILURE_RETRY (write (fd, buf + nb, len - nb));
      if (n == -1)
          break;
      nb += n;
    }

  if (n == -1)
    {
      perror ("write_message");
      return -1;
    }
  return 0;
}

static size_t
funcstr (char *restrict buf, size_t len,
         const char *restrict func, int line)
{
  char *p = buf;

  if (len > 5)
    {
      *p++ = '[';
      p = stpncpy (p, func, len - 5);
      *p++ = ':';

      len = &buf[len] - p;

      p = ulongstr (p, len, line);
      *p++ = ']';
      *p++ = ' ';
      *p = 0;
    }

  return p - buf;
}

void
error_message (const char *restrict func, int line,
               const char *restrict format, ...)
{
  const size_t size = 4096;
  size_t len;
  ssize_t n;
  char *p;
  va_list ap;

  p = mmap (NULL, size, PROT_READ | PROT_WRITE, MAP_ANON | MAP_PRIVATE, -1, 0);

  if (__builtin_expect (p == MAP_FAILED, 0))
    {
      perror ("printerr:mmap()");
      return;
    }

  len = funcstr (p, size, func, line);

  va_start (ap, format);
  n = vsnprintf (p + len, size - len, format, ap);
  va_end (ap);

  if (n >= 0)
    len += n;
  else
    perror ("printerr:vsnprintf()");

  write_message (STDERR_FILENO, p, len);

  munmap (p, size);

  return;
}

#if DEBUG

static int debug_attached = 0;
static int debug_gdb_pidfd = -1;
static pid_t debug_gdb_pid = 0;

void
debug_gdb_kill (void)
{
  if (debug_attached || debug_gdb_pid)
    {
      if (kill (debug_gdb_pid, SIGTERM) == -1)
	printerrno ("kill");

      usleep (100000);

      kill (debug_gdb_pid, SIGKILL);

      debug_attached = 0;
      debug_gdb_pid = 0;
    }
}

void
debug_gdb_attach (void)
{
  struct clone3 cl = {
      .cl_flags = CLONE_CLEAR_SIGHAND | CLONE_VFORK | CLONE_PIDFD, //CLONE_PARENT |
      .cl_pidfd = (sys_t) &debug_gdb_pidfd
  };
  //pid_t ppid = getppid ();
  pid_t pid = getpid ();
  long ret;

  debug_attached = 1;
  ret = clone3 (&cl);

  if (ret == -1)
    fatal_perror ("clone3");

  if (ret == 0)
    {
      char argpid[6 + 21];

      snprintf (&argpid[0], sizeof (argpid)-1, "--pid=%u", pid);

      char *args[] = {
	  "gdb",
	  &argpid[0],
	  "--quiet",
	  "--ex=cont",
	  "--ex=s",
	  NULL
      };
      if (execve ("/bin/gdb", args, environ) == -1)
	fatal_perror ("execve");
      fatal_error ("execve returned an impossible value.");
    }
  else
    {
      debug_gdb_pid = ret;
    }
  usleep (100000);
  return;
}

static const int debug_trap_signals[] = {
//    SIGHUP,		/* Hangup.  */
//    SIGINT,		/* Interactive attention signal.  */
//    SIGQUIT,		/* Quit.  */
    SIGILL,		/* Illegal instruction.  */
    SIGTRAP,		/* Trace/breakpoint trap.  */
    SIGABRT,		/* Abnormal termination.  */
    SIGBUS,		/* Bus error.  */
    SIGFPE,		/* Erroneous arithmetic operation.  */
//    SIGKILL,		/* Killed.  */
    SIGSEGV,		/* Invalid access to storage.  */
    SIGPIPE,		/* Broken pipe.  */
//    SIGALRM,		/* Alarm clock.  */
//    SIGTERM,		/* Termination request.  */
//    SIGSTKFLT,		/* Stack fault (obsolete).  */
//    SIGCHLD,		/* Child terminated or stopped.  */
//    SIGCONT,		/* Continue.  */
//    SIGSTOP,		/* Stop, unblockable.  */
//    SIGTSTP,		/* Keyboard stop.  */
//    SIGTTIN,		/* Background read from control terminal.  */
//    SIGTTOU,		/* Background write to control terminal.  */
//    SIGURG,		/* Urgent data is available at a socket.  */
    SIGXCPU,		/* CPU time limit exceeded.  */
    SIGXFSZ,		/* File size limit exceeded.  */
//    SIGVTALRM,	/* Virtual timer expired.  */
//    SIGPROF,		/* Profiling timer expired.  */
//    SIGWINCH,		/* Window size change (4.3 BSD, Sun).  */
//    SIGPOLL,		/* Pollable event occurred (System V).  */
//    SIGPWR,		/* Power failure imminent.  */
//    SIGSYS,		/* Bad system call.  */
};
//__SIGRTMIN	32
//__SIGRTMAX	64

static const char *ptrace_events[] = {
    [ PTRACE_EVENT_STOP	      ] = "PTRACE_EVENT_STOP",	      /* 128 */
    [ PTRACE_EVENT_FORK	      ] = "PTRACE_EVENT_FORK",	      /*   1 */
    [ PTRACE_EVENT_VFORK      ] = "PTRACE_EVENT_VFORK",
    [ PTRACE_EVENT_CLONE      ] = "PTRACE_EVENT_CLONE",
    [ PTRACE_EVENT_EXEC	      ] = "PTRACE_EVENT_EXEC",
    [ PTRACE_EVENT_VFORK_DONE ] = "PTRACE_EVENT_VFORK_DONE",
    [ PTRACE_EVENT_EXIT	      ] = "PTRACE_EVENT_EXIT",
    [ PTRACE_EVENT_SECCOMP    ] = "PTRACE_EVENT_SECCOMP",     /*   7 */
    [ 8 ]			= "Unknown ptrace event in si_code.",     /*   7 */
};

static pid_t
get_tracer_pid (void)
{
  const size_t len = 4096;
  char *buf, *p;
  pid_t pid = 0;
  ssize_t n;
  int fd;

  buf = mmap (NULL, len, PROT_READ | PROT_WRITE, MAP_PRIVATE | MAP_ANON, -1, 0);

  if (buf == MAP_FAILED)
    {
      warn_perror ("mmap");
      return -1;
    }

  fd = TEMP_FAILURE_RETRY (open ("/proc/self/status", O_RDONLY));

  if (fd == -1)
    {
      warn_perror ("open");
      munmap (buf, len);
      return -1;
    }

  n = TEMP_FAILURE_RETRY (read (fd, &buf[0], len - 1));
  close (fd);

  if (n == -1)
    {
      warn_perror ("read");
      munmap (buf, len);
      return -1;
    }
  buf[n] = 0;

  p = strstr (&buf[0], "TracerPid:\t");

  if (p != NULL)
    {
      p += 11;
      n = strtoul (p, NULL, 10);

      if (n != -1)
	pid = (pid_t) n;
    }

  munmap (buf, len);

  return pid;
}

void
debug_trap (int sig, siginfo_t *si, void *uc)
{
  (void) uc;
  int code = si->si_code;

  fprintf (stderr, "debug_trap: caught SIG%s\n", sigabbrev_np (sig));

  if ((code & SIGTRAP) && (code & (255 << 8)))
    {
      /* PTRACE_EVENT_STOP is equal to 128, so we don't select that bit.
       * This zeroing allows us to use ptrace_events[0] for _STOP, as the
       * other events are within the range 1-7.
       */
      int ev = (code >> 8) & 128;
      if (ev > 7)
	ev = 8;
      fprintf (stderr, "debug_trap: %s\n", ptrace_events[ev]);
    }

  if (get_tracer_pid () != debug_gdb_pid)
    {
      debug_gdb_kill ();

      struct sigaction sa = {
	  .sa_flags = SA_RESETHAND | SA_NODEFER,
      };

      size_t n, i;
      n = sizeof (debug_trap_signals) / sizeof (debug_trap_signals[0]);

      for (i = 0; i < n; ++i)
	if (sigaction (debug_trap_signals[i], &sa, NULL) == -1)
	{
	  printerrno ("sigaction");
	  exit (1);
	}
      return;
    }
  if (debug_attached)
    return;

  debug_gdb_attach ();

  __BREAK
  return;
}

void __attribute__ ((used, visibility ("default"), constructor))
debug_setup (void)
{
  struct sigaction sa;
  size_t i, n;
  pid_t tracepid;

  tracepid = get_tracer_pid ();

  if (tracepid > 0)
    return;

  sa = (struct sigaction) {
      .sa_sigaction = debug_trap,
      .sa_flags = SA_SIGINFO,
  };
  sigemptyset (&sa.sa_mask);
  //sigaddset (&sa.sa_mask, SIGINT);
  sigaddset (&sa.sa_mask, SIGALRM);

  n = sizeof (debug_trap_signals) / sizeof (debug_trap_signals[0]);

  for (i = 0; i < n; ++i)
    {
      sigaddset (&sa.sa_mask, debug_trap_signals[i]);
    }

  for (i = 0; i < n; ++i)
    if (sigaction (debug_trap_signals[i], &sa, NULL) == -1)
    {
      printerrno ("sigaction");
      exit (1);
    }

  //printf ("debug_setup\n");

  return;
}
#endif

#if 0
void __attribute__ ((used, visibility ("default"), destructor))
debug_exit (void)
{
  siginfo_t info;
  int sig;

  if (debug_gdb_pidfd != -1)
    {
#if 0
      sig = SIGHUP;
      info = (siginfo_t) {
	  .si_signo = sig,
	  .si_code = 0,
	  .si_errno = 0,
	  .si_pid = getpid (),
	  .si_uid = getuid (),
      };

      if (pidfd_send_signal (debug_gdb_pidfd, sig, &info, 0) == -1)
	fatal_perror ("pidfd_send_signal()");
#endif
      sleep (1);
      kill (debug_gdb_pid, SIGHUP);
    }
}
#endif

#if 0
#if !DEBUG
/*
 * If not DEBUG make instrument functions noop. Though, the GCC option
 * -finstrument-functions should be disabled.
 */
void
__cyg_profile_func_enter (void *func, void *callsite)
{
}
strong_alias (__cyg_profile_func_enter, __cyg_profile_func_exit)
#else
void
__cyg_profile_func_enter (void *func, void *callsite)
{
}
strong_alias (__cyg_profile_func_enter, __cyg_profile_func_exit)
#endif

//extern void __cyg_profile_func_enter (void *this_fn, void *call_site);
//extern void __cyg_profile_func_exit (void *this_fn, void *call_site);
#endif

