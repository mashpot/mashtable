/*
 * region.c
 *
 * Created 11 Aug 2023 by Mason Hall <mason@mashpot.net>
 *
 * Copyright (C) 2023 Mason Hall
 */

#include <stdio.h>
#include <stdlib.h>
#include <stdint.h>
#include <string.h>
#include <sys/mman.h>
#include <assert.h>

#include <globdefs.h>
#include <mreg.h>

#include <mashlib.h>

#include "region.h"

/* Define offsetof */
#define offsetof __builtin_offsetof

/* Join string with macro value */
#define _CAT(A, B) A #B
#define CAT(A, B) _CAT (A, B)

/* Forward declarations */
struct mregion;
typedef struct mregion mregion;

struct mchunk;
typedef struct mchunk mchunk;

/*
 * --------------------------- Internal routines ------------------------------
 */

static void   _int_region_init	  (mregion *mreg, size_t bytes, size_t hdsz);
static bui_t  _int_region_malloc  (mregion *mreg, size_t bytes);
static void   _int_region_free	  (mregion *mreg, bui_t bi);
static bui_t  _int_region_realloc (mregion *, bui_t, size_t, size_t);
static void   region_consolidate  (mregion *mreg);

/*
 * ------------------------- Chunk representations ----------------------------
 */

struct mchunk
{
  bui_t _chunk_prevsize;
  bui_t _chunk_size;

  bui_t binfwd;
  bui_t binbwd;

  bui_t sizfwd;
  bui_t sizbwd;
};


typedef struct mchunk *mchunkptr;


#define MIN_CHUNK_SIZE	(offsetof (struct mchunk, sizfwd))


#define MINSIZE ((MIN_CHUNK_SIZE + BLOCK_ALIGN_MASK) & ~(BLOCK_ALIGN_MASK))


#define CHUNK_HDR_SIZE	(offsetof (struct mchunk, binfwd))


#define chunk2mem(p) ((__typeof__ (p)) ((uintptr_t) (p) + CHUNK_HDR_SIZE))
#define mem2chunk(p) ((__typeof__ (p)) ((uintptr_t) (p) - CHUNK_HDR_SIZE))

#define chunk_misaligned(p) ((((uintptr_t) (p) + CHUNK_HDR_SIZE) & BLOCK_ALIGN_MASK) != 0)


#define OFFSET_ALIGN	(offsetof (struct mchunk, _chunk_size))

#define req2size(sz) \
  (((sz) + OFFSET_ALIGN <= MINSIZE) ? \
   MINSIZE                         : \
   ((sz) + OFFSET_ALIGN + BLOCK_ALIGN_MASK) & ~(BLOCK_ALIGN_MASK))

/* leave the low n bits for flags */
#define NFLAGS (3UL)


/* bit set if previous chunk in use */
#define PREV_INUSE (0x1UL)

#define __prev_inuse(n) ((n) & PREV_INUSE)
#define prev_inuse(p) (__prev_inuse ((p)->_chunk_size))

#define clear_bit_prev_inuse(p) ((p)->_chunk_size &= ~(PREV_INUSE));

/* bit set if chunk is in is use */
#define IS_INUSE (0x2UL)

#define __is_inuse(n) ((n) & IS_INUSE)
#define is_inuse(p) (__is_inuse ((p)->_chunk_size))


/* size field is or'ed with IN_POOL if the chunk belongs to a pool region. */
#define IN_POOL (0x4UL)

/* check if chunk belongs to a pool region */
#define chunk_in_pool(p) ((p)->_chunk_size & IN_POOL)

/* set chunk in pool flag */
#define set_in_pool(p) ((p)->_chunk_size |= IN_POOL)

#define clr_in_pool(p) ((p)->_chunk_size &= ~(IN_POOL))

/* bit flag mask */
#define CHK_FLAG_MASK (15UL)
#define CHK_FLAG_MASK_INUSE (CHK_FLAG_MASK & ~(IS_INUSE))

_Static_assert (CHK_FLAG_MASK <= BLOCK_ALIGN_MASK, "CHK_FLAG_MASK > BLOCK_ALIGN_MASK");

/* extract size without flag bits */
#define __chunksize(n) ((n) & ~(CHK_FLAG_MASK))
#define chunksize(p) (__chunksize ((p)->_chunk_size))


/* return previous chunk size */
#define prev_size(p) ((p)->_chunk_prevsize)

#define set_prev_size(p, sz) ((p)->_chunk_prevsize = (sz))


/* moving around */
#define next_chunk(p) ((mchunk *) (((char *) (p)) + chunksize (p)))

#define prev_chunk(p) ((mchunk *) (((char *) (p)) - prev_size (p)))


/* chunk at index/offset from pointer */
#define chunk_at_offset(p, s) \
  ((mchunk *) __builtin_assume_aligned ((((char *) (p)) + (s)), 8))

/* An address that is offset by an index of zero will be unchanged, equivalent
 * to dereferencing a null pointer, this also has the potential of going
 * unnoticed until the resulting usage of the base address causes issues. In
 * our case an index of zero will point to the region header, writes to which
 * may be fatal.
 *
 * While debugging new changes it may be helpful for zero to result in a null
 * pointer, usage of which will produce a segfault at the point of error.
 */

#if 0 && DEBUG
# define __chunk(p, i) ((i) ? chunk_at_offset (p, i) : NULL)
#else
# define __chunk(p, i) chunk_at_offset (p, i)
#endif

/* chunk pointer at index from region */
#define chunk(p, i) (((void) ((p) == ((struct mregion *) (p)))), __chunk (p, i))

/* inverse of the above, get the index of a chunk pointer from region */
#define chunkindex(reg, p) (((char*)(p)) - ((char*)(reg)))


#define ptr_bi(r, p) ((bui_t) (((char *)(p)) - ((char *)(r))))


/* set head size/flags */
#define set_head(p, s) ((p)->_chunk_size = (s))

#define get_head(p) ((p)->_chunk_size)


/* set foot size */
#define _set_foot(p, sz, s) ((chunk_at_offset (p, sz))->_chunk_prevsize = (s))

//#define set_foot(p, s) (_set_foot (p, chunksize (p)))
#define set_foot(p, sz) ((chunk_at_offset (p, sz))->_chunk_prevsize = (sz))


/* set chunk size at head */
#define set_size_head(p, sz) ((p)->_chunk_size = (((p)->_chunk_size & CHK_FLAG_MASK) | (sz)))

#define set_size_head_inuse(p, sz) ((p)->_chunk_size = (((p)->_chunk_size & CHK_FLAG_MASK) | (sz) | IS_INUSE))

/* set chunk size at foot */
#define set_size_foot(p, sz) ((chunk_at_offset (p, sz))->_chunk_prevsize = (sz))

/* set/clear inuse flag */
#if 0
#define _set_inuse(p, sz) ((chunk_at_offset (p, sz))->_chunk_size |= PREV_INUSE)

#define set_inuse(p) (_set_inuse (p, chunksize (p)))


#define _clr_inuse(p, sz) ((chunk_at_offset (p, sz))->_chunk_size &= ~(PREV_INUSE))

#define clr_inuse(p) (_clr_inuse (p, chunksize (p)))


#define _is_inuse(p, sz) ((chunk_at_offset (p, sz))->_chunk_size & PREV_INUSE)

#define is_inuse(p) (_is_inuse (p, chunksize (p)))

#else

#if 1
static __always_inline void
__set_inuse_prev (mchunk *p, const size_t size)
{
  chunk_at_offset (p, size)->_chunk_size |= PREV_INUSE;
}

static __always_inline void
__set_inuse (mchunk *p, const size_t size)
{
  p->_chunk_size |= IS_INUSE;
  chunk_at_offset (p, size)->_chunk_size |= PREV_INUSE;
}

static __always_inline void
__clr_inuse (mchunk *p, const size_t size)
{
  p->_chunk_size &= ~(IS_INUSE);
  chunk_at_offset (p, size)->_chunk_size &= ~(PREV_INUSE);
}

static __always_inline void
___set_inuse (mchunk *p)
{
  bui_t head = p->_chunk_size;
  p->_chunk_size = head | IS_INUSE;
  head &= ~(CHK_FLAG_MASK);
  chunk_at_offset (p, head)->_chunk_size |= PREV_INUSE;
}

static __always_inline void
___clr_inuse (mchunk *p)
{
  bui_t head = p->_chunk_size;
  p->_chunk_size = head & ~(IS_INUSE);
  head &= ~(CHK_FLAG_MASK);
  chunk_at_offset (p, head)->_chunk_size &= ~(PREV_INUSE);
}

#else

#define __set_inuse(p, sz) do						\
  {									\
    (p)->_chunk_size |= IS_INUSE;					\
    chunk_at_offset ((p), (sz))->_chunk_size |= PREV_INUSE;		\
  }									\
while (0)

#define __clr_inuse(p, sz) do						\
  {									\
    (p)->_chunk_size &= ~(IS_INUSE);					\
    chunk_at_offset ((p), (sz))->_chunk_size &= ~(PREV_INUSE);		\
  }									\
while (0)

#endif

#define set_inuse(p) ___set_inuse ((p))

#define clr_inuse(p) ___clr_inuse ((p))

#define _set_inuse __set_inuse
#define _clr_inuse __clr_inuse

#define _is_inuse(p, sz) (is_inuse (p))

#endif


/*
 * ------------------------ Internal data structures --------------------------
 */

/*
 * Bins
 *
 *  An array of bin headers for free chunks. Each bin is doubly linked to free
 *  chunks, with the bin itself acting as an mchunk to simplify linking. We use
 *  only a pair of indicies that are accessed at offset in such a way that
 *  aligns with the binfwd/binbwd indices of an mchunk structure.
 *
 *  As with GNU malloc, there are currently 128 bins. This may look excessive,
 *  but as GNU malloc states, this does work well in practice. I would argue
 *  that as memory cost reduces, allowing capacity of both main memory and CPU
 *  cache to increase, it may also be of benefit to increase the number of
 *  bins. Furthermore, I have had thought the concept of dynamic binning,
 *  weather that be the amount bins or range of chunk sizes, though I cannot
 *  find a way to perform this efficiently.
 *
 *  ...
 *
 */

typedef struct mchunk *mbinptr;

#define bin_at(r, i) \
  ((mbinptr) (((char *) &((r)->bi_bin[(i) * 2])) \
	      - offsetof (struct mchunk, binfwd)))

#define bin_bi(i) \
  (offsetof (struct mregion, bi_bin[(i) * 2]) \
   - offsetof (struct mchunk, binfwd))


#define next_bin(b) ((mbinptr) ((char *) (b) + offsetof (struct mchunk, binfwd)))
#define next_binbi(b) ((bui_t) ((b) + offsetof (struct mchunk, binfwd)))


#define NBINS		128UL
#define NSMALLBINS	 64UL
#define SMALLBIN_WIDTH	BLOCK_ALIGNMENT
#define MIN_LARGE_SIZE	(NSMALLBINS * SMALLBIN_WIDTH)

#define in_smallbin_range(sz)	((sz) < MIN_LARGE_SIZE)

#define smallbin_index(sz)	((sz) >> 4)

#define largebin_index(sz) \
  ((((sz) >>  6) <= 48) ?  48 + ((sz) >>  6) : \
   (((sz) >>  9) <= 20) ?  91 + ((sz) >>  9) : \
   (((sz) >> 12) <= 10) ? 110 + ((sz) >> 12) : \
   (((sz) >> 15) <=  4) ? 119 + ((sz) >> 15) : \
   (((sz) >> 18) <=  2) ? 124 + ((sz) >> 18) : \
   (((sz) >> 20) <=  1) ? 126 + ((sz) >> 20) : \
   127)

#define NBINBI	bin_bi (NBINS)

/* Helper functions that assume `struct mregion *mreg' is in the scope. */
#define _binfwd(p) (chunk (mreg, p->binfwd))
#define _binbwd(p) (chunk (mreg, p->binbwd))
#define _sizfwd(p) (chunk (mreg, p->sizfwd))
#define _sizbwd(p) (chunk (mreg, p->sizbwd))

/*
 * Unsorted bin
 *
 *  All chunks returned from use, besides those that fit fastbins, as well as
 *  small-size remainder chunks, will consolidate their neighbors before being
 *  placed in the unsorted bin. They are then placed in regular bins after
 *  region_malloc() gives them ONE chance to be used, or consolidation with
 *  other chunks freed or released from fastbins.
 *
 *  Also, in practice, programs tend allocate a burst of chunks then return
 *  most or all of them. So, the unsorted bin avoids having to chunks inserted
 *  in sorted bins only for them to be immediately reused.
 */

#define unsorted_bin(r)		(bin_at (r, 0))
#define unsorted_binbi		(bin_bi (0))

#define _unsorted_bin		(bin_at (mreg, 0))

/*
 * Remainder bin
 *
 *  All large remainder chunks are placed in the remainder bin. When a large
 *  number of neighboring chunks are freed they will be consolidated into
 *  a large chunk, to efficiently reclaim that space the remainder bin should
 *  hold chunks identified as being towards the end (low-address).
 *
 *  The large chunks toward the top should be kept free for consolidation with
 *  the top-most chunk, which then allows storage to be given back to the
 *  system.
 */

#define REMAINDERBIN 1

#define remainder_binidx	(1)
#define remainder_bin(r)	(bin_at (r, 1))
#define remainder_binbi		(bin_bi (1))

#define MIN_REMAINDER_SIZE	(48*1024UL)//65536UL)

#define middle_index(top) ((top) > 262144UL ? ((top) >> 1) : 131072UL)

#define in_remainder_size(sz) ((sz) >= MIN_REMAINDER_SIZE)

/*
 * Binmap
 */

#define BINMAPSHIFT	  (5)
#define BITSPERMAP	  (1U << BINMAPSHIFT)
#define BINMAPSIZE	  (NBINS / BITSPERMAP)

#define idx2block(i)	  ((i) >> BINMAPSHIFT)
#define idx2bit(i)	  ((1U << ((i) & ((1U << BINMAPSHIFT) - 1))))

#define mark_bin(r, i)	  ((r)->binmap[idx2block (i)] |= idx2bit (i))
#define unmark_bin(r, i)  ((r)->binmap[idx2block (i)] &= ~(idx2bit (i)))
#define get_binmap(r, i)  ((r)->binmap[idx2block (i)] & idx2bit (i))

/*
 * Fastbins
 *
 *  An array of recently freed small chunks, that single-linked with other
 *  same-size chunks, all of which keep their inuse bits set so they cannot be
 *  consolidated. The fastbins are consolidated only by region_malloc(), where
 *  chunks are placed in the unsorted bin before it is processed.
 */

typedef bui_t *mfastbinptr;

#define fastbin(r, idx) ((r)->bi_fastbin[idx])

#define fastbin_index(sz)	(((sz) >> 4) - 2)

#define MAX_FAST_SIZE	(80 * BUI_SZ / 4)
#define NFASTBINS	(fastbin_index (req2size (MAX_FAST_SIZE)) + 1)

#define FASTBIN_CONSOLIDATION_THRESHOLD (65536UL)

/*
 * ------------------------------- Statistics ---------------------------------
 */

struct regioninfo
{
  /*
   * Region runtime stats (reset on initialization).
   */
  bui_t nchks;	/* total number of chunks (includes the bi_top chunk) */
  bui_t nused;	/* number of used chunks */
  bui_t bused;	/* number of used bytes */
  /*
   * Cumulative remainder bin runtime stats.
   */
  long remainder_add_count; /* number of chunks added */
  long remainder_pop_count; /* number of chunks removed */
  long remainder_use_count; /* number of chunks removed for use */
};

#define REGION_STATS	0
#define REMAINDER_STATS 1

#if REGION_STATS
/* Global statistics structure */
static struct regioninfo stats = { 0 };
#endif

/*
 * ----------- Internal region representation and initialization --------------
 */

struct mregion
{
  bui_t _chunk_prevsize;

  bui_t _chunk_size;

  unsigned int head_size;

  int have_fastchunks;

  bui_t bi_fastbin[NFASTBINS];

  bui_t bi_top;

  bui_t bi_bin[NBINS * 2];

  unsigned int binmap[BINMAPSIZE];
};

#define REGION_HDR_SZ (sizeof (struct mregion) & ~15)

_Static_assert (sizeof (struct mregion) <= REGION_HDR_SZ,
		CAT ("sizeof (struct mregion) > ", REGION_HDR_SZ));


#define _regionsize(p) ((p)->_chunk_size)

#define regionsize(p) ((p)->_chunk_size & ~(CHK_FLAG_MASK))

#define _set_region_size(p, sz) ((p)->_chunk_size = (sz))

#define set_region_size(p, sz) \
  ((p)->_chunk_size = (((p)->_chunk_size & CHK_FLAG_MASK) | (sz)))


#define _REGION_MIN_SZ (REGION_HDR_SZ + MINSIZE)

#define REGION_MIN_SZ (REGION_HDR_SZ + MINSIZE + MINSIZE)

#define _PREGION_MIN_SZ (PREGION_HDR_SZ + MINSIZE)

#define PREGION_MIN_SZ (REGION_HDR_SZ + SUBREG_SZ + MINSIZE + MINSIZE)


#define PAGE_SIZE_MASK (PAGE_SIZE - 1)

#define REGION_ALIGNMENT (4096)

#define REGION_ALIGN_MASK (REGION_ALIGNMENT - 1)

#define region_align(sz) \
  ((sz) + REGION_ALIGN_MASK < REGION_MIN_SZ \
   ? REGION_MIN_SZ \
   : ((sz) + REGION_ALIGN_MASK) & ~(REGION_ALIGN_MASK))


#pragma GCC poison _chunk_size
#pragma GCC poison _chunk_prevsize


/* Unlink chunk from bin list. */
static void
unlink_chunk (mregion *mreg, bui_t bi)
{
  mchunk *p = chunk (mreg, bi);
  bui_t bfd = p->binfwd;
  mchunk *fd = _binfwd (p);
  bui_t bbk = p->binbwd;
  mchunk *bk = _binbwd (p);

  if (__builtin_expect (chunksize (p) != prev_size (next_chunk (p)), 0))
    fatal_error ("chunk->size != next->prev_size");

  if (__builtin_expect (fd->binbwd != bi, 0)
      || __builtin_expect (bk->binfwd != bi, 0))
    fatal_error ("corrupt chunk bin linked list");
  fd->binbwd = bbk;
  bk->binfwd = bfd;

  if (!in_smallbin_range (chunksize (p)) && p->sizfwd != 0)
    {
      if (__builtin_expect (_sizfwd (p)->sizbwd != bi, 0)
	  || __builtin_expect (_sizbwd (p)->sizfwd != bi, 0))
	fatal_error ("corrupt chunk sorted linked list");

      if (bfd > NBINBI && fd->sizfwd == 0)
	{
	  if (__builtin_expect (get_head (fd) != get_head (p), 0))
	    fatal_error ("corrupt chunk sorted linked list");

	  if (p->sizfwd == bi)
	    fd->sizfwd = fd->sizbwd = bfd;
	  else
	    {
	      fd->sizfwd = p->sizfwd;
	      fd->sizbwd = p->sizbwd;
	      _sizfwd (p)->sizbwd = bfd;
	      _sizbwd (p)->sizfwd = bfd;
	    }
	}
      else
	{
	  _sizfwd (p)->sizbwd = p->sizbwd;
	  _sizbwd (p)->sizfwd = p->sizfwd;
	}
    }
}

/*
 * Initialize mregion structure.
 */

static void
_int_region_init (mregion *mreg, size_t bytes, size_t hdsz)
{
  bui_t bi = block_align (hdsz);
  bui_t size;
  mchunk *chk;
  mbinptr bin;
  unsigned int i;

#if REGION_STATS
  /*
   * Reset region runtime stats.
   * TODO: Handle multiple regions, though stats only for development.
   */
  stats.nchks = 1; /* includes top chunk */
  stats.nused = 0;
  stats.bused = 0;
#endif

  mreg->head_size = bi;
  mreg->bi_top = bi;

  /* Clear fastbins */
  memset (&fastbin (mreg, 0), 0, NFASTBINS * sizeof (fastbin (mreg, 0)));

  /* Initalizse circular link for normal bins */
  for (i = 0; i < NBINS; i++)
    {
      bin = bin_at (mreg, i);
      bin->binfwd = bin->binbwd = bin_bi (i);
    }

  /* Zero binmap */
  memset (&mreg->binmap[0], 0, BINMAPSIZE * sizeof (mreg->binmap[0]));

  /* Setup top chunk */
  size = bytes - bi;
  chk = chunk (mreg, bi);
  set_prev_size (chk, 0);
  set_head (chk, size | IS_INUSE | PREV_INUSE);
}

/*
 * ------------------------ Public wrapper/routines ---------------------------
 */

int
region_init (void *mem, size_t size)
{
  uintptr_t align;

  if (__builtin_expect (size < REGION_MIN_SZ, 0))
    {
      warn_error ("size is insufficient");
      return -1;
    }

  align = size & PAGE_SIZE_MASK;

  if (__builtin_expect (align > 0, 0))
    {
      warn_error ("size is not page aligned");
      return -1;
    }

  align = (uintptr_t) mem & PAGE_SIZE_MASK;

  if (__builtin_expect (align > 0, 0))
    {
      warn_error ("mem is not page aligned");
      return -1;
    }
  set_region_size ((mregion *) mem, size);
  _int_region_init (mem, size, REGION_HDR_SZ);

  return 0;
}

void
region_free (void *region, bui_t bi)
{
  mregion *mreg;
  bui_t bchk;

  if (bi == 0)
    return;

  mreg = region;
  bchk = mem2chunk (bi);

  _int_region_free (mreg, bchk);
}

bui_t
region_malloc (void *region, size_t bytes)
{
  mregion *mreg;
  bui_t bi;

  mreg = region;
  bi = _int_region_malloc (mreg, bytes);

  return bi;
}

bui_t
region_realloc (void *region, bui_t bi, size_t bytes)
{
  mregion *mreg;
  size_t reqsize;
  bui_t bchk;

  if (bytes == 0 && bi != 0)
    {
      region_free (region, bi);
      return 0;
    }

  if (bi == 0)
    return region_malloc (region, bytes);

  mreg = region;

  const bui_t bold = mem2chunk (bi);
  const mchunk *old = chunk (mreg, bold);
  const bui_t oldsize = chunksize (old);

  size_t usable = mem2chunk (oldsize);

  if (bytes <= usable && usable - bytes < BLOCK_ALIGNMENT)
    return bi;

  if (__builtin_expect (bold + oldsize > mreg->bi_top, 0)
      || __builtin_expect (chunk_misaligned (bold), 0))
    fatal_error ("region_realloc(): invalid chunk index");

  reqsize = req2size (bytes);

  if (reqsize == 0)
    return 0;

  bchk = _int_region_realloc (mreg, bold, oldsize, reqsize);

  return bchk;
}

void
region_print (void *region)
{
  mregion *mreg = region;
  bui_t bchk = mreg->head_size;
  mchunk *chk = chunk (mreg, bchk);
  bui_t size;
  unsigned long i, n, nmax = ((base10_ndigits (regionsize (mreg)) + 7) & ~7) >> 3;
  do
    {
      size = chunksize (chk);
#if 1
      n = nmax - (base10_ndigits (bchk) >> 3);
      printf ("[%s]\t%lu", is_inuse (chk) ? "used" : "free", bchk);
      for (i = 0; i < n; ++i)
	putchar ('\t');
      printf ("-\t%lu\n", size);
#else
      if (!is_inuse (chk))
	printf ("%lu\n", size);
#endif
      bchk += size;
      chk = chunk_at_offset (chk, size);
    }
  while (bchk <= mreg->bi_top);
  putchar ('\n');
}

void
region_print_bins (void *region)
{
  mregion *mreg = region;
  (void) mreg;
#if 0
  unsigned int idx;
  mfastbinptr fb;
  mbinptr bin;
  mchunk *chk;
  bui_t bi, count, nfree;
  int didskip;

#if REGION_STATS
  nfree = stats.nchks - stats.nused;
#else
  nfree = mreg->bi_top;
#endif

  printf (" ---------------------\n"
	  " --- Fastbins [%2lu] ---\n"
	  " ---------------------\n"
	  "   idx | count\n"
	  " ---------------------\n", NFASTBINS);

  for (didskip = 0, idx = 0; idx < NFASTBINS; ++idx)
    {
      fb = &fastbin (mreg, idx);
      count = 0;

#define _nextfastlink(bi) \
  ((mfastbinptr) ((char *) mreg + bi + offsetof (mchunk, binfwd)))

      while ( (bi = *fb) )
	{
	  fb = _nextfastlink (bi);
	  ++count;
	}

      if (!count)
	++didskip;
      else
	{
	  if (didskip)
	    {
	      printf ("  [%3u]\n", idx - didskip);
	      if (didskip > 2)
		puts ("   ...");
	      if (didskip > 1)
		printf ("  [%3u]\n", idx - 1);
	      didskip = 0;
	    }
	  printf ("  [%3u]%7lu\n", idx, count);
	}
    }
  if (didskip > 2)
    printf ("  [%3u]\n   ...\n  [%3u]\n", idx - didskip, idx - 1);

  printf (" ---------------------\n"
	  " ---   Bins  [%3lu] ---\n"
	  " ---------------------\n"
	  "   idx | count | pct\n"
	  " ---------------------\n", NBINS);
//	  "  [999]   2710   00.99 "

  for (didskip = 0, idx = 0; idx < NBINS; ++idx)
    {
      bin = bin_at (mreg, idx);
      chk = _binfwd (bin);
      count = 0;

      while (chk != bin)
	{
	  chk = _binfwd (chk);
	  ++count;
	}

      if (!count)
	++didskip;
      else
	{
	  if (didskip)
	    {
	      printf ("  [%3u]\n", idx - didskip);
	      if (didskip > 2)
		puts ("   ...");
	      if (didskip > 1)
		printf ("  [%3u]\n", idx - 1);
	      didskip = 0;
	    }
	  printf ("  [%3u]%7lu   %4.1f\n", idx, count, (float) (count * 100) / (float) nfree);
	}
    }
#endif
#if REGION_STATS
  printf ("nchks: %lu\n"
	  "nused: %lu\n"
	  "bused: %lu\n"
	  "bitop: %lu\n"
	  "rsize: %lu\n",
	  stats.nchks,
	  stats.nused,
	  stats.bused,
	  mreg->bi_top,
	  regionsize (mreg));
# if REMAINDER_STATS
  printf ("remainder_add: %lu\n"
	  "remainder_pop: %lu\n"
	  "remainder_use: %lu\n",
	  stats.remainder_add_count,
	  stats.remainder_pop_count,
	  stats.remainder_use_count);
# endif
#endif
}

/*
 * ------------------------------ region_free ---------------------------------
 */

static void
_int_region_free (mregion *mreg, bui_t bi)
{
  mchunk *chk;
  bui_t head;
  bui_t size;
  bui_t bfwd;
  mchunk *fwd;
  bui_t bbwd;
  mchunk *bwd;
  bui_t bnxt;
  mchunk *next;
  bui_t nsiz;

  mfastbinptr fb;

  //bi = mem2chunk (bi);
  chk = chunk (mreg, bi);
  head = get_head (chk);
  size = __chunksize (head);

#if REGION_STATS
  stats.nused --;
  stats.bused -= size;
#endif

#if 0
  if (__builtin_expect (!__is_inuse (head), 0))
    fatal_error ("double free or corruption (!IS_INUSE)");
#endif
  if (__builtin_expect (chunk_misaligned (bi), 0)
      || __builtin_expect ((uintptr_t) chk > (uintptr_t) -size, 0))
    fatal_error ("invalid chunk index");
  if (__builtin_expect (size < MINSIZE, 0)
      || __builtin_expect (chunk_misaligned (size), 0))
    fatal_error ("invalid chunk size");

  {
    if (__builtin_expect (get_head (chunk_at_offset (chk, size))
			  <= CHUNK_HDR_SIZE, 0)
	|| __builtin_expect (get_head (chunk_at_offset (chk, size))
			     >= regionsize (mreg), 0))
      fatal_error ("invalid next chunk size");
  }

  if (size <= MAX_FAST_SIZE && bi + size != mreg->bi_top)
    {
      mreg->have_fastchunks = 1;
      unsigned int idx = fastbin_index (size);
      fb = &fastbin (mreg, idx);
      if (__builtin_expect (*fb == bi, 0))
	fatal_error ("double free or corruption (fd == bi)");
      chk->binfwd = *fb;
      *fb = bi;
    }
  else
    {
      bnxt = bi + size;
      next = chunk_at_offset (chk, size);
      nsiz = get_head (next);

      /* Ensure chunk being freed is, in fact, not free. The top chunk
       * borders the end-most inuse chunk and spans the available space at the
       * end of a region (highest address). Any chunk being freed that are
       * end-most, bordering with top, must consolidate becoming the new
       * top chunk.
       */
      if (__builtin_expect (bnxt > mreg->bi_top, 0))
	fatal_error ("double free or corruption (nxt > top)");

      /* Ask our next neighbor for consensus on whether we are truly inuse. */
      if (__builtin_expect (!__prev_inuse (nsiz), 0))
	fatal_error ("double free or corruption (!PREV_INUSE)");

      /* Unanimous agreement has been formed, and credibility was checked.
       * Consolidation with our prev neighbor can be attempted.
       */
      if (!__prev_inuse (head))
	{
	  bui_t prevsize = prev_size (chk);
	  size += prevsize;
	  bi -= prevsize;
	  chk = chunk_at_offset (chk, -((long) prevsize));
	  if (__builtin_expect (chunksize (chk) != prevsize, 0))
	    fatal_error ("mismatching chunk->prev_size vs prev->chunk_size");
	  unlink_chunk (mreg, bi);
#if REGION_STATS
	  stats.nchks--;
#endif
	}

      if (bnxt != mreg->bi_top)
	{
	  if (!__is_inuse (nsiz))
	    {
	      size += __chunksize (nsiz);
	      unlink_chunk (mreg, bnxt);
#if REGION_STATS
	      stats.nchks--;
#endif
	    }
	  else
	    clear_bit_prev_inuse (next);

	  bbwd = unsorted_binbi;
	  bwd = chunk (mreg, bbwd);
	  bfwd = bwd->binfwd;
	  fwd = chunk (mreg, bfwd);
	  if (__builtin_expect (fwd->binbwd != bbwd, 0))
	    fatal_error ("unsorted chunk link corrupted");
	  chk->binfwd = bfwd;
	  chk->binbwd = bbwd;
	  if (!in_smallbin_range (size))
	    {
	      chk->sizfwd = 0;
	      chk->sizbwd = 0;
	    }
	  fwd->binbwd = bi;
	  bwd->binfwd = bi;

	  set_head (chk, size | PREV_INUSE);
	  set_foot (chk, size);
	}
      else
	{
	  size += __chunksize (nsiz);
	  set_head (chk, size | IS_INUSE | PREV_INUSE);
	  mreg->bi_top = bi;
#if REGION_STATS
	  stats.nchks--;
#endif
	}
      if (size >= FASTBIN_CONSOLIDATION_THRESHOLD && mreg->have_fastchunks)
	region_consolidate (mreg);
    }
}

/*
 * --------------------------- region_consolidate -----------------------------
 */

static void
region_consolidate (mregion *mreg)
{
  mfastbinptr fb;
  mfastbinptr maxfb;
  bui_t nextbi;
  bui_t bbin;
  mchunk *bin;
  bui_t bfwd;
  bui_t bchk;
  mchunk *chk;
  bui_t bnxt;
  mchunk *next;
  bui_t head;
  bui_t size;
  bui_t nsiz;
  bui_t prevsize;

  mreg->have_fastchunks = 0;

  bbin = unsorted_binbi;
  bin = unsorted_bin (mreg);

  maxfb = &fastbin (mreg, NFASTBINS - 1);
  fb = &fastbin (mreg, 0);
  do
    {
      bchk = *fb;
      *fb = 0;
      if (bchk != 0)
	{
	  do
	    {
	      chk = chunk (mreg, bchk);
	      head = get_head (chk);
	      size = __chunksize (head);

	      {
		if (__builtin_expect (bchk < NBINBI, 0)
		    || __builtin_expect (chunk_misaligned (bchk), 0))
		  fatal_error ("unaligned minbin chunk index");

		unsigned int idx = fastbin_index (size);
		if ((&fastbin (mreg, idx)) != fb)
		  fatal_error ("invalid chunk size");
	      }

	      nextbi = chk->binfwd;

	      bnxt = bchk + size;
	      next = chunk_at_offset (chk, size);
	      nsiz = get_head (next);

	      if (!__prev_inuse (head))
		{
		  prevsize = prev_size (chk);
		  size += prevsize;
		  bchk -= prevsize;
		  chk = chunk_at_offset (chk, -((long) prevsize));
		  if (__builtin_expect (chunksize (chk) != prevsize, 0))
		    fatal_error ("chunk->prev_size != prev->chunk_size (minbin)");
		  unlink_chunk (mreg, bchk);
#if REGION_STATS
		  stats.nchks--;
#endif
		}

	      if (bnxt != mreg->bi_top)
		{
		  if (!__is_inuse (nsiz))
		    {
		      size += __chunksize (nsiz);
		      unlink_chunk (mreg, bnxt);
#if REGION_STATS
		      stats.nchks--;
#endif
		    }
		  else
		    clear_bit_prev_inuse (next);

		  bfwd = bin->binfwd;
		  bin->binfwd = bchk;
		  chunk (mreg, bfwd)->binbwd = bchk;

		  if (!in_smallbin_range (size))
		    {
		      chk->sizfwd = 0;
		      chk->sizbwd = 0;
		    }

		  set_head (chk, size | PREV_INUSE);
		  chk->binbwd = bbin;
		  chk->binfwd = bfwd;
		  set_foot (chk, size);
		}
	      else
		{
		  size += __chunksize (nsiz);
		  set_head (chk, size | IS_INUSE | PREV_INUSE);
		  mreg->bi_top = bchk;
#if REGION_STATS
		  stats.nchks--;
#endif
		}
	    }
	  while ( (bchk = nextbi) != 0);
	}
    }
  while (fb++ != maxfb);
}

/*
 *
 */

#if REMAINDERBIN
static void
region_empty_remainder_bin (mregion *mreg)
{
  bui_t bbin;
  mbinptr bin;
  bui_t bfwd;
  mchunk *fwd;
  bui_t bbwd;
  mchunk *bwd;
  bui_t bchk;
  mchunk *chk;

  unmark_bin (mreg, remainder_binidx);

  bbin = remainder_binbi;
  bin = chunk (mreg, bbin);
  bfwd = bin->binfwd;
  bbwd = bin->binbwd;

  if (bbwd != bbin)
    {
      bin->binfwd = bin->binbwd = bbin;
      fwd = chunk (mreg, bfwd);
      bwd = chunk (mreg, bbwd);
      if (__builtin_expect (bwd->binfwd != bbin, 0)
	  || __builtin_expect (fwd->binbwd != bbin, 0))
	fatal_error ("remainder bin link corrupted");

      bbin = unsorted_binbi;
      fwd->binbwd = bbin;

      bin = chunk (mreg, bbin);
      bchk = bin->binfwd;

      bwd->binfwd = bchk;
      bin->binfwd = bfwd;

      chk = chunk (mreg, bchk);
      if (__builtin_expect (chk->binbwd != bbin, 0))
	fatal_error ("unsorted bin link corrupted");
      chk->binbwd = bbwd;
    }
}
#endif

/*
 * ----------------------------- region_malloc --------------------------------
 */

static bui_t
_int_region_malloc (mregion *mreg, size_t bytes)
{
  bui_t reqsize;

  unsigned int idx;
  bui_t bbin;
  mbinptr bin;

  bui_t bchk;
  mchunk *chk;
  bui_t size;
  unsigned int chk_idx;

  bui_t bfwd;
  mchunk *fwd;
  bui_t bbwd;
  mchunk *bwd;
  bui_t bnxt;
  mchunk *next;
  bui_t diff;

  unsigned int block;
  unsigned int bit;
  unsigned int map;

  reqsize = req2size (bytes);

  if (__builtin_expect (mreg == NULL, 0))
    return 0;

  if (reqsize <= MAX_FAST_SIZE)
    {
      idx = fastbin_index (reqsize);
      mfastbinptr fb = &fastbin (mreg, idx);
      bchk = *fb;

      if (bchk != 0)
	{
	  if (__builtin_expect (chunk_misaligned (bchk), 0))
	    fatal_error ("invalid minbin chunk index");

	  chk = chunk (mreg, bchk);
	  *fb = chk->binfwd;
	  /* fastbin_index may overflow for invalid chunk sizes. */
	  size_t fb_idx = fastbin_index (chunksize (chk));
	  if (__builtin_expect (fb_idx != idx, 0))
	    fatal_error ("corrupted fastbin chunk size");
#if REGION_STATS
	  stats.nused ++;
	  stats.bused += reqsize;
#endif
	  return chunk2mem (bchk);
	}
    }

#if REMAINDERBIN
  int emptied_remainders;
#endif

  if (in_smallbin_range (reqsize))
    {
      idx = smallbin_index (reqsize);
      bbin = bin_bi (idx);
      bin = chunk (mreg, bbin);
      bchk = bin->binbwd;

      if (bchk != bbin)
	{
	  chk = chunk (mreg, bchk);
	  bbwd = chk->binbwd;
	  bwd = chunk (mreg, bbwd);
	  if (__builtin_expect (bwd->binfwd != bchk, 0))
	    fatal_error ("smallbin chunk link corrupted");
	__set_inuse (chk, reqsize);
	  bin->binbwd = bbwd;
	  bwd->binfwd = bbin;
#if REGION_STATS
	  stats.nused ++;
	  stats.bused += reqsize;
#endif
	  return chunk2mem (bchk);
	}
#if REMAINDERBIN
      emptied_remainders = 0;
#endif
    }
  else
    {
      idx = largebin_index (reqsize);

      if (mreg->have_fastchunks)
	region_consolidate (mreg);
#if REMAINDERBIN
      if (reqsize >= FASTBIN_CONSOLIDATION_THRESHOLD
	  && get_binmap (mreg, remainder_binidx))
	{
	  emptied_remainders = 1;
	  region_empty_remainder_bin (mreg);
	}
      else
	emptied_remainders = 0;
#endif
    }

  for (;;)
    {
#if REMAINDERBIN
      bui_t middleidx = middle_index (mreg->bi_top);
#endif
      int iters = 0;
      while ( (bchk = unsorted_bin (mreg)->binbwd) != unsorted_binbi)
	{
	  chk = chunk (mreg, bchk);
	  bbwd = chk->binbwd;
	  size = chunksize (chk);
	  bwd = chunk (mreg, bbwd);
	  next = chunk_at_offset (chk, size);
#if 0
	  if (__builtin_expect (__is_inuse (head) != 0, 0))
	    fatal_error ("unsorted chunk is_inuse");
#endif
	  if (__builtin_expect (size <= CHUNK_HDR_SIZE, 0)
	      || __builtin_expect (size >= regionsize (mreg), 0))
	    fatal_error ("unsorted: invalid chunk size");
	  if (__builtin_expect (get_head (next) < CHUNK_HDR_SIZE, 0)
	      || __builtin_expect (get_head (next) >= regionsize (mreg), 0))
	    fatal_error ("unsorted: invalid next size");
	  if (__builtin_expect (prev_size (next) != size, 0))
	    fatal_error ("unsorted: chunk->size != next->prev_size");
	  if (__builtin_expect (prev_inuse (next) != 0, 0))
	    fatal_error ("unsorted: invalid next->prev_inuse");
	  if (__builtin_expect (bwd->binfwd != bchk, 0)
	      || __builtin_expect (chk->binfwd != unsorted_binbi, 0))
	    fatal_error ("unsorted: linked list corrupted");

#if REMAINDERBIN
	  if (in_smallbin_range (reqsize)
	      && bbwd == unsorted_binbi
	      && size > reqsize + MINSIZE
	      && bchk < remainder_bin (mreg)->binbwd)
	    {
	      diff = size - reqsize;
	      bnxt = bchk + reqsize;
	      unsorted_bin (mreg)->binbwd = unsorted_bin (mreg)->binfwd = bnxt;
	      next = chunk (mreg, bnxt);
	      next->binbwd = next->binfwd = unsorted_binbi;

	      if (!in_smallbin_range (diff))
		{
		  next->sizfwd = 0;
		  next->sizbwd = 0;
		}
	      set_head (chk, reqsize | PREV_INUSE | IS_INUSE);
	      set_head (next, diff | PREV_INUSE);
	      set_foot (next, diff);
#if REGION_STATS
	      stats.nchks ++;
	      stats.nused ++;
	      stats.bused += reqsize;
#endif
	      return chunk2mem (bchk);
	    }
#endif
	  unsorted_bin (mreg)->binbwd = bbwd;
	  bwd->binfwd = unsorted_binbi;

	  if (size == reqsize)
	    {
#if REGION_STATS
	      stats.nused ++;
	      stats.bused += size;
#endif
	    __set_inuse (chk, size);
	      return chunk2mem (bchk);
	    }

	  if (in_smallbin_range (size))
	    {
	      chk_idx = smallbin_index (size);
	      bbwd = bin_bi (chk_idx);
	      bwd = chunk (mreg, bbwd);
	      bfwd = bwd->binfwd;
	      fwd = chunk (mreg, bfwd);
	    }
	  else
#if REMAINDERBIN
	    if (!in_remainder_size (size) || bchk >= middleidx
		|| (size > reqsize && emptied_remainders))
#endif
	    {
	      chk_idx = largebin_index (size);
	      bbwd = bin_bi (chk_idx);
	      bwd = chunk (mreg, bbwd);
	      bfwd = bwd->binfwd;
	      fwd = chunk (mreg, bfwd);

	      /* Maintain size sorted ordering for large bins */
	      if (bfwd != bbwd)
		{
		  /* OR with inuse bit now for faster comparisons. */
		  size |= PREV_INUSE;
		  /* If size is smaller than smallest, insert at bwd end. */
		  if (size < get_head (_binbwd (bwd)))
		    {
		      chk->sizfwd = bfwd;
		      chk->sizbwd = fwd->sizbwd;
		      fwd->sizbwd = _sizbwd (chk)->sizfwd = bchk;
		      bfwd = bbwd;
		      fwd = bwd;
		      bbwd = bwd->binbwd;
		      bwd = chunk (mreg, bbwd);
		    }
		  else
		    {
		      while (size < get_head (fwd))
			{
			  bfwd = fwd->sizfwd;
			  fwd = chunk (mreg, bfwd);
			}

		      if (size == get_head (fwd))
			{
			  bfwd = fwd->binfwd;
			  fwd = chunk (mreg, bfwd);
			}
		      else
			{
			  bbwd = fwd->sizbwd;
			  chk->sizfwd = bfwd;
			  chk->sizbwd = bbwd;
			  bwd = chunk (mreg, bbwd);
			  if (__builtin_expect (bwd->sizfwd != bfwd, 0))
			    fatal_error ("largebin size link corrupted");
			  fwd->sizbwd = bwd->sizfwd = bchk;
			}
		      bbwd = fwd->binbwd;
		      bwd = chunk (mreg, bbwd);
		      if (__builtin_expect (bwd->binfwd != bfwd, 0))
			fatal_error ("largebin bin link corrupted");
		    }
		}
	      else
		  chk->sizbwd = chk->sizfwd = bchk;
	    }
#if REMAINDERBIN
	  else
	    {
#if REGION_STATS && REMAINDER_STATS
	      ++stats.remainder_add_count;
#endif
	      chk_idx = remainder_binidx;
	      bbwd = bin_bi (chk_idx);
	      bwd = chunk (mreg, bbwd);
	      bfwd = bwd->binfwd;
	      fwd = chunk (mreg, bfwd);

	      if (bfwd != bbwd)
		{
		  if (bchk < bwd->binbwd)
		    {
		      bfwd = bbwd;
		      fwd = bwd;
		      bbwd = bwd->binbwd;
		      bwd = chunk (mreg, bbwd);
		    }
		  else
		    {
		      /* Remove chunks that are passed middle index */
		      if (bfwd > middleidx)
			{
			  while (fwd->binfwd > middleidx)
			    {
			      bfwd = fwd->binfwd;
			      fwd = chunk (mreg, bfwd);
			    }
			  bnxt = fwd->binfwd;

			  bbin = unsorted_binbi;
			  bin = chunk (mreg, bbin);
			  fwd->binfwd = bin->binfwd;
			  _binfwd (bin)->binbwd = bfwd;

			  bfwd = bwd->binfwd;
			  bwd->binfwd = bnxt;

			  fwd = chunk (mreg, bfwd);
			  if (__builtin_expect (fwd->binbwd != bbwd, 0))
			    fatal_error ("remainder linked list corrupted");
			  fwd->binbwd = bbin;
			  bin->binfwd = bfwd;

			  bfwd = bnxt;
			  fwd = chunk (mreg, bfwd);
			  fwd->binbwd = bbwd;
			}

		      while (fwd > chk)
			{
			  bfwd = fwd->binfwd;
			  fwd = chunk (mreg, bfwd);
			}

		      if (__builtin_expect (chk == fwd, 0))
			fatal_error ("double free or corruption (remainder)");
		      bbwd = fwd->binbwd;
		      bwd = chunk (mreg, bbwd);
		      if (__builtin_expect (bwd->binfwd != bfwd, 0))
			fatal_error ("remainder bin link corrupted");
		    }
		}
	    }
#endif
	  mark_bin (mreg, chk_idx);
	  chk->binfwd = bfwd;
	  chk->binbwd = bbwd;
	  bwd->binfwd = bchk;
	  fwd->binbwd = bchk;

#define MAX_UNSORTED_ITERS 16384
	  if (++iters >= MAX_UNSORTED_ITERS)
	    break;
	}

      if (!in_smallbin_range (reqsize))
	{
	  bbin = bin_bi (idx);
	  bin = bin_at (mreg, idx);
	  bchk = bin->binfwd;
	  chk = chunk (mreg, bchk);

	  size = reqsize | PREV_INUSE;

	  if (bchk != bbin && size <= get_head (chk))
	    {
	      if (size != get_head (chk))
		{
		  do
		    {
		      bchk = chk->sizbwd;
		      chk = chunk (mreg, bchk);
		    }
		  while (size > get_head (chk));
		}

	      size = get_head (chk);
	      bfwd = chk->binfwd;
	      fwd = chunk (mreg, bfwd);

	      if (bchk != bin->binbwd && size == get_head (fwd))
		{
		  bchk = bfwd;
		  chk = fwd;
		}

	      size = __chunksize (size);
	      diff = size - reqsize;

	      unlink_chunk (mreg, bchk);

	      if (diff < MINSIZE)
		__set_inuse (chk, size);
	      else
		{
		  size = reqsize;
		  bnxt = bchk + size;
		  next = chunk_at_offset (chk, size);

		  bbwd = unsorted_binbi;
		  bwd = unsorted_bin (mreg);
		  bfwd = bwd->binfwd;
		  fwd = chunk (mreg, bfwd);
		  if (__builtin_expect (fwd->binbwd != bbwd, 0))
		    fatal_error ("corrupted unsorted chunks");
		  next->binbwd = bbwd;
		  next->binfwd = bfwd;
		  bwd->binfwd = bnxt;
		  fwd->binbwd = bnxt;
		  if (!in_smallbin_range (diff))
		    {
		      next->sizfwd = 0;
		      next->sizbwd = 0;
		    }
		  set_head (chk, reqsize | IS_INUSE | PREV_INUSE);
		  set_head (next, diff | PREV_INUSE);
		  set_foot (next, diff);
#if REGION_STATS
		  stats.nchks++;
#endif
		}
#if REGION_STATS
	      stats.nused ++;
	      stats.bused += size;
#endif
	      return chunk2mem (bchk);
	    }
	}
#if REMAINDERBIN
      if (get_binmap (mreg, remainder_binidx))
	{
	  bbin = remainder_binbi;
	  bin = remainder_bin (mreg);
	  bchk = bin->binbwd;

	  if (bchk != bbin)
	    {
	      chk = chunk (mreg, bchk);
	      size = chunksize (chk);

	      if (size >= reqsize)
		{
		  diff = size - reqsize;

		  bfwd = bbin;
		  fwd = bin;
		  bbwd = chk->binbwd;
		  bwd = chunk (mreg, bbwd);

		  if (diff < MINSIZE)
		    __set_inuse (chk, size);
		  else
		    {
		      size = reqsize;
		      bnxt = bchk + size;
		      next = chunk_at_offset (chk, size);

		      if (in_smallbin_range (diff))
			{
			  bwd->binfwd = bfwd;
			  fwd->binbwd = bbwd;

			  bbwd = unsorted_binbi;
			  bwd = unsorted_bin (mreg);
			  bfwd = bwd->binfwd;
			  fwd = _binfwd (bwd);
			  if (__builtin_expect (fwd->binbwd != bbwd, 0))
			    fatal_error ("corrupted unsorted chunks");
			}
		      else
			{
#if REGION_STATS && REMAINDER_STATS
			  /* Down further we increment remainder_pop_count, as not
			   * to interrupt the other paths, and as we are the only
			   * path that keeps the remainder in the bin. We decrement
			   * to the effect of zero.
			   */
			  --stats.remainder_pop_count;
#endif
			  next->sizfwd = 0;
			  next->sizbwd = 0;
			}
		      next->binbwd = bbwd;
		      next->binfwd = bfwd;
		      bfwd = bbwd = bnxt;

		      set_head (chk, reqsize | IS_INUSE | PREV_INUSE);
		      set_head (next, diff | PREV_INUSE);
		      set_foot (next, diff);

#if REGION_STATS
		      stats.nchks++;
#endif
		    }
		  bwd->binfwd = bfwd;
		  fwd->binbwd = bbwd;
#if REGION_STATS && REMAINDER_STATS
		  ++stats.remainder_pop_count;
		  ++stats.remainder_use_count;
#endif
#if REGION_STATS
		  stats.nused ++;
		  stats.bused += size;
#endif
		  return chunk2mem (bchk);
		}
	      else
		{
		  bbwd = chk->binbwd;
		  bwd = chunk (mreg, bbwd);
		  if (__builtin_expect (bwd->binfwd != bchk, 0))
		    fatal_error ("corrupted unsorted chunks");
		  bin->binbwd = bbwd;
		  bwd->binfwd = bbin;
		  if (bin == bwd)
		    unmark_bin (mreg, remainder_binidx);

		  bbwd = unsorted_binbi;
		  bwd = unsorted_bin (mreg);
		  bfwd = bwd->binfwd;
		  fwd = chunk (mreg, bfwd);
		  if (__builtin_expect (fwd->binbwd != bbwd, 0))
		    fatal_error ("corrupted unsorted chunks");
		  chk->binbwd = bbwd;
		  chk->binfwd = bfwd;
		  bwd->binfwd = bchk;
		  fwd->binbwd = bchk;
#if REGION_STATS && REMAINDER_STATS
		  ++stats.remainder_pop_count;
#endif
		}
	    }
	  else
	    unmark_bin (mreg, remainder_binidx);
	}
#endif
      ++idx;
      bbin = bin_bi (idx);
      bin = bin_at (mreg, idx);
      block = idx2block (idx);
      map = mreg->binmap[block];
      bit = idx2bit (idx);

      for (;;)
	{
	  if (bit > map || bit == 0)
	    {
	      do
		{
		  if (++block >= BINMAPSIZE)
		    goto use_top;
		}
	      while ((map = mreg->binmap[block]) == 0);

	      bbin = bin_bi ((block << BINMAPSHIFT));
	      bin = bin_at (mreg, (block << BINMAPSHIFT));
	      bit = 1;
	    }

	  while ((bit & map) == 0)
	    {
	      bbin = next_binbi (bbin);
	      bin = next_bin (bin);
	      bit <<= 1;
	      assert (bit != 0);
	    }

	  bchk = bin->binbwd;

	  if (bchk == bbin)
	    {
	      mreg->binmap[block] = map &= ~bit;
	      bbin = next_binbi (bbin);
	      bin = next_bin (bin);
	      bit <<= 1;
	    }
	  else
	    {
	      chk = chunk (mreg, bchk);
	      size = chunksize (chk);
	      diff = size - reqsize;

	      assert (size >= reqsize);

	      unlink_chunk (mreg, bchk);

	      if (diff < MINSIZE)
		__set_inuse (chk, size);
	      else
		{
		  size = reqsize;
		  bnxt = bchk + size;
		  next = chunk_at_offset (chk, size);

		  bbwd = unsorted_binbi;
		  bwd = unsorted_bin (mreg);
		  bfwd = bwd->binfwd;
		  fwd = chunk (mreg, bfwd);
		  if (__builtin_expect (fwd->binbwd != bbwd, 0))
		    fatal_error ("corrupted unsorted chunks");
		  next->binbwd = bbwd;
		  next->binfwd = bfwd;
		  bwd->binfwd = bnxt;
		  fwd->binbwd = bnxt;
		  if (!in_smallbin_range (diff))
		    {
		      next->sizfwd = 0;
		      next->sizbwd = 0;
		    }
		  set_head (chk, reqsize | IS_INUSE | PREV_INUSE);
		  set_head (next, diff | PREV_INUSE);
		  set_foot (next, diff);
#if REGION_STATS
		  stats.nchks++;
#endif
		}
#if REGION_STATS
	      stats.nused ++;
	      stats.bused += size;
#endif
	      return chunk2mem (bchk);
	    }
	}

    use_top:
      bchk = mreg->bi_top;
      chk = chunk (mreg, bchk);
      size = chunksize (chk);

      if (__builtin_expect (size >= regionsize (mreg), 0))
	fatal_error ("top chunk size corrupted");

      if (size >= reqsize + MINSIZE)
	{
	  diff = size - reqsize;
	  bnxt = bchk + reqsize;
	  next = chunk_at_offset (chk, reqsize);
	  mreg->bi_top = bnxt;
	  set_head (chk, reqsize | IS_INUSE | PREV_INUSE);

	  /* Is this needed??? */
	  //set_prev_size (next, reqsize);

	  set_head (next, diff | IS_INUSE | PREV_INUSE);
#if REGION_STATS
	  stats.nchks++;
	  stats.nused ++;
	  stats.bused += reqsize;
#endif
	  return chunk2mem (bchk);
	}
      else if (mreg->have_fastchunks)
	{
	  region_consolidate (mreg);
	  if (in_smallbin_range (reqsize))
	    idx = smallbin_index (reqsize);
	  else
	    idx = largebin_index (reqsize);
	}
#if REMAINDERBIN
      else if (!emptied_remainders && get_binmap (mreg, remainder_binidx))
	{
	  emptied_remainders = 1;
	  region_empty_remainder_bin (mreg);
	  if (in_smallbin_range (reqsize))
	    idx = smallbin_index (reqsize);
	  else
	    idx = largebin_index (reqsize);
	}
#endif
      else
	{
	  return 0;
	}
    }
}

static bui_t
_int_region_realloc (mregion *const mreg,
		     bui_t bold,
		     size_t oldsize,
		     size_t reqsize)
{
  mchunk *old;

  bui_t bchk;
  mchunk *chk;
  bui_t size;

  bui_t bnxt;
  mchunk *next;

  bui_t diff;
  bui_t head;

  old = chunk (mreg, bold);
  bnxt = bold + oldsize;
  next = chunk_at_offset (old, oldsize);
  head = get_head (next);

  if (__builtin_expect (bnxt > mreg->bi_top, 0))
    fatal_error ("region_realloc(): invalid chunk size");

  if (__builtin_expect (head <= MINSIZE, 0)
      || __builtin_expect (bnxt + head >= regionsize (mreg), 0))
    fatal_error ("region_realloc(): invalid next size");

  if (oldsize >= reqsize)
    {
      bchk = bold;
      chk = old;
      size = oldsize;
    }
  else
    {
      if (bnxt == mreg->bi_top
	  && ( size = oldsize + __chunksize (head) ) >= reqsize + MINSIZE)
	{
	  diff = size - reqsize;
	  bnxt = bold + reqsize;
	  next = chunk_at_offset (old, reqsize);
	  mreg->bi_top = bnxt;
	  set_size_head (old, reqsize);
	  set_head (next, diff | IS_INUSE | PREV_INUSE);
#if REGION_STATS
	  stats.bused += reqsize - oldsize;
#endif
	  return chunk2mem (bold);
	}

      if (bnxt != mreg->bi_top
	  && !__is_inuse (head)
	  && ( size = oldsize + __chunksize (head) ) >= reqsize)
	{
	  bchk = bold;
	  chk = old;
	  unlink_chunk (mreg, bnxt);
#if REGION_STATS
	  stats.bused += __chunksize (head);
#endif
	}
      else
	{
	  bchk = _int_region_malloc (mreg, mem2chunk (reqsize));

	  if (bchk == 0)
	    return 0;

	  if (mem2chunk (bchk) == bnxt)
	    {
	      size = oldsize + chunksize (next);
	      bchk = bold;
	      chk = old;
#if REGION_STATS
	      stats.nused--;
#endif
	    }
	  else
	    {
	      void *newmem = chunk (mreg, bchk);
	      void *oldmem = chunk2mem (old);
	      size_t bytes = mem2chunk (oldsize);

	      memcpy (__builtin_assume_aligned (newmem, 8),
		      __builtin_assume_aligned (oldmem, 8), bytes);

	      _int_region_free (mreg, bold);

	      return bchk;
	    }
	}
    }

  assert (size >= reqsize);

  diff = size - reqsize;

  if (diff < MINSIZE)
    {
      set_size_head (chk, size);
    __set_inuse_prev (chk, size);
    }
  else
    {
      bnxt = bchk + reqsize;
      next = chunk_at_offset (chk, reqsize);
      set_size_head (chk, reqsize);
      set_head (next, diff | PREV_INUSE | IS_INUSE);
    __set_inuse_prev (next, diff);
      _int_region_free (mreg, bnxt);
    }

  return chunk2mem (bchk);
}



EXPORT int region_init (void *mem, size_t size);

EXPORT bui_t region_malloc (void *region, size_t bytes);

EXPORT void region_free (void *region, bui_t bi);

EXPORT bui_t region_realloc (void *region, bui_t bi, size_t bytes);

EXPORT void region_print_bins (void *region);

EXPORT void region_print (void *region);

/*
   Chunk Bins

    The first bin, bin zero, contains free chunks that are exactly fixsize size.

      ┌─────binbwd: NULL
      │
     [0]: sizes = fixsize
      │
      └─────binfwd ┐
		   ┊
      ┌─────binbwd ┘
   │
   [1]: sizes < fixsize
      │
      └─────binfwd ┐
		   ┊
      ┌─────binbwd ┘
      │
     [2]: sizes > fixsize
      │
      └─────binfwd
*/


#if 0
  /*   Fast free fixsize bin
   *
   * When chunks of fixsize are freed or created from a large chunk, they may
   * be placed in ffsbin to save some time. This is especially useful when
   * splitting large to fixsize chunks, but also allows the already quick
   * fixsize chunks to perform housekeeping tasks and still return in
   * reasonable time. As with GNU malloc's Fastbins, the chunks in this bin may
   * also retain their inuse bit; Chunks are processed LIFO for speed.
   *
   * Importantly, the last chunk uses the otherwise unused bwd, to holding a
   * count of linked chunks.  This count is incremented or decremented when
   * linking, and like an odd relay race, passed to the new or next top of bin
   * chunk. Ensuring not to overload this bin, the count lets us know when to
   * unload the bin, fragmentation may occur or large chunks may be blocked
   * from growing due to a chunk stuck at the bottom of this bin.
   */
  bui_t bi_ffsbin;
#endif

