/*
 * memregion.c
 *
 * Created 31 Aug 2023 by Mason Hall (mason@mashpot.net)
 *
 * Copyright (C) 2023 Mason Hall
 */

#include <stdio.h>
#include <stdlib.h>
#include <sys/stat.h>
#include <sys/mman.h>
#include <sys/syscall.h>
#include <unistd.h>
#include <fcntl.h>

#include "region.h"

#define ALIGN_MIN (4096)

#define ALIGN_MIN_MASK (ALIGN_MIN - 1)

#define POOL_ISOPEN (1 << 0)

struct rpool
{
  u64 _reserved0;

  union
    {
      u8 lock;
      u32 flags;
    };
  pid_t lockid;

  u32 refcnt;

  u32 _reserved1;

  bui_t size;

  bui_t rgsize;
  bui_t rgcount;

};

_Static_assert (sizeof (struct rpool) > ALIGN_MIN,
		"rpool: larger than ALIGN_MIN");

#define RPOOL_HDR_SIZE (ALIGN_MIN)

typedef struct _mregion
{
  union
    {
      u8 _mregion_lock;
      u32 _mregion_flags;
    };
  pid_t lockid;

  bui_t size;

} mregion;

_Static_assert (sizeof (pid_t) == 4,
		"mregion->lock: pid_t is not 32-bits");

#define MREGION_SIZE (32768)

#define MREGION_ALIGN (MREGION_SIZE)

#define MREGION_ALIGN_MASK (MREGION_ALIGN - 1)

#define mregion_align(n) \
  (((n) + MREGION_ALIGN_MASK) & ~(MREGION_ALIGN))

/* _mregion_flags is at top of mregion, so we can use the same pointer */
#define mregion_flags(p) \
  (atomic_load_relaxed (((u32 *) (p))))

#pragma GCC poison _mregion_lock
#pragma GCC poison _mregion_flags

struct rinfo
{
  u16 flags;

  i16 heat;

  u32 eac;

  /* Available chunks should provide at lease chunksize bytes, within
   * chunkavail is number of available chunks that can service this size.
   * If access to regions occurs from other processes, the accuracy of these
   * values is determined on the frequency the region is managed locally. When
   * the current process is alone in managing regions, all values should be
   * accurate.
   */
  u32_t chunksize;
  u32_t chunkavail;

  size_t size;

  bui_t idx;

  mregion *mem;

};

typedef struct _mpool
{
  size_t size;

  int flags;

  int f_fd;
  off_t f_size;
  off_t f_off;

  char _pad0 [
    ALIGN_MIN
      - sizeof (size_t)
      - sizeof (int)
      - sizeof (int)
      - sizeof (off_t)
      - sizeof (off_t)
  ];

  struct rpool rpool;

  char _pad1 [
    ALIGN_MIN
      - sizeof (struct rpool)
  ];

  size_t rmem_bytes;
  size_t rmem_count;
  size_t rinfo_max;
  size_t rinfo_len;

  struct rinfo rinfo [];

} mpool;

#define rpool_newsize(sz) \
  ((sz) + MREGION_ALIGN_MASK <= RPOOL_HDR_SIZE + MREGION_ALIGN \
   ? RPOOL_HDR_SIZE + MREGION_ALIGN \
   : ((sz) + MREGION_ALIGN_MASK) & ~(MREGION_ALIGN_MASK))


#define MPOOL_RINFO_MIN (1024)

#define mpool_newsize(nreg)				\
({						\
  size_t _sz = (nreg) > MPOOL_RINFO_MIN			\
		? (nreg)			\
		: MPOOL_RINFO_MIN;			\
						\
  _sz = _sz * sizeof (struct rinfo) + sizeof (mpool);			\
						\
  (_sz + ALIGN_MIN_MASK) & ~(ALIGN_MIN_MASK);			\
  })

static off_t
file_stat_size (int fd)
{
  struct stat st;

  if (stat (fd, &st) == -1)
    {
      warn_perror ("stat");
      return -1;
    }
  return st.st_size;
}

mpool *
mpool_init_new (size_t size, int fd)
{
  mpool *pool;
  void *mem;
  size_t msize;
  size_t nreg;
  off_t fsize;
  off_t foff;
  struct flock fl;

  size = rpool_newsize (size);

  nreg = (size - RPOOL_HDR_SIZE) / MREGION_SIZE;

  msize = mpool_newsize (nreg);

  mem = mmap (NULL, msize, PROT_NONE, MAP_SHARED | MAP_ANON, -1, 0);

  if (mem == MAP_FAILED)
    fatal_perror ("mmap");

  pool = mem;

  if (mprotect (pool, ALIGN_MIN, PROT_READ | PROT_WRITE) == -1)
    {
      warn_perror ("mprotect");
      goto err1;
    }

  foff = file_stat_size (fd);

  if (foff == -1)
    goto err1;

  foff = align_up (foff, ALIGN_MIN);
  fsize = foff + size;

  if (ftruncate (fd, fsize) == -1)
    {
      warn_perror ("ftruncate");
    goto err1;
    }

  pool->size = msize;
  pool->flags = 0;
  pool->f_fd = fd;
  pool->f_size = fsize;
  pool->f_off = foff;

  fl = (struct flock) {
      .l_type = F_WRLCK,
      .l_whence = SEEK_SET,
      .l_start = foff,
      .l_len = size,
      .l_pid = 0
  };
  if (TEMP_FAILURE_RETRY (fcntl (fd, F_OFD_SETLK, &fl)) == -1)
    {
      if (errno != EAGAIN && errno != EACCES)
	{
	  warn_perror ("fnctl");
	goto err1;
	}
    }

  mem = mmap (&pool->rpool, RPOOL_HDR_SIZE, PROT_READ | PROT_WRITE,
	      MAP_SHARED | MAP_FIXED, fd, foff);

  if (mem == MAP_FAILED)
    {
      warn_perror ("mmap");
    goto err1;
    }




err1:
  if (munmap (pool, msize) == -1)
    warn_perror ("munmap");
  return NULL;

}

static int
region_new (mregion *region, size_t bytes, size_t fixsize,
	    struct rinfo *rinfo)
{
  size_t chkavail;
  size_t chksize;

  if (region_init (region, bytes) != 0)
    return -1;

  if (fixsize)
    {
      chksize = fixsize;
      chkavail = region_fixed_bins (region, chksize);
    }
  else
    {
      chksize = bytes;
      chkavail = 1;
    }

  rinfo->chunksize = size;
  rinfo->chunkavail = count;
  rinfo->size = bytes;

  return 0;
}

