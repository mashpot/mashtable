#
# Makefile
#
# Created 31 Jul 2023 by Mason Hall (mason@mashpot.net)
#
# Copyright (C) 2023 Mason Hall
#

CC 			:=	gcc

SO			:=	libmashtable.so
EX			:=	mashtable

LIBS		:=	-lc -lm


#### C Flags ####

CFLAGS		:=	-Wall \
				-Wextra \
				-Wpedantic 

## Includes ##

CFLAGS		+=	-I"./src/include" \
				-include"./src/include/types.h" #\
				-save-temps=obj \
				-fopt-info-optimized \
				-fdiagnostics-color=always 

## Warnings ##

CFLAGS		+=	-Wformat=2 \
				-Wswitch-default \
				-Wcast-align \
				-Wshadow \
				-Wdisabled-optimization \
				-Wmissing-attributes \
				-Wsuggest-attribute=pure \
				-Wsuggest-attribute=const \
				-Wsuggest-attribute=noreturn \
				-Wmissing-noreturn \
				-Wsuggest-attribute=malloc \
				-Wsuggest-attribute=format \
				-Wmissing-format-attribute \
				-Wsuggest-attribute=cold \
				-Wduplicated-branches \
				-Wduplicated-cond \
				-Wunsafe-loop-optimizations \
				-Wdangling-else \
				-Wno-aggressive-loop-optimizations


 ## Releases ##

ifdef RELEASE
CFLAGS		+=	-O3 -DNDEBUG
 ifneq ($(RELEASE), 0)
CFLAGS		+= -g0 -s
 endif
else
CFLAGS		+=	-O3 -g3 \
				-fanalyzer \
				-fanalyzer-verbosity=3 \
				-Wno-missing-braces \
				-fvar-tracking-assignments \
				-gdwarf-5 \
				-Winline
#				-Wanalyzer-too-complex
#				-fsanitize=address
ifdef GDB
CFLAGS		+= -ggdb3 -fno-omit-frame-pointer
endif 

ifdef NODEBUG
CFLAGS		+= -DNDEBUG
else
CFLAGS		+= -DDEBUG=1
endif

ifdef DEBUG
 ifneq ($(DEBUG), 0)
CFLAGS		+=	-O0 -g3 -ggdb3
 endif
 ifeq ($(DEBUG), 2)
CFLAGS		+=	-Og
 endif
endif
endif

ifdef RB_FREE_LIST
CFLAGS		+= -DRB_FREE_LIST=$(RB_FREE_LIST)
endif

 ## Security ##

CFLAGS		+=	-Wformat-security \
				-D_FORTIFY_SOURCE=3 \
				-DPIC \
				-fpie \
				-fPIC \
				-Wl,-z,relro \
				-Wl,-z,now

 ## Misc ##

YEAR		=	$(shell date +%Y)

CFLAGS		+=	-D__YEAR__=$(YEAR)


#### Shared Object ####

SOFLAGS		:=	$(CFLAGS) \
				-Wstack-usage=512 \
				-fvisibility=hidden \
				-shared \
				-Wl,-e,soentry \
				-Wl,-soname=$(SO)
				#-flto \
				#-ffat-lto-objects \
				#-fuse-linker-plugin \

SOFILES		:=	globdefs.c \
				mreg.c \
				region.c \
				mashlib.c \
				debug.c \
				test.c \
				sys/clone3.c

SOHEADS		:=	globdefs.h \
				mreg.h \
				balloc.h \
				mashlib.h \
				timing.h


#### Executable ####

EXFLAGS		:=	$(CFLAGS)

EXFILES		:=	main.c

EXHEADS		:=	mreg.h \
				balloc.h \
				mashlib.h


#### SO Files ####

SOSRCS		:=	$(addprefix ./src/, $(SOFILES))
SOHDRS		:=	$(addprefix ./src/include/, $(SOHEADS))

SOOBJS		:=	$(addprefix ./build/, $(SOFILES:.c=.o))
SODEPS		:=	$(addprefix ./build/, $(SOFILES:.c=.d))
SOASMS		:=	$(SOOBJS:.o=.S)


#### EX Files ####

EXSRCS		:=	$(addprefix ./src/, $(EXFILES))
EXHDRS		:=	$(addprefix ./src/include/, $(EXHEADS))

EXOBJS		:=	$(addprefix ./build/, $(EXFILES:.c=.o))
EXDEPS		:=	$(addprefix ./build/, $(EXFILES:.c=.d))
EXASMS		:=	$(EXOBJS:.o=.S)


#### Misc ####

FMT1	:= '\n%s\n\n'
FMT2	:= '\n%s\n\n'
FMT3	:= '%s\n\n'
FMTC1	:= '\n\e[1;92m%s\e[0m\n\n'
FMTC2	:= '\n\e[1;34m%s\e[0m\n\n'
FMTC3	:= '\e[38;5;7m%s\e[0m\n\n'

POUT=@[ -t 1 ] && FMT=$(FMTC1) || FMT=$(FMT1); printf "$${FMT}" "$(shell ./scripts/pname.sh $@)"
POBJ=@[ -t 1 ] && FMT=$(FMTC2) || FMT=$(FMT2); printf "$${FMT}" "$(shell ./scripts/pname.sh $<)"
PCMD=@[ -t 1 ] && FMT=$(FMTC3) || FMT=$(FMT3); printf "$${FMT}"

#### Recipies ####

default: all

none: ;

all: so ex

so:		$(SO)
ex:		$(EX)
asm:	$(SOASMS) $(EXASMS)

ifneq (,$(filter all so,$(MAKECMDGOALS)))
include $(SODEPS)
endif

$(SODEPS)	: $(SOOBJS)
$(SOOBJS)	: CFLAGS := $(SOFLAGS)
$(SO)		: $(SOOBJS) 
	
	$(POUT)
	$(PCMD) '$(CC) $(SOFLAGS) -o $@ $(SOOBJS) $(LIBS)'
	@$(CC) $(SOFLAGS) -o $@ $(SOOBJS) $(LIBS)

ifneq (,$(filter all ex,$(MAKECMDGOALS)))
include $(EXDEPS)
endif

$(EXDEPS)	: $(EXOBJS)
$(EXOBJS)	: CFLAGS := $(EXFLAGS)
$(EX)		: $(EXOBJS)
	
	$(POUT)
	$(PCMD) '$(CC) $(EXFLAGS) -o $@ $(EXOBJS) $(LIBS) -l$(EX)'
	@$(CC) $(EXFLAGS) -o $@ $(EXOBJS) $(LIBS) -l$(EX)


BUILD_CC_CMD = $(CC) $(CFLAGS) \
			   -MD -MP -MF"$(@:%.o=%.d)" \
			   -MT"$@" -MT"$(@:%.o=%.d)" \
			   -c "$<" -o "$@"

build/%.o : ./src/%.c
	
	@mkdir -p $(@D)
	$(POBJ)
	$(PCMD) '$(BUILD_CC_CMD)'
	@$(BUILD_CC_CMD)

################

$(SOASMS)	: CFLAGS := $(SOFLAGS)
$(EXASMS)	: CFLAGS := $(EXFLAGS)

BUILD_AS_CMD = $(CC) $(CFLAGS) -masm=intel -fverbose-asm -S -c "$<" -o "$@"

build/%.S : ./src/%.c
	
	@mkdir -p $(@D)
	$(POBJ)
	$(PCMD) '$(BUILD_AS_CMD)'
	@$(BUILD_AS_CMD)

################

#SOMACROS = $(SOOBJS:.o=.c.h)
#EXMACROS = $(EXOBJS:.o=.c.h)
#
#macros: $(SOMACROS) $(EXMACROS)
#
#$(SOMACROS)	: CFLAGS := $(SOFLAGS)
#$(EXMACROS)	: CFLAGS := $(EXFLAGS)
#
#build/%.c.h : ./src/%.c
#	
#	$(CC) $(CFLAGS) -E -dM -c "$<" -o "$@"

################

CLEAN_FILES	:= $(shell find ./build/ -mindepth 1 \( -type f -a -iname '*.[osdi]' \) -o \( -type d -a -empty \)) \
			   $(shell [ -f "$(SO)" ] && echo "$(SO)") \
			   $(shell [ -f "$(EX)" ] && echo "$(EX)")

.PHONY: clean
clean:
	echo "make clean!"
ifneq (0,$(words $(CLEAN_FILES)))
	-rm -d $(CLEAN_FILES)
endif
	
	
#printf '"%s"\n' $(CLEAN_FILES)
#-rm -rf ./build/*.o $(SO) $(EX)


#build/%.o : ./src/%.c
#	@mkdir -p $(@D)
#	@printf $(FMTC2) "$(shell ./scripts/pname.sh $<)"
#	$(PCMD) '$(CC) $(CFLAGS) -MD -MP -MF"$(@:%.o=%.d)" -MT"$@" -c "$<" -o "$@"'
#	@$(CC) $(CFLAGS) -c "$<" -o "$@"
#	 -MP  $(DEPS) -MD -MF"$(@:%.o=%.d)" -MT"$@"

