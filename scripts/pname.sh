#!/bin/bash
#
# pname.sh
#
# Created 31 Aug 2023 by Mason Hall (mason@mashpot.net)
#
# Copyright (C) 2023 Mason Hall

#set -x

alignup() {
  local v a
  v=${1:-0}
  a=${2:-0}
  (( a = 0 < a ? a - 1 : 0 ))
  (( v = (v + a) & ~a ))
  echo $v
  return 0
}

strfill() {
  local str len s n
  local -n buf=$1

  len=${2:-4}
  (( n = len - (len & 3) ))

  str=${3:-"-"}
  s="$str$str$str$str"

  for ((i = 0; i < $n; i += 4))
  do
    buf+="$s"
  done

  for ((i = $n; i < $len; ++i))
  do
    buf+="$char"
  done

  return 0
}

printname() {
  local name nsize n l hfill hsize

  name="$1"
  nlen=${#name}

  (( hlen = $2 ? $2 : 32 ))
  hlen=$(alignup "$hlen" "4")

  fill=' '
  strfill fill "$hlen" "-"
  fill+=' '

  (( n = (nlen) ))
  (( n = (hlen - n) ))
  (( n >= 4 )) \
    && (( n /= 2 )) \
    || (( n = 2 ))

  (( l = -(n + hlen - nlen - n*2) ))

  printf '%s%s%s\n' "${fill:$l}" "$name" "${fill:0:$n}"

  return 0
}

length=0
name=

while test $# -gt 0
do
  case "$1" in
  -n | --n | -s | --s | --si | --siz | --size)
    if test $# -eq 1; then
      printf "\
pname: missing SIZE argument '$1'
syntax: pname [-n SIZE] [--size=SIZE] name...
" 1>&2
      exit 1
    fi
    shift
    length="$1"
    ;;
  -n=* | --n=* | -s=* | --s=* | --si* | --siz* | --size*)
    length=${1##*=}
    ;;
  *)
    name+="$1"
  esac
  shift
done

printname "$name" "$length"

