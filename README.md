Mashtable
=========

Position independent memory allocator
-------------------------------------

This was meant to be just a rewriting the allocator and hash-table I
wrote for Nirah, another project of mine. Initially the focus was on
just the hash-table hence then name 'mashtable'. Though, now I would
like to put more focus towards the allocator, subsequently allowing for
use in further structures other than the planned hash-table.

The allocator performs position independent allocation using memory
offsets instead of pointers, offset from the top of a memory mapped
region. Currently these regions exist only within memory, but the purpose
of using offsets is to store data that persists on disk.

Building
--------

I typically build in Linux for Linux, in this project I am using some
hardening techniques that are particular to ELF binaries, as well as GCC
particular builtins.

Support for non-POSIX compliant operating systems is not guaranteed. Currently
only supporting linux and GCC will be needed to build.

Building will result in two binaries, the shared object 'mashtable.so', and
executable 'mashtable'.

The following commands can be used to build:

```
git clone https://gitlab.com/mashpot/mashtable.git
cd ./mashtable/
make all
```

Usage
-----

Right now, usage is limited to the internal functions. I will fill this section
out later on.

